"""titan_web_api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.urls import include
from django.conf.urls.static import static
from django.conf import settings
from api_v1.serializers import CustomJWTSerializer
from rest_framework_swagger.views import get_swagger_view
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework_jwt.views import ObtainJSONWebToken, obtain_jwt_token, refresh_jwt_token
from api_v1 import views

schema_view = get_swagger_view(title='Request API')
urlpatterns = [
    path('', admin.site.urls),
    path('api/v1/login', obtain_jwt_token),
    path('api/v1/user/login', ObtainJSONWebToken.as_view(serializer_class=CustomJWTSerializer)),
    path('api/v1/refresh', refresh_jwt_token),
    path('api/v1/', include('api_v1.urls')),
]

if settings.DEBUG is True:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
