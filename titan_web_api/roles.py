from rolepermissions.roles import AbstractUserRole


class Admin(AbstractUserRole):
    available_permissions = {
        'create_admin_user': True,
        'create_member_user': True,
        'create_payee': True,
        'approve_payee': True,
        'process_deposit': True,
        'process_payment': True,
    }


class MainAccountUser(AbstractUserRole):
    available_permissions = {
        'create_admin_user': False,
        'create_member_user': True,
        'create_payee': True,
        'approve_payee': False,
        'process_deposit': False,
        'process_payment': False,
    }


class MemberUser(AbstractUserRole):
    available_permissions = {
        'create_admin_user': False,
        'create_member_user': False,
        'create_payee': True,
        'approve_payee': False,
        'process_deposit': False,
        'process_payment': False,
    }
