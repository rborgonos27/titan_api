class ResponseTemplate:
    @staticmethod
    def success(version, data, message):
        return {
            'version': version,
            'success': True,
            'data': data,
            'message': message
        }

    @staticmethod
    def failure(version, code):
        return {
            'version': version,
            'success': False,
            'data': None,
            'message': ''
        }
