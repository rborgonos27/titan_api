import datetime
from api_v1.models import FeeTier, FeeTransaction
from api_v1.serializers import FeeTierSerializer, FeeTransactionSerializer

class Computation:
    @staticmethod
    def get_total_transaction_amount(date, type, user_id):
        month = datetime.datetime.strptime(date, "%Y-%m-%d").month
        transaction = FeeTransaction.objects.filter(
            user_id=user_id,
            type=type,
            created_at__month=month
        ).order_by('-created_at')
        # print(transaction)
        if transaction:
            transaction_data = FeeTransactionSerializer(transaction, many=True)
            if not transaction_data:
                total_transaction_amount = 0
            else:
                total_transaction_amount = transaction_data.data[0]['total_transaction_amount']
        else:
            total_transaction_amount = 0
        return total_transaction_amount

    @staticmethod
    def compute_fee(logger, segragated_amount, rate_type, fee_tier_data, total_transaction_amount, type):
        first_fee_tier = fee_tier_data.data[0]
        second_fee_tier = fee_tier_data.data[1]
        third_fee_tier = fee_tier_data.data[2]

        logger.info("First tier amount: {amount}".format(amount=segragated_amount['first_amount']))
        logger.info("Second tier amount: {amount}".format(amount=segragated_amount['second_amount']))
        logger.info("Third tier amount: {amount}".format(amount=segragated_amount['third_amount']))

        first_tier_amount = segragated_amount['first_amount'] * float(first_fee_tier[rate_type])
        second_tier_amount = segragated_amount['second_amount'] * float(second_fee_tier[rate_type])
        third_tier_amount = segragated_amount['third_amount'] * float(third_fee_tier[rate_type])

        logger\
            .info("First Tier Amount ({tier_amount}) x first tier Rate ({tier_rate}) : {amount}"
                .format(
                    amount=first_tier_amount,
                    tier_amount=segragated_amount['first_amount'],
                    tier_rate=float(first_fee_tier[rate_type])
                )
            )

        logger.info("Second Tier Amount ({tier_amount}) x second ier Rate ({tier_rate}) : {amount}"
            .format(
            amount=second_tier_amount,
            tier_amount=segragated_amount['second_amount'],
            tier_rate=float(second_fee_tier[rate_type])
        )
        )

        logger.info("Third Tier Amount ({tier_amount}) x third ier Rate ({tier_rate}) : {amount}"
            .format(
            amount=third_tier_amount,
            tier_amount=segragated_amount['third_amount'],
            tier_rate=float(third_fee_tier[rate_type])
        )
        )

        total_fee_amount = first_tier_amount + second_tier_amount + third_tier_amount

        logger.info("Current total amount of monthly {type} bucket: {amount}"
            .format(
            amount=total_transaction_amount,
            type=type))

        logger.info("Transaction fee amount: {amount}".format(amount=total_fee_amount))
        return total_fee_amount

    @staticmethod
    def segregate_amount_by_tier(tier_data, amount, total_transaction_amount, threshold_type):
        amount = float(amount)
        total_transaction_amount = float(total_transaction_amount)

        first_fee_tier = tier_data[0]
        second_fee_tier = tier_data[1]

        first_tier_amount = 0
        second_tier_amount = 0
        third_tier_amount = 0
        total_amount = amount + total_transaction_amount
        if total_transaction_amount > 0:
            print('step 1')
            if total_transaction_amount < float(first_fee_tier[threshold_type]):
                print('step 2')
                if total_amount > float(second_fee_tier[threshold_type]):
                    print('step 3')
                    first_tier_amount = float(first_fee_tier[threshold_type]) - total_transaction_amount
                    second_tier_amount = float(second_fee_tier[threshold_type]) -\
                        float(first_fee_tier[threshold_type])
                    third_tier_amount = amount - first_tier_amount - second_tier_amount
                elif total_amount > float(first_fee_tier[threshold_type]):
                    first_tier_amount = float(first_fee_tier[threshold_type]) - total_transaction_amount
                    second_tier_amount = amount - first_tier_amount
                else:
                    first_tier_amount = amount
            elif total_transaction_amount <= float(second_fee_tier[threshold_type]):
                if total_amount > float(second_fee_tier[threshold_type]):
                    second_tier_amount = float(second_fee_tier[threshold_type]) - total_transaction_amount
                    third_tier_amount = amount - second_tier_amount
                else:
                    second_tier_amount = amount
            elif total_transaction_amount > float(second_fee_tier[threshold_type]):
                third_tier_amount = amount
            return {'first_amount': first_tier_amount, 'second_amount': second_tier_amount,
                    'third_amount': third_tier_amount}

        if amount <= float(first_fee_tier[threshold_type]):
            first_tier_amount = amount
        elif amount <= float(second_fee_tier[threshold_type]):
            first_tier_amount = float(first_fee_tier[threshold_type])
            second_tier_amount = amount - first_tier_amount
        elif amount > float(second_fee_tier[threshold_type]):
            first_tier_amount = float(first_fee_tier[threshold_type])
            second_tier_amount = float(second_fee_tier[threshold_type]) - first_tier_amount
            third_tier_amount = amount - float(second_fee_tier[threshold_type])
        return {
                'first_amount': first_tier_amount,
                'second_amount': second_tier_amount,
                'third_amount': third_tier_amount
                }

    @staticmethod
    def get_fee_tier(user_id):
        fee_tier = FeeTier.objects.filter(user_id=user_id)
        fee_tier_data = FeeTierSerializer(fee_tier, many=True)
        return fee_tier_data

    @staticmethod
    def get_fee_percent(segragated_amount, fee_tier_data, rate_type):
        first_fee_tier = fee_tier_data.data[0]
        second_fee_tier = fee_tier_data.data[1]
        third_fee_tier = fee_tier_data.data[2]

        first_tier_amount = segragated_amount['first_amount']
        second_tier_amount = segragated_amount['second_amount']
        third_tier_amount = segragated_amount['third_amount']

        fee_percent = 0
        if third_tier_amount > 0:
            fee_percent = third_fee_tier[rate_type]
        elif second_tier_amount > 0:
            fee_percent = second_fee_tier[rate_type]
        elif first_tier_amount > 0:
            fee_percent = first_fee_tier[rate_type]

        return fee_percent
