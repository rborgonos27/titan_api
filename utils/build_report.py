from reportlab.pdfgen import canvas
from PyPDF2 import PdfFileWriter, PdfFileReader

import time
from reportlab.lib.enums import TA_JUSTIFY
from reportlab.lib.pagesizes import letter
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image
from reportlab.platypus.tables import Table, TableStyle
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import inch
from reportlab.lib import colors
from reportlab.pdfbase.pdfmetrics import stringWidth


class BuildReport:
    font = 'Helvetica'
    
    @staticmethod
    def build_line(c, line_x, line_y, line_end, is_thick):
        c.line(line_x, line_y, line_end, line_y)
        if is_thick:
            c.line(line_x, line_y - 1, line_end, line_y - 1)

    @staticmethod
    def build_text(c, x, y, text, is_bold):
        c.drawString(x, y, text)
        if is_bold:
            c.drawString(x + 1, y, text)

    @staticmethod
    def build_sub_header(c, x, y, text, is_bold):
        font = 'Helvetica'
        c.setLineWidth(.3)
        c.setFont(font, 20)
        BuildReport.build_text(c, x, y, text, is_bold)

    @staticmethod
    def build_sub_header_small(c, x, y, text, is_bold):
        font = 'Helvetica'
        c.setLineWidth(.3)
        c.setFont(font, 14)
        BuildReport.build_text(c, x, y, text, is_bold)

    @staticmethod
    def build_ordinary_text(c, x, y, text, is_bold):
        font = 'Helvetica'
        c.setLineWidth(.3)
        c.setFont(font, 10)
        BuildReport.build_text(c, x, y, text, is_bold)

    @staticmethod
    def format_transaction_number(value):
        return "$ {: >15,.2f}".format(value)

    @staticmethod
    def format_report_number(value):
        return "$ {: >5,.2f}".format(value)
