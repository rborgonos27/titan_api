from api_v1.models import User, AuditLog, Wallet, Fee, FeeTransaction, FeeTierRevision, FeeTier, File, MemberStatement,\
    Request, RequestRevisions


class ClearRecord:

    @staticmethod
    def clear(email):
        user = User.objects.get(email=email)

        if not user:
            print ("email not found")
            return None

        ClearRecord.clear_audit(user.id)
        print ('Audit Cleared')
        ClearRecord.clear_wallet(user.id)
        print ('Wallet Cleared')
        ClearRecord.clear_fee_transaction(user.id)
        print ('Fee Transaction Cleared')
        ClearRecord.clear_fee_tier_revision(user.id)
        print ('Fee Tier Revision Cleared')
        ClearRecord.clear_fee(user.id)
        print ('Fee Cleared')
        ClearRecord.clear_fee_tier(user.id)
        print ('Fee Tier Cleared')
        ClearRecord.clear_file(user.id)
        print ('File Cleared')
        ClearRecord.clear_member_statement(user.id)
        print ('Member Statement Cleared')
        ClearRecord.clear_member_request_revision(user.id)
        print ('Member Request Revision Cleared')
        ClearRecord.clear_member_request(user.id)
        print ('Member Request Cleared')
        ClearRecord.clear_user(user.id)
        print ('User Cleared')

        return None

    @staticmethod
    def clear_audit(user_id):
        AuditLog.objects.filter(user_id=user_id).delete()
        return None

    @staticmethod
    def clear_wallet(user_id):
        Wallet.objects.filter(user_id=user_id).delete()
        return None

    @staticmethod
    def clear_fee_transaction(user_id):
        FeeTransaction.objects.filter(user_id=user_id).delete()
        return None

    @staticmethod
    def clear_fee_tier_revision(user_id):
        FeeTierRevision.objects.filter(user_id=user_id).delete()
        return None

    @staticmethod
    def clear_fee(user_id):
        Fee.objects.filter(user_id=user_id).delete()
        return None

    @staticmethod
    def clear_fee_tier(user_id):
        FeeTier.objects.filter(user_id=user_id).delete()
        return None

    @staticmethod
    def clear_file(user_id):
        File.objects.filter(user_id=user_id).delete()
        return None

    @staticmethod
    def clear_member_statement(user_id):
        MemberStatement.objects.filter(user_id=user_id).delete()
        return None

    @staticmethod
    def clear_member_request_revision(user_id):
        RequestRevisions.objects.filter(user_id=user_id).delete()
        return None

    @staticmethod
    def clear_member_request(user_id):
        Request.objects.filter(user_id=user_id).delete()
        return None

    @staticmethod
    def clear_user(user_id):
        User.objects.filter(id=user_id).delete()
        return None
