from datetime import datetime
from api_v1.models import AuditLog


def audit_log(audit):
    audit_log = AuditLog(module=audit['module'],
                         transaction=audit['transaction'],
                         description=audit['description'],
                         log_at=datetime.now(),
                         user_id=audit['user_id'],
                         )
    audit_log.save()

    return audit_log
