from reportlab.pdfgen import canvas
from reportlab.graphics.barcode import createBarcodeDrawing
from reportlab.lib.units import mm
from api_v1.models import User


class BuildDepositTicket:
    @staticmethod
    def build(request_data):
        ticket_file = "storage/ticket_{user_id}.pdf".format(user_id=request_data.user_id)
        member = User.objects.get(id=request_data.user_id)
        athena_pay_logo = 'storage/images/athena_logo.png'
        titan_logo = 'storage/images/titan-vault.png'
        separator = 'storage/images/ticket-separator.png'

        c = canvas.Canvas(ticket_file)
        c.setTitle('Deposit Ticket')

        font = 'Helvetica'

        x = 30
        y = 800

        member_location_id = member.location_id if member.location_id else 'N/A'
        for i in range(3):
            # header
            c.setFont(font, 8)
            c.drawString(200, y + 35, 'Please send this portion with your deposit')

            y = y - 11
            # LOGO and affiliates
            c.drawImage(athena_pay_logo, x + 350, y + 10, 134, 15, mask='auto')
            c.setFont(font, 7)
            c.drawString(x + 375, y, 'affiliate of')
            c.drawString(x + 375, y, 'affiliate of')
            c.drawImage(titan_logo, x + 412, y - 3, 69, 9, mask='auto')

            c.setFont(font, 12)
            c.drawString(x, y+20, 'Athena Pay, LLC')
            c.drawString(x, y+20, 'Athena Pay, LLC')

            y = y + 10

            c.setFont(font, 7)
            c.drawString(x, y, "{name}, {account_no}".format(name=member.business_name, account_no=member.member_account_id))

            c.setFont(font, 10)
            y = y - 15
            c.drawString(x, y, "{address}".format(address=member.address))

            y = y - 15
            c.setFont(font, 10)
            c.drawString(x, y, "{city}, {state}, {zip}".format(city=member.city, state=member.state, zip=member.zip_code))

            # barcode
            c.setFont(font, 9)
            barcode_width = 3
            y = y - 70
            barcode_rtn_value='321177573'
            barcode_rtn = createBarcodeDrawing('Code128', value=barcode_rtn_value, barWidth=barcode_width*mm, barHeight=50*mm, humanReadable=True).save(formats=['gif'],outDir='.',fnRoot='barcode')
            c.drawImage(barcode_rtn, x, y - 3, 99, 30, mask='auto')
            c.drawString(x + 100, y + 10, barcode_rtn_value)
            c.drawString(x + 100, y + 10, barcode_rtn_value)

            c.drawString(x + 180, y + 10, 'Member')
            c.drawString(x + 180, y + 10, 'Member')

            c.line(x+260, y + 8, x+380, y+8)
            c.drawString(x + 260, y + 10, member.business_name)

            c.drawString(x+410, y + 10, 'Currency')
            c.drawString(x+410, y + 10, 'Currency')

            c.line(x+470, y + 8, x+530, y+8)
            c.drawString(x+470, y + 10, 'Paper USD')

            y = y - 40
            barcode_account_value=member.member_account_id
            barcode_account = createBarcodeDrawing('Code128', value=barcode_account_value, barWidth=barcode_width*mm, barHeight=50*mm, humanReadable=True).save(formats=['gif'],outDir='.',fnRoot='barcode_account')
            c.drawImage(barcode_account, x, y - 3, 99, 30, mask='auto')
            c.drawString(x + 100, y + 10, barcode_account_value)
            c.drawString(x + 100, y + 10, barcode_account_value)

            c.drawString(x + 180, y + 10, 'Location Id')
            c.drawString(x + 180, y + 10, 'Location Id')

            c.line(x+260, y + 8, x+380, y+8)
            c.drawString(x + 260, y + 10, member_location_id)

            c.drawString(x+410, y + 10, 'Total Deposit')
            c.drawString(x+410, y + 10, 'Total Deposit')

            c.line(x+470, y + 8, x+530, y+8)
            c.drawString(x+470, y + 10, "$ {: >15,.2f}".format(request_data.amount))

            y = y - 40
            barcode_location_value=member_location_id
            barcode_location = createBarcodeDrawing('Code128', value=barcode_location_value, barWidth=barcode_width*mm, barHeight=50*mm, humanReadable=True).save(formats=['gif'],outDir='.',fnRoot='barcode_location')
            c.drawImage(barcode_location, x, y - 3, 99, 30, mask='auto')
            c.drawString(x + 100, y + 10, barcode_location_value)
            c.drawString(x + 100, y + 10, barcode_location_value)

            c.drawString(x + 180, y + 10, 'Pickup Date')
            c.drawString(x + 180, y + 10, 'Pickup Date')

            c.line(x+260, y + 8, x+380, y+8)
            c.drawString(x + 260, y + 10, request_data.pickup_at)

            y = y - 56
            c.drawImage(separator, x - 10, y - 3, 550, 30, mask='auto')

            # ========== second row
            y = y - 45

        c.setFont(font, 26)
        c.save()

        return ticket_file;
