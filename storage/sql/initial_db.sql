-- DROP DATABASE titan_db; -- optional
CREATE DATABASE titan_db DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
GRANT ALL PRIVILEGES ON titan_db.* TO 'coyns'@'localhost' IDENTIFIED BY 'snyoc';
GRANT ALL PRIVILEGES ON titan_db.* TO 'coyns'@'%' IDENTIFIED BY 'snyoc';

-- DROP DATABASE test_titan_db; -- optional
CREATE DATABASE test_titan_db DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
GRANT ALL PRIVILEGES ON test_titan_db.* TO 'coyns'@'localhost' IDENTIFIED BY 'snyoc';
GRANT ALL PRIVILEGES ON test_titan_db.* TO 'coyns'@'%' IDENTIFIED BY 'snyoc';

--CREATE DATABASE titan_db DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
--GRANT ALL PRIVILEGES ON titan_db.* TO 'coyns'@'localhost' IDENTIFIED BY 'snyoc';
--GRANT ALL PRIVILEGES ON titan_db.* TO 'coyns'@'%' IDENTIFIED BY 'snyoc';