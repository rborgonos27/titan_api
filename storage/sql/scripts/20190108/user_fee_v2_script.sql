delimiter //
DROP PROCEDURE IF EXISTS proc_fee_user_v2 //
CREATE PROCEDURE proc_fee_user_v2 ()
BEGIN
    DECLARE v_user_id INT;
    DECLARE done INT;

    DECLARE cursorUser CURSOR FOR
        select id
        from api_v1_user;

    DECLARE CONTINUE handler FOR NOT FOUND SET done = 1;
    SET done = 0;

    OPEN cursorUser;
    userLoop: loop
        FETCH cursorUser INTO v_user_id;
        IF done = 1 THEN leave userLoop; END IF;

        INSERT INTO api_v1_feetier(
            type,
            tier_no,
            deposit_rate,
            deposit_threshold,
            payment_rate,
            payment_threshold,
            user_id,
            created_by,
            updated_by,
            created_at,
            updated_at
        )
        SELECT
            'v2',
            4,
            tier_4_deposit_rate,
            tier_4_deposit_threshold,
            tier_4_payment_rate,
            tier_4_deposit_threshold,
            v_user_id,
            v_user_id,
            v_user_id,
            NOW(),
            NOW() from api_v1_defaultsettings;

        INSERT INTO api_v1_feetier(
            type,
            tier_no,
            deposit_rate,
            deposit_threshold,
            payment_rate,
            payment_threshold,
            user_id,
            created_by,
            updated_by,
            created_at,
            updated_at
        )
        SELECT
            'v2',
            5,
            tier_5_deposit_rate,
            tier_5_deposit_threshold,
            tier_5_payment_rate,
            tier_5_deposit_threshold,
            v_user_id,
            v_user_id,
            v_user_id,
            NOW(),
            NOW() from api_v1_defaultsettings;

    END loop userLoop;
    CLOSE cursorUser;
END//

delimiter ;