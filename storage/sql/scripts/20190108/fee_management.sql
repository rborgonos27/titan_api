DELETE FROM api_v1_defaultsettings;

INSERT INTO api_v1_defaultsettings (
                                    tier_1_deposit_rate,
                                    tier_2_deposit_rate,
                                    tier_3_deposit_rate,
                                    tier_4_deposit_rate,
                                    tier_5_deposit_rate,
                                    tier_1_deposit_threshold,
                                    tier_2_deposit_threshold,
                                    tier_3_deposit_threshold,
                                    tier_4_deposit_threshold,
                                    tier_5_deposit_threshold,
                                    tier_1_payment_rate,
                                    tier_2_payment_rate,
                                    tier_3_payment_rate,
                                    tier_4_payment_rate,
                                    tier_5_payment_rate,
                                    tier_1_payment_threshold,
                                    tier_2_payment_threshold,
                                    tier_3_payment_threshold,
                                    tier_4_payment_threshold,
                                    tier_5_payment_threshold,
                                    tax_payment_rate,
                                    payroll_payment_rate,
                                    deposit_flat_rate,
                                    payment_flat_rate,
                                    created_by,
                                    updated_by,
                                    created_at,
                                    updated_at
                                    ) VALUES (
                                        0.0490,
                                        0.0390,
                                        0.0290,
                                        0.0390,
                                        0.0290,
                                        500000.00,
                                        1000000.00,
                                        null,
                                        500000.00,
                                        null,
                                        0.0490,
                                        0.0390,
                                        0.0290,
                                        0.0390,
                                        0.0290,
                                        500000.00,
                                        1000000.00,
                                        null,
                                        500000.00,
                                        null,
                                        0.0200,
                                        0.0200,
                                        0.0290,
                                        0.0290,
                                        1,
                                        1,
                                        NOW(),
                                        NOW()
                                    );

delimiter //
DROP TRIGGER IF EXISTS auto_insert_fee_settings_per_user//

CREATE TRIGGER auto_insert_fee_settings_per_user AFTER INSERT ON api_v1_user
FOR each ROW
BEGIN
   SET @lastID = NEW.id;
   INSERT INTO api_v1_fee(
        tax_payment_rate,
        payroll_payment_rate,
        deposit_flat_rate,
        payment_flat_rate,
        fee_setting,
        setting_change_next_month,
        user_id,
        created_by,
        updated_by,
        created_at,
        updated_at
        )
    SELECT
        tax_payment_rate,
        payroll_payment_rate,
        deposit_flat_rate,
        payment_flat_rate,
        'flat_rate',
        '',
        @lastID,
        @lastID,
        @lastID,
        NOW(),
        NOW() from api_v1_defaultsettings;

    INSERT INTO api_v1_feetier(
        type,
        tier_no,
        deposit_rate,
        deposit_threshold,
        payment_rate,
        payment_threshold,
        user_id,
        created_by,
        updated_by,
        created_at,
        updated_at
    )
    SELECT
        'v1',
        1,
        tier_1_deposit_rate,
        tier_1_deposit_threshold,
        tier_1_payment_rate,
        tier_1_deposit_threshold,
        @lastID,
        @lastID,
        @lastID,
        NOW(),
        NOW() from api_v1_defaultsettings;

     INSERT INTO api_v1_feetier(
        type,
        tier_no,
        deposit_rate,
        deposit_threshold,
        payment_rate,
        payment_threshold,
        user_id,
        created_by,
        updated_by,
        created_at,
        updated_at
    )
    SELECT
        'v1',
        2,
        tier_2_deposit_rate,
        tier_2_deposit_threshold,
        tier_2_payment_rate,
        tier_2_deposit_threshold,
        @lastID,
        @lastID,
        @lastID,
        NOW(),
        NOW() from api_v1_defaultsettings;

    INSERT INTO api_v1_feetier(
        type,
        tier_no,
        deposit_rate,
        deposit_threshold,
        payment_rate,
        payment_threshold,
        user_id,
        created_by,
        updated_by,
        created_at,
        updated_at
    )
    SELECT
        'v1',
        3,
        tier_3_deposit_rate,
        tier_3_deposit_threshold,
        tier_3_payment_rate,
        tier_3_deposit_threshold,
        @lastID,
        @lastID,
        @lastID,
        NOW(),
        NOW() from api_v1_defaultsettings;

   INSERT INTO api_v1_feetier(
        tier_no,
        type,
        deposit_rate,
        deposit_threshold,
        payment_rate,
        payment_threshold,
        user_id,
        created_by,
        updated_by,
        created_at,
        updated_at
    )
    SELECT
        4,
        'v2',
        tier_4_deposit_rate,
        tier_4_deposit_threshold,
        tier_4_payment_rate,
        tier_4_deposit_threshold,
        @lastID,
        @lastID,
        @lastID,
        NOW(),
        NOW() from api_v1_defaultsettings;

   INSERT INTO api_v1_feetier(
        tier_no,
        type,
        deposit_rate,
        deposit_threshold,
        payment_rate,
        payment_threshold,
        user_id,
        created_by,
        updated_by,
        created_at,
        updated_at
    )
    SELECT
        5,
        'v2',
        tier_5_deposit_rate,
        tier_5_deposit_threshold,
        tier_5_payment_rate,
        tier_5_deposit_threshold,
        @lastID,
        @lastID,
        @lastID,
        NOW(),
        NOW() from api_v1_defaultsettings;
END//

delimiter ;