delimiter //
DROP PROCEDURE IF EXISTS proc_fee_user //
CREATE PROCEDURE proc_fee_user ()
BEGIN
    DECLARE v_user_id INT;
    DECLARE done INT;

    DECLARE cursorUser CURSOR FOR
        select id
        from api_v1_user;

    DECLARE CONTINUE handler FOR NOT FOUND SET done = 1;
    SET done = 0;

    OPEN cursorUser;
    userLoop: loop
        FETCH cursorUser INTO v_user_id;
        IF done = 1 THEN leave userLoop; END IF;

        INSERT INTO api_v1_fee(
            tax_payment_rate,
            payroll_payment_rate,
            fee_setting,
            user_id,
            created_by,
            updated_by,
            created_at,
            updated_at
        )
        SELECT
            tax_payment_rate,
            payroll_payment_rate,
            'fee_tier_v1',
            v_user_id,
            v_user_id,
            v_user_id,
            NOW(),
            NOW() from api_v1_defaultsettings;

        INSERT INTO api_v1_feetier(
            tier_no,
            deposit_rate,
            deposit_threshold,
            payment_rate,
            payment_threshold,
            user_id,
            created_by,
            updated_by,
            created_at,
            updated_at
        )
        SELECT
            1,
            tier_1_deposit_rate,
            tier_1_deposit_threshold,
            tier_1_payment_rate,
            tier_1_deposit_threshold,
            v_user_id,
            v_user_id,
            v_user_id,
            NOW(),
            NOW() from api_v1_defaultsettings;

        INSERT INTO api_v1_feetier(
            tier_no,
            deposit_rate,
            deposit_threshold,
            payment_rate,
            payment_threshold,
            user_id,
            created_by,
            updated_by,
            created_at,
            updated_at
        )
        SELECT
            2,
            tier_2_deposit_rate,
            tier_2_deposit_threshold,
            tier_2_payment_rate,
            tier_2_deposit_threshold,
            v_user_id,
            v_user_id,
            v_user_id,
            NOW(),
            NOW() from api_v1_defaultsettings;

        INSERT INTO api_v1_feetier(
            tier_no,
            deposit_rate,
            deposit_threshold,
            payment_rate,
            payment_threshold,
            user_id,
            created_by,
            updated_by,
            created_at,
            updated_at
        )
        SELECT
            3,
            tier_3_deposit_rate,
            tier_3_deposit_threshold,
            tier_3_payment_rate,
            tier_3_deposit_threshold,
            v_user_id,
            v_user_id,
            v_user_id,
            NOW(),
            NOW() from api_v1_defaultsettings;

    END loop userLoop;
    CLOSE cursorUser;
END//

delimiter ;