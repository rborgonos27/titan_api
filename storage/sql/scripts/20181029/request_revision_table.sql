delimiter //
DROP TRIGGER IF EXISTS request_revisions//

CREATE TRIGGER request_revisions BEFORE UPDATE ON api_v1_request
FOR each ROW
BEGIN
   INSERT INTO api_v1_requestrevisions (
        `request_request_id`
        ,`type`
        ,`status`
        ,`to`
        ,`amount`
        ,`fee`
        ,`remarks`
        ,`due_at`
        ,`pickup_at`
        ,`created_by`
        ,`updated_by`
        ,`created_at`
        ,`updated_at`
        ,`payee_id`
        ,`request_id`
        ,`user_id`
        ,`transaction_type`
   ) values (
        OLD.request_id
        ,OLD.type
        ,OLD.status
        ,OLD.to
        ,OLD.amount
        ,OLD.fee
        ,OLD.remarks
        ,OLD.due_at
        ,OLD.pickup_at
        ,OLD.created_by
        ,OLD.updated_by
        ,OLD.created_at
        ,OLD.updated_at
        ,OLD.payee_id
        ,OLD.id
        ,OLD.user_id
        ,OLD.transaction_type
   );
END//

delimiter ;

