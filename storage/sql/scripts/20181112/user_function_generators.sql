delimiter //
DROP FUNCTION IF EXISTS getMemberAccountId //
CREATE FUNCTION getMemberAccountId
 () RETURNS VARCHAR(20)
BEGIN
    DECLARE memberAccountId VARCHAR(15);
    SET memberAccountId = (
        SELECT UPPER
		(
			CONCAT(
				CHAR(ROUND(RAND()*25)+97),
				CHAR(ROUND(RAND()*25)+97),
				CHAR(ROUND(RAND()*25)+97),
				LPAD(FLOOR(RAND() * 99999999999), 11, '0')
		   	)
	   	)
	 );

	SET @ret = (SELECT UPPER(memberAccountId));
	RETURN @ret;
END //

delimiter ;

-- trigger for user
delimiter //
DROP TRIGGER IF EXISTS custom_autonums_member//

CREATE TRIGGER custom_autonums_member BEFORE INSERT ON api_v1_user
FOR each ROW
BEGIN
   SET NEW.member_id = getNextCustomSeq("member","mbr");
   SET NEW.account_number = geAccountNumberSeq();
   SET NEW.member_account_id = getMemberAccountId();
END//

delimiter ;

-- update api_v1_user set member_account_id = getMemberAccountId() where member_account_id = '';
