delimiter //
DROP TRIGGER IF EXISTS custom_autonums_payee//

CREATE TRIGGER custom_autonums_payee BEFORE INSERT ON api_v1_payee
FOR each ROW
BEGIN
   SET NEW.payee_id = getNextCustomSeq("payee","pye");
END//

delimiter ;