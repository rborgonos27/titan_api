delimiter //
DROP TRIGGER IF EXISTS fee_tier_revisions//

CREATE TRIGGER fee_tier_revisions BEFORE UPDATE ON api_v1_feetier
FOR each ROW
BEGIN
   INSERT INTO api_v1_feetierrevision (
        `tier_no`
        ,`deposit_rate`
        ,`deposit_threshold`
        ,`payment_rate`
        ,`payment_threshold`
        ,`type`
        ,`created_by`
        ,`updated_by`
        ,`created_at`
        ,`updated_at`
        ,`fee_tier_id`
        ,`user_id`
   ) values (
        OLD.tier_no
        ,OLD.deposit_rate
        ,OLD.deposit_threshold
        ,OLD.payment_rate
        ,OLD.payment_threshold
        ,OLD.type
        ,OLD.created_by
        ,OLD.updated_by
        ,OLD.created_at
        ,OLD.updated_at
        ,OLD.id
        ,OLD.user_id
   );
END//

delimiter ;

