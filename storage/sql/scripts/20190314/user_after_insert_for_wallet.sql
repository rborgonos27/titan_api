-- trigger for wallet upon registration
delimiter //
DROP TRIGGER IF EXISTS create_wallet_upon_registration//

CREATE TRIGGER create_wallet_upon_registration
AFTER INSERT
   ON api_v1_user FOR EACH ROW

BEGIN
   -- Insert record into audit table
   INSERT INTO api_v1_wallet
   ( `type`,
     `balance`,
     `user_id`,
     `created_by`,
     `updated_by`,
     `created_at`,
     `updated_at`)
   VALUES
   ('AVAILABLE_BALANCE',
     0,
     NEW.id,
     NEW.id,
     NEW.id,
     NOW(),
     NOW()
     ),
   ('PENDING_DEPOSIT',
     0,
     NEW.id,
     NEW.id,
     NEW.id,
     NOW(),
     NOW()
     ),
    ('PENDING_PAYMENT',
     0,
     NEW.id,
     NEW.id,
     NEW.id,
     NOW(),
     NOW()
     ),
    ('ELECTRONIC_BALANCE',
     0,
     NEW.id,
     NEW.id,
     NEW.id,
     NOW(),
     NOW()
     ),
     ('PAPER_BALANCE',
     0,
     NEW.id,
     NEW.id,
     NEW.id,
     NOW(),
     NOW()
     );

END; //

DELIMITER ;