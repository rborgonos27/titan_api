delimiter //
DROP PROCEDURE IF EXISTS proc_wallet_user //
CREATE PROCEDURE proc_wallet_user ()
BEGIN
    DECLARE v_user_id INT;
    DECLARE done INT;

    DECLARE cursorUser CURSOR FOR
        select id
        from api_v1_user;

    DECLARE CONTINUE handler FOR NOT FOUND SET done = 1;
    SET done = 0;

    OPEN cursorUser;
    userLoop: loop
        FETCH cursorUser INTO v_user_id;
        IF done = 1 THEN leave userLoop; END IF;

        INSERT INTO api_v1_wallet(
            type,
            balance,
            user_id,
            created_by,
            updated_by,
            created_at,
            updated_at
        )
        SELECT
            'ELECTRONIC_BALANCE',
            0,
            v_user_id,
            v_user_id,
            v_user_id,
            NOW(),
            NOW();

        INSERT INTO api_v1_wallet(
            type,
            balance,
            user_id,
            created_by,
            updated_by,
            created_at,
            updated_at
        )
        SELECT
            'PAPER_BALANCE',
            0,
            v_user_id,
            v_user_id,
            v_user_id,
            NOW(),
            NOW();

    END loop userLoop;
    CLOSE cursorUser;
END//

delimiter ;
