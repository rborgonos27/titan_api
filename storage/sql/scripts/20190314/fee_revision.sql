delimiter //
DROP TRIGGER IF EXISTS fee_revisions//

CREATE TRIGGER fee_revisions BEFORE UPDATE ON api_v1_fee
FOR each ROW
BEGIN
   INSERT INTO api_v1_feerevision (
        `fee_id`
        ,`tax_payment_rate`
        ,`payroll_payment_rate`
        ,`deposit_flat_rate`
        ,`payment_flat_rate`
        ,`fee_setting`
        ,`created_by`
        ,`updated_by`
        ,`created_at`
        ,`updated_at`
        ,`fee_settting_id`
        ,`user_id`
   ) values (
        OLD.fee_id
        ,OLD.tax_payment_rate
        ,OLD.payroll_payment_rate
        ,OLD.deposit_flat_rate
        ,OLD.payment_flat_rate
        ,OLD.fee_setting
        ,OLD.created_by
        ,OLD.updated_by
        ,OLD.created_at
        ,OLD.updated_at
        ,OLD.id
        ,OLD.user_id
   );
END//

delimiter ;

