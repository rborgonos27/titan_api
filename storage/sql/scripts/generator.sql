-- create titan custom generator
-- note this is special table used only on the background
CREATE TABLE IF NOT EXISTS api_v1_sequence
(
    seq_name VARCHAR(50) NOT NULL PRIMARY KEY,
    seq_group VARCHAR(10) NOT NULL,
    seq_val INT UNSIGNED NOT NULL
);


-- function to use to generate custom sequence number
delimiter //
DROP FUNCTION IF EXISTS getNextCustomSeq//
CREATE FUNCTION getNextCustomSeq
(
    sSeqName VARCHAR(50),
    sSeqGroup VARCHAR(10)
) RETURNS VARCHAR(20)
BEGIN
    DECLARE nLast_val INT;

    SET nLast_val =  (SELECT seq_val
                          FROM api_v1_sequence
                          WHERE seq_name = sSeqName
                                AND seq_group = sSeqGroup);
    IF nLast_val IS NULL THEN
        SET nLast_val = 1;
        INSERT INTO api_v1_sequence (seq_name,seq_group,seq_val)
        VALUES (sSeqName,sSeqGroup,nLast_Val);
    ELSE
        SET nLast_val = nLast_val + 1;
        UPDATE api_v1_sequence SET seq_val = nLast_val
        WHERE seq_name = sSeqName AND seq_group = sSeqGroup;
    END IF;

    SET @ret = (SELECT concat(sSeqGroup,'-',lpad(nLast_val,6,'0')));
    RETURN @ret;
END//

delimiter ;


-- stored procedure to modify the current sequence
delimiter //
DROP PROCEDURE IF EXISTS sp_setCustomVal//
CREATE PROCEDURE sp_setCustomVal(sSeqName VARCHAR(50),
              sSeqGroup VARCHAR(10), nVal INT UNSIGNED)
BEGIN
    IF (SELECT COUNT(*) FROM api_v1_sequence
            WHERE seq_name = sSeqName
                AND seq_group = sSeqGroup) = 0 THEN
        INSERT INTO api_v1_sequence (seq_name,seq_group,seq_val)
        VALUES (sSeqName,sSeqGroup,nVal);
    ELSE
        UPDATE api_v1_sequence SET seq_val = nVal
        WHERE seq_name = sSeqName AND seq_group = sSeqGroup;
    END IF;
END//
delimiter ;


-- trigger for user
delimiter //
DROP TRIGGER IF EXISTS custom_autonums_member//

CREATE TRIGGER custom_autonums_member BEFORE INSERT ON api_v1_user
FOR each ROW
BEGIN
   SET NEW.member_id = getNextCustomSeq("member","mbr");
   SET NEW.account_number = geAccountNumberSeq();
   SET NEW.member_account_id = getMemberAccountId();
   IF (NEW.user_type = 'member') THEN
    SET NEW.location_id = getLocationNumberSeq();
   END IF;
END//

delimiter ;


-- trigger for deposit
delimiter //
DROP TRIGGER IF EXISTS custom_autonums_deposit//

CREATE TRIGGER custom_autonums_deposit BEFORE INSERT ON api_v1_deposit
FOR each ROW
BEGIN
   SET NEW.transaction_id = getNextCustomSeq("deposit","dpt");
END//

delimiter ;

-- trigger for payment
delimiter //
DROP TRIGGER IF EXISTS custom_autonums_payment//

CREATE TRIGGER custom_autonums_payment BEFORE INSERT ON api_v1_payment
FOR each ROW
BEGIN
   SET NEW.transaction_id = getNextCustomSeq("payment","pyt");
END//

delimiter ;

-- trigger for request
delimiter //
DROP TRIGGER IF EXISTS custom_autonums_request//

CREATE TRIGGER custom_autonums_request BEFORE INSERT ON api_v1_request
FOR each ROW
BEGIN
   SET NEW.request_id = getNextCustomSeq("request","req");
END//

delimiter ;

-- trigger for transaction
delimiter //
DROP TRIGGER IF EXISTS custom_autonums_transaction//

CREATE TRIGGER custom_autonums_transaction BEFORE INSERT ON api_v1_transaction
FOR each ROW
BEGIN
   SET NEW.transaction_id = getNextCustomSeq("transaction","trn");
END//

delimiter ;

-- function to use to generate custom sequence number
delimiter //
DROP FUNCTION IF EXISTS geAccountNumberSeq//
CREATE FUNCTION geAccountNumberSeq
() RETURNS VARCHAR(20)
BEGIN
    DECLARE nLast_val INT;

    SET nLast_val =  (SELECT seq_val
                          FROM api_v1_sequence
                          WHERE seq_name = 'ACCOUNT_NUMBER');
    IF nLast_val IS NULL THEN
        SET nLast_val = 1;
        INSERT INTO api_v1_sequence (seq_name,seq_group,seq_val)
        VALUES ('ACCOUNT_NUMBER','',nLast_Val);
    ELSE
        SET nLast_val = nLast_val + 1;
        UPDATE api_v1_sequence SET seq_val = nLast_val
        WHERE seq_name = 'ACCOUNT_NUMBER';
    END IF;

    SET @ret = (SELECT lpad(nLast_val,10,'0'));
    RETURN @ret;
END//

delimiter ;

delimiter //
DROP FUNCTION IF EXISTS getMemberAccountId //
CREATE FUNCTION getMemberAccountId
 () RETURNS VARCHAR(20)
BEGIN
    DECLARE memberAccountId VARCHAR(15);
    SET memberAccountId = (
        SELECT UPPER
		(
			CONCAT(
				CHAR(ROUND(RAND()*25)+97),
				CHAR(ROUND(RAND()*25)+97),
				CHAR(ROUND(RAND()*25)+97),
				LPAD(FLOOR(RAND() * 99999999999), 11, '0')
		   	)
	   	)
	 );

	SET @ret = (SELECT UPPER(memberAccountId));
	RETURN @ret;
END //

-- trigger for wallet
delimiter //
DROP TRIGGER IF EXISTS custom_autonums_wallet//

CREATE TRIGGER custom_autonums_wallet BEFORE INSERT ON api_v1_wallet
FOR each ROW
BEGIN
   SET NEW.wallet_id = getNextCustomSeq("wallet","wlt");
END//

delimiter ;

-- trigger for ledger
delimiter //
DROP TRIGGER IF EXISTS custom_autonums_ledger//

CREATE TRIGGER custom_autonums_ledger BEFORE INSERT ON api_v1_ledger
FOR each ROW
BEGIN
   SET NEW.ledger_id = getNextCustomSeq("ledger","lgr");
END//

delimiter ;


-- trigger for wallet upon registration
delimiter //
DROP TRIGGER IF EXISTS create_wallet_upon_registration//

CREATE TRIGGER create_wallet_upon_registration
AFTER INSERT
   ON api_v1_user FOR EACH ROW

BEGIN
   -- Insert record into audit table
   INSERT INTO api_v1_wallet
   ( `type`,
     `balance`,
     `user_id`,
     `created_by`,
     `updated_by`,
     `created_at`,
     `updated_at`)
   VALUES
   ('AVAILABLE_BALANCE',
     0,
     NEW.id,
     NEW.id,
     NEW.id,
     NOW(),
     NOW()
     ),
   ('PENDING_DEPOSIT',
     0,
     NEW.id,
     NEW.id,
     NEW.id,
     NOW(),
     NOW()
     ),
    ('PENDING_PAYMENT',
     0,
     NEW.id,
     NEW.id,
     NEW.id,
     NOW(),
     NOW()
     );

END; //

DELIMITER ;

-- trigger for fee
delimiter //
DROP TRIGGER IF EXISTS custom_autonums_fee//

CREATE TRIGGER custom_autonums_fee BEFORE INSERT ON api_v1_fee
FOR each ROW
BEGIN
   SET NEW.fee_id = getNextCustomSeq("fee","fee");
END//

delimiter ;
-- trigger for fee_transaction
delimiter //
DROP TRIGGER IF EXISTS custom_autonums_fee_transaction//

CREATE TRIGGER custom_autonums_fee_transaction BEFORE INSERT ON api_v1_feetransaction
FOR each ROW
BEGIN
   SET NEW.fee_transaction_id = getNextCustomSeq("tra","tra");
END//

delimiter ;

DELETE FROM api_v1_defaultsettings;

INSERT INTO api_v1_defaultsettings (
                                    tier_1_deposit_rate,
                                    tier_2_deposit_rate,
                                    tier_3_deposit_rate,
                                    tier_4_deposit_rate,
                                    tier_5_deposit_rate,
                                    tier_1_deposit_threshold,
                                    tier_2_deposit_threshold,
                                    tier_3_deposit_threshold,
                                    tier_4_deposit_threshold,
                                    tier_5_deposit_threshold,
                                    tier_1_payment_rate,
                                    tier_2_payment_rate,
                                    tier_3_payment_rate,
                                    tier_4_payment_rate,
                                    tier_5_payment_rate,
                                    tier_1_payment_threshold,
                                    tier_2_payment_threshold,
                                    tier_3_payment_threshold,
                                    tier_4_payment_threshold,
                                    tier_5_payment_threshold,
                                    tax_payment_rate,
                                    payroll_payment_rate,
                                    deposit_flat_rate,
                                    payment_flat_rate,
                                    created_by,
                                    updated_by,
                                    created_at,
                                    updated_at
                                    ) VALUES (
                                        0.0490,
                                        0.0390,
                                        0.0290,
                                        0.0390,
                                        0.0290,
                                        500000.00,
                                        1000000.00,
                                        null,
                                        500000.00,
                                        null,
                                        0.0490,
                                        0.0390,
                                        0.0290,
                                        0.0390,
                                        0.0290,
                                        500000.00,
                                        1000000.00,
                                        null,
                                        500000.00,
                                        null,
                                        0.0200,
                                        0.0200,
                                        0.0290,
                                        0.0290,
                                        1,
                                        1,
                                        NOW(),
                                        NOW()
                                    );

delimiter //
DROP TRIGGER IF EXISTS auto_insert_fee_settings_per_user//

CREATE TRIGGER auto_insert_fee_settings_per_user AFTER INSERT ON api_v1_user
FOR each ROW
BEGIN
   SET @lastID = NEW.id;
   INSERT INTO api_v1_fee(
        tax_payment_rate,
        payroll_payment_rate,
        deposit_flat_rate,
        payment_flat_rate,
        fee_setting,
        setting_change_next_month,
        user_id,
        created_by,
        updated_by,
        created_at,
        updated_at
        )
    SELECT
        tax_payment_rate,
        payroll_payment_rate,
        deposit_flat_rate,
        payment_flat_rate,
        'flat_rate',
        '',
        @lastID,
        @lastID,
        @lastID,
        NOW(),
        NOW() from api_v1_defaultsettings;

    INSERT INTO api_v1_feetier(
        type,
        tier_no,
        deposit_rate,
        deposit_threshold,
        payment_rate,
        payment_threshold,
        user_id,
        created_by,
        updated_by,
        created_at,
        updated_at
    )
    SELECT
        'v1',
        1,
        tier_1_deposit_rate,
        tier_1_deposit_threshold,
        tier_1_payment_rate,
        tier_1_deposit_threshold,
        @lastID,
        @lastID,
        @lastID,
        NOW(),
        NOW() from api_v1_defaultsettings;

     INSERT INTO api_v1_feetier(
        type,
        tier_no,
        deposit_rate,
        deposit_threshold,
        payment_rate,
        payment_threshold,
        user_id,
        created_by,
        updated_by,
        created_at,
        updated_at
    )
    SELECT
        'v1',
        2,
        tier_2_deposit_rate,
        tier_2_deposit_threshold,
        tier_2_payment_rate,
        tier_2_deposit_threshold,
        @lastID,
        @lastID,
        @lastID,
        NOW(),
        NOW() from api_v1_defaultsettings;

    INSERT INTO api_v1_feetier(
        type,
        tier_no,
        deposit_rate,
        deposit_threshold,
        payment_rate,
        payment_threshold,
        user_id,
        created_by,
        updated_by,
        created_at,
        updated_at
    )
    SELECT
        'v1',
        3,
        tier_3_deposit_rate,
        tier_3_deposit_threshold,
        tier_3_payment_rate,
        tier_3_deposit_threshold,
        @lastID,
        @lastID,
        @lastID,
        NOW(),
        NOW() from api_v1_defaultsettings;

   INSERT INTO api_v1_feetier(
        tier_no,
        type,
        deposit_rate,
        deposit_threshold,
        payment_rate,
        payment_threshold,
        user_id,
        created_by,
        updated_by,
        created_at,
        updated_at
    )
    SELECT
        4,
        'v2',
        tier_4_deposit_rate,
        tier_4_deposit_threshold,
        tier_4_payment_rate,
        tier_4_deposit_threshold,
        @lastID,
        @lastID,
        @lastID,
        NOW(),
        NOW() from api_v1_defaultsettings;

   INSERT INTO api_v1_feetier(
        tier_no,
        type,
        deposit_rate,
        deposit_threshold,
        payment_rate,
        payment_threshold,
        user_id,
        created_by,
        updated_by,
        created_at,
        updated_at
    )
    SELECT
        5,
        'v2',
        tier_5_deposit_rate,
        tier_5_deposit_threshold,
        tier_5_payment_rate,
        tier_5_deposit_threshold,
        @lastID,
        @lastID,
        @lastID,
        NOW(),
        NOW() from api_v1_defaultsettings;
END//

delimiter ;

delimiter //
DROP PROCEDURE IF EXISTS proc_fee_user_v2 //
CREATE PROCEDURE proc_fee_user_v2 ()
BEGIN
    DECLARE v_user_id INT;
    DECLARE done INT;

    DECLARE cursorUser CURSOR FOR
        select id
        from api_v1_user;

    DECLARE CONTINUE handler FOR NOT FOUND SET done = 1;
    SET done = 0;

    OPEN cursorUser;
    userLoop: loop
        FETCH cursorUser INTO v_user_id;
        IF done = 1 THEN leave userLoop; END IF;

        INSERT INTO api_v1_feetier(
            type,
            tier_no,
            deposit_rate,
            deposit_threshold,
            payment_rate,
            payment_threshold,
            user_id,
            created_by,
            updated_by,
            created_at,
            updated_at
        )
        SELECT
            'v2',
            4,
            tier_4_deposit_rate,
            tier_4_deposit_threshold,
            tier_4_payment_rate,
            tier_4_deposit_threshold,
            v_user_id,
            v_user_id,
            v_user_id,
            NOW(),
            NOW() from api_v1_defaultsettings;

        INSERT INTO api_v1_feetier(
            type,
            tier_no,
            deposit_rate,
            deposit_threshold,
            payment_rate,
            payment_threshold,
            user_id,
            created_by,
            updated_by,
            created_at,
            updated_at
        )
        SELECT
            'v2',
            5,
            tier_5_deposit_rate,
            tier_5_deposit_threshold,
            tier_5_payment_rate,
            tier_5_deposit_threshold,
            v_user_id,
            v_user_id,
            v_user_id,
            NOW(),
            NOW() from api_v1_defaultsettings;

    END loop userLoop;
    CLOSE cursorUser;
END//

delimiter ;

delimiter //
DROP TRIGGER IF EXISTS custom_autonums_payee//

CREATE TRIGGER custom_autonums_payee BEFORE INSERT ON api_v1_payee
FOR each ROW
BEGIN
   SET NEW.payee_id = getNextCustomSeq("payee","pye");
END//

delimiter ;

delimiter //
DROP TRIGGER IF EXISTS request_revisions//

CREATE TRIGGER request_revisions BEFORE UPDATE ON api_v1_request
FOR each ROW
BEGIN
   INSERT INTO api_v1_requestrevisions (
        `request_request_id`
        ,`type`
        ,`status`
        ,`to`
        ,`amount`
        ,`fee`
        ,`remarks`
        ,`due_at`
        ,`pickup_at`
        ,`created_by`
        ,`updated_by`
        ,`created_at`
        ,`updated_at`
        ,`payee_id`
        ,`request_id`
        ,`user_id`
        ,`transaction_type`
   ) values (
        OLD.request_id
        ,OLD.type
        ,OLD.status
        ,OLD.to
        ,OLD.amount
        ,OLD.fee
        ,OLD.remarks
        ,OLD.due_at
        ,OLD.pickup_at
        ,OLD.created_by
        ,OLD.updated_by
        ,OLD.created_at
        ,OLD.updated_at
        ,OLD.payee_id
        ,OLD.id
        ,OLD.user_id
        ,OLD.transaction_type
   );
END//

delimiter ;

UPDATE api_v1_payee set status = 'APPROVED' where status is null;

insert into api_v1_sequence (seq_name, seq_group, seq_val) values ('LOCATION_NUMBER','',10016);

delimiter //
DROP FUNCTION IF EXISTS getLocationNumberSeq//
CREATE FUNCTION getLocationNumberSeq
() RETURNS VARCHAR(20)
BEGIN
    DECLARE nLast_val INT;

    SET nLast_val =  (SELECT seq_val
                          FROM api_v1_sequence
                          WHERE seq_name = 'LOCATION_NUMBER');
    IF nLast_val IS NULL THEN
        SET nLast_val = 1;
        INSERT INTO api_v1_sequence (seq_name,seq_group,seq_val)
        VALUES ('LOCATION_NUMBER','',nLast_Val);
    ELSE
        SET nLast_val = nLast_val + 1;
        UPDATE api_v1_sequence SET seq_val = nLast_val
        WHERE seq_name = 'LOCATION_NUMBER';
    END IF;

    SET @ret = (SELECT nLast_val);
    RETURN @ret;
END//

delimiter ;

delimiter //
DROP TRIGGER IF EXISTS custom_location_sequence_member//

CREATE TRIGGER custom_location_sequence_member BEFORE UPDATE ON api_v1_user
FOR each ROW
BEGIN
    IF NEW.user_type = 'member' && NEW.is_first_time_member = 1 && OLD.location_id IS NULL THEN
        SET NEW.location_id = getLocationNumberSeq();
    END IF;
END//

delimiter ;

--tessa@plusproducts.com
--abinashi@io7mgmt.com
--info@jadeNectar.com
--brandon@getmeadow.com
--richard@harvestpacificfarms.com
--Maggie@thcdesign.com
--jozee@etheridgefarms.us
--glowingbuddhachocolates@gmail.com
--constance@constancecbd.com
--sadie@thehigherconscious.org
--wil@privatereserve.org
--alanbramson@gmail.com
--ryan@flowkana.com
