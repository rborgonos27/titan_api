-- trigger for fee
delimiter //
DROP TRIGGER IF EXISTS custom_autonums_fee//

CREATE TRIGGER custom_autonums_fee BEFORE INSERT ON api_v1_fee
FOR each ROW
BEGIN
   SET NEW.fee_id = getNextCustomSeq("fee","fee");
END//

delimiter ;
-- trigger for fee_transaction
delimiter //
DROP TRIGGER IF EXISTS custom_autonums_fee_transaction//

CREATE TRIGGER custom_autonums_fee_transaction BEFORE INSERT ON api_v1_feetransaction
FOR each ROW
BEGIN
   SET NEW.fee_transaction_id = getNextCustomSeq("tra","tra");
END//

delimiter ;