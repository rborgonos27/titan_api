-- delete transactions, revisions and requests
DELETE FROM `api_v1_requestrevisions`;
DELETE FROM `api_v1_transaction`;
DELETE FROM `api_v1_request`;
DELETE FROM `api_v1_ledger`;

-- reset series
UPDATE `api_v1_sequence` SET `seq_val`='0' WHERE `seq_name`='request';
UPDATE `api_v1_sequence` SET `seq_val`='0' WHERE `seq_name`='transaction';
UPDATE `api_v1_sequence` SET `seq_val`='0' WHERE `seq_name`='ledger';

-- reset series
UPDATE `api_v1_wallet` SET `balance`='0';