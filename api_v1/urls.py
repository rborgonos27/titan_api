from django.urls import path, re_path
from django.conf.urls import include

from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register('user', views.UserViewSet, 'User')
router.register('deposit', views.DepositViewSet)
router.register('payment', views.PaymentViewSet)
router.register('request/detail', views.RequestDetailViewSet)
router.register('file', views.FileAPI)
router.register('file_request', views.FileRequestViewset)
router.register('payee', views.PayeeViewset)
router.register('user_payee', views.UserPayeeView)
router.register('fee', views.FeeViewset, 'Fee')
router.register('user_file', views.UserFileView)
router.register('payee_file', views.PayeeFileView)
router.register('fee_transaction', views.FeeTransactionViewset)
router.register('fee_tier', views.FeeTierViewset, 'FeeTier')
router.register('payee_agreement', views.PayeeAgreementViewSet, 'PayeeAgreement')
router.register('member_statement', views.MemberStatementViewSet, 'MemberStatement')
router.register('member_user', views.UserMemberViewSet, 'MemberStatement')

urlpatterns = [
    path('', include(router.urls)),
    path('request/', views.RequestAPI.as_view(), name='request'),
    path('request_history/', views.RequestHistoryAPI.as_view(), name='request_history'),
    path('admin/user/detail/', views.UserDetailAPI.as_view(), name='user_detail'),
    path('member_wallet/', views.MemberWalletView.as_view(), name='user_wallet'),
    path('top_request/', views.TopRequestDetailsApi.as_view(), name='top_request'),
    re_path(r'^request/(?P<pk>\d+)/$', views.RequestAPI.as_view(), name='request'),
    path('transaction/', views.TransactionAPI.as_view(), name='transaction'),
    re_path(r'^profile/user/(?P<pk>\d+)/$', views.userAPI.as_view(), name='user_profile'),
    path('requestrevisions/', views.RequestRevisionAPI.as_view(), name='request_revisions'),
    re_path(r'^transaction/(?P<pk>\d+)/$', views.TransactionAPI.as_view(), name='transaction'),
    re_path(r'^user/account/(?P<pk>\d+)/$', views.UserUpdateAccount.as_view(), name='user_account'),
    path('wallet/', views.WalletAPI.as_view(), name='wallet'),
    path('current_user/', views.currentUser.as_view()),
    path('admin/request/', views.AdminRequestAPI.as_view()),
    path('payee_details/', views.PayeeDetailsListAPI.as_view()),
    path('time/', views.TimeZoneView.as_view()),
    path('backup/', views.FileRequestAPI.as_view()),
    path('user_picture/', views.UserProfilePicture.as_view()),
    path('contact/', views.ContactView.as_view()),
    path('athena_contact/', views.AthenaContactView.as_view()),
    path('new/user/notif', views.UserNotifcation.as_view()),
    path('new/payee/notif', views.NewPayeeRequestNotification.as_view()),
    path('approve/payee/notif', views.ApprovePayeeRequestNotification.as_view()),
    path('update/user/notif', views.UserUpdateNotification.as_view()),
    path('member/user/notif', views.UserMemberNotification.as_view()),
    path('new/request/notif', views.NewRequestNotification.as_view()),
    path('update/request/notif', views.RequestUpdateNotification.as_view()),
    path('payee_user/', views.PayeeUserApi.as_view()),
    path('payee_deposit_account/', views.DepositPayeeAccount.as_view()),
    path('payee_notification/', views.PayeeNotification.as_view()),
    path('send_message/', views.SendMessageMember.as_view()),
    path('fee_transaction_user/', views.FeeTransactionApi.as_view()),
    path('log/', views.ComputationLogView.as_view()),
    path('compute_fee/', views.ComputationFeeView.as_view()),
    path('reset_password/', views.ResetPasswordAPI.as_view()),
    path('send_attempts/', views.SendMaximumAttemptEmail.as_view()),
    path('forgot_password/', views.ForgotPassword.as_view()),
    path('account_summary/', views.AccountSummaryApi.as_view()),
    path('upload_signature/', views.UploadSignature.as_view()),
    path('payee_view/', views.PayeeNoAuth.as_view()),
    path('payee_user_agreement/', views.PayeeUserAgreementApi.as_view()),
    path('state_cities/', views.StateCitiesAPI.as_view()),
    path('csv_request/', views.RequestCsv.as_view()),
    path('csv_user/', views.UserCsv.as_view()),
    path('user_member/', views.UserMemberAPI.as_view()),
    path('payee_duplicate/', views.PayeeDuplicate.as_view()),
    path('move_user_payee/', views.MovePayeeRequest.as_view()),
    path('generate_statement/', views.GenerateStatementAPI.as_view()),
    path('enable_payee/', views.EnablePayee.as_view()),
    path('enable_user/', views.EnableUser.as_view()),
    path('tools/clear_email/', views.ClearUserRecord.as_view()),
    path('assign_role/', views.AssignRoleApi.as_view()),
    re_path(r'^revert_request/(?P<pk>\d+)/$', views.RevertRequestAPI.as_view(), name='revert_request'),
]
