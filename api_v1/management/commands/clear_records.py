from django.conf import settings
from utils.clear_record import ClearRecord
import os
from django.core.management.base import BaseCommand


def clear_records(email):

    ClearRecord.clear(email)
    return None


class Command(BaseCommand):
    requires_system_checks = False

    def add_arguments(self, parser):
        parser.add_argument('email')

    def handle(self, **options):
        email = options['email']
        clear_records(email)
