from django.template.loader import get_template
from django.core.mail import EmailMessage
from django.conf import settings
from django.db.models import Q

from django.core.management.base import BaseCommand
from datetime import date, timedelta
from ...models import Request
from ...serializers import RequestSerializer


def payment_due():
    current_date = date.today().strftime('%Y-%m-%d')
    next_three_days = date.today()+timedelta(days=3)
    requests = Request.objects.filter(type='PAYMENT')\
        .filter(~Q(status='COMPLETED'))\
        .filter(due_at__range=['{date} {time}'.format(date=current_date, time='00:00'),
                               '{date} {time}'.format(date=next_three_days, time='23:59')])

    request_data = RequestSerializer(requests, many=True)

    if len(requests) > 0:
        subject = 'Payments Request Due'
        to = [settings.TITAN_ALERT_EMAIL]
        from_email = 'Titan Vault <no-reply@zeuss.tech>'
        reply_to = [settings.TITAN_REPLY_TO_EMAIL]

        ctx = {
            'name': 'Titan Officer',
            'link': "{server}/request/".format(server=settings.APP_URL),
            'requests': request_data.data,
        }

        message = get_template('payment_due_alert.html').render(ctx)
        msg = EmailMessage(subject, message, to=to, from_email=from_email, reply_to=reply_to)
        msg.content_subtype = 'html'

        msg.send()


class Command(BaseCommand):
    def handle(self, **options):
        payment_due()

