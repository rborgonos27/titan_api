from django.conf import settings
import os
from ...models import User, Payee

from django.core.management.base import BaseCommand


def migrate(host, user, password):
    db_command = "mysql -h{server} -u{user} -p{password} < storage/sql/initial_db_v2.sql".format(
        server=host,
        user=user,
        password=password
    )
    print(db_command)
    os.system(db_command)
    print('database created...')
    migrate_scripts = 'python manage.py migrate'
    os.system(migrate_scripts)
    print('migrated...')
    print('running generators...')

    generator_command = "mysql -h{server} -u{user} -p{password} {database} < storage/sql/scripts/generator.sql".format(
        server=settings.DATABASES['default']['HOST'],
        user=settings.DATABASES['default']['USER'],
        password=settings.DATABASES['default']['PASSWORD'],
        database=settings.DATABASES['default']['NAME']
    )

    os.system(generator_command)
    print('running generators completed...')

    print('create initial user...')
    user = User.objects.filter(id=1)
    if not user:
        user = User(
            email='admin@titan-vault.com',
            first_name='Titan Admin',
            last_name='Titan Admin',
            username='admin',
            user_type='super_admin',
            is_first_time_member=0,
        )

        user.set_password('password123')
        user.save()

    print('create default payee')

    payee = Payee.objects.filter(id=1)
    if not payee:
        payee = Payee(
            name='Fake Payee Account',
            bank_name='Fake Bank',
            bank_account_name='Ekaf Eman',
            bank_account='0002300221',
            email='admin@titan-vault.com',
            bank_routing_number='2333233232',
            address='Palo Alto',
            created_by=1,
            updated_by=1,
        )

        payee.save()

    print('create default payee done...')

    return None


class Command(BaseCommand):
    requires_system_checks = False

    def add_arguments(self, parser):
        parser.add_argument('root_db_host')
        parser.add_argument('root_db_user')
        parser.add_argument('root_db_password')

    def handle(self, **options):
        host = options['root_db_host']
        user = options['root_db_user']
        password = options['root_db_password']

        migrate(host, user, password)
