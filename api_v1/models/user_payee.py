from django.db import models
import datetime

from . user import User
from . payee import Payee


class UserPayee(models.Model):
    payee = models.ForeignKey(Payee, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_by = models.IntegerField(blank=True)
    updated_by = models.IntegerField(blank=True)
    created_at = models.DateTimeField(default=datetime.datetime.now, blank=True)
    updated_at = models.DateTimeField(default=datetime.datetime.now, blank=True)

    def __str__(self):
        return self

    class Meta(object):
        app_label = 'api_v1'
        unique_together = ('payee', 'user')
