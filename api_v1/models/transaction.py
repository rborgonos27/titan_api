from django.db import models
from . user import User
from . request import Request

import datetime


class Transaction(models.Model):
    STATUS = [
        ("PENDING", "PENDING"),
        ("SCHEDULED", "SCHEDULED"),
        ("REQUESTED", "REQUESTED"),
        ("COMPLETED", "COMPLETED"),
        ("VOID", "VOID"),
    ]
    TYPE = [
        ("PAYMENT", "PAYMENT"),
        ("DEPOSIT", "DEPOSIT"),
        ("PICKUP", "PICKUP"),
        ("TRANSFER", "TRANSFER"),
        ("UPDATE", "UPDATE"),
    ]

    transaction_id = models.TextField(max_length=14, blank=False)
    request = models.ForeignKey(Request, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    type = models.CharField(max_length=20, choices=TYPE)
    status = models.CharField(max_length=20, choices=STATUS)
    to = models.TextField(max_length=50, blank=True)
    amount = models.DecimalField(max_digits=9, decimal_places=2)
    remarks = models.TextField(blank=True)
    process_at = models.DateTimeField(default=datetime.datetime.now, blank=False)
    created_by = models.IntegerField(blank=True)
    updated_by = models.IntegerField(blank=True)
    created_at = models.DateTimeField(default=datetime.datetime.now, blank=True)
    updated_at = models.DateTimeField(default=datetime.datetime.now, blank=True)

    def __str__(self):
        return self

    class Meta(object):
        app_label = 'api_v1'
