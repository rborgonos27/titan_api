from django.db import models
import datetime


class Payee(models.Model):
    # status APPROVED, REQUESTED, DECLINED
    payee_id = models.TextField(max_length=14, blank=False)
    name = models.TextField(max_length=100, blank=False)
    bank_name = models.TextField(max_length=100, blank=False)
    bank_account_name = models.TextField(max_length=100, blank=False)
    bank_account = models.TextField(max_length=100, blank=False)
    bank_routing_number = models.TextField(max_length=100, blank=False)
    email = models.TextField(max_length=80, blank=True, default=None)
    address = models.TextField(max_length=100, blank=True)
    address_2 = models.TextField(max_length=80, blank=True, null=True)
    status = models.TextField(max_length=30, blank=True, null=True, default='APPROVED')
    city = models.TextField(max_length=80, blank=True, null=True)
    state = models.TextField(max_length=80, blank=True, null=True)
    zip_code = models.TextField(max_length=80, blank=True, null=True)
    created_by = models.IntegerField(blank=True)
    updated_by = models.IntegerField(blank=True)
    deleted_at = models.DateTimeField(null=True, blank=True)
    created_at = models.DateTimeField(default=datetime.datetime.now, blank=True)
    updated_at = models.DateTimeField(default=datetime.datetime.now, blank=True)

    def __str__(self):
        return self

    class Meta(object):
        app_label = 'api_v1'
