from django.db import models
import datetime

from . user import User


class MemberStatement(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    cutoff_at = models.DateTimeField(default=datetime.datetime.now, blank=True)
    created_by = models.IntegerField(blank=True)
    updated_by = models.IntegerField(blank=True)
    created_at = models.DateTimeField(default=datetime.datetime.now, blank=True)
    updated_at = models.DateTimeField(default=datetime.datetime.now, blank=True)

    def __str__(self):
        return self.id

    class Meta(object):
        app_label = 'api_v1'
