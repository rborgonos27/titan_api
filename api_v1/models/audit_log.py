from django.db import models
import datetime

from . user import User


class AuditLog(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    module = models.TextField(max_length=30, blank=False)
    transaction = models.TextField(max_length=30, blank=False)
    description = models.TextField(max_length=30, blank=False)
    log_at = models.DateTimeField(default=datetime.datetime.now, blank=True)

    def __str__(self):
        return self

    class Meta(object):
        app_label = 'api_v1'
