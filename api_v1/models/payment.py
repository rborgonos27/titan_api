from django.db import models
import datetime

from . user import User


class Payment(models.Model):
    TRANSACTION_STATUS_CHOICES = [("completed","completed"), ("pending","pending"), ("void","void")]

    transaction_id = models.TextField(max_length=14, blank=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    pay_to = models.ForeignKey(User, related_name='pay_to_member', on_delete=models.CASCADE)
    pay_at = models.DateTimeField(default=datetime.datetime.now, blank=True)
    status = models.CharField(max_length=10, choices=TRANSACTION_STATUS_CHOICES)
    amount = models.DecimalField(max_digits=9, decimal_places=2)
    notes = models.CharField(max_length=100, blank=True)
    created_by = models.IntegerField(blank=True)
    updated_by = models.IntegerField(blank=True)
    created_at = models.DateTimeField(default=datetime.datetime.now, blank=True)
    updated_at = models.DateTimeField(default=datetime.datetime.now, blank=True)

    def __str__(self):
        return self

    class Meta(object):
        app_label = 'api_v1'
