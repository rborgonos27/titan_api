from django.contrib.auth.models import AbstractUser

from django.db import models


class User(AbstractUser):
    USER_TYPES = (
        ("super_admin", "super_admin"),
        ("staff", "staff"),
        ("member", "member"),
        ("sub_member", "sub_member"),
        ("temporary", "temporary")
    )

    member_id = models.TextField(max_length=14, blank=True)
    member_account_id = models.TextField(max_length=15, blank=True)
    account_number = models.TextField(max_length=14, blank=True)
    first_name = models.TextField(max_length=50, blank=False)
    middle_name = models.TextField(max_length=50, blank=True)
    last_name = models.TextField(max_length=50, blank=False)
    email = models.EmailField(max_length=50, unique=True)
    qualifier = models.TextField(max_length=5, blank=True)
    gender = models.TextField(max_length=1, blank=True)
    is_verified = models.BooleanField(default=False)
    is_first_time_member = models.BooleanField(default=False)
    birth_date = models.DateField(null=True, blank=True)
    primary_contact_name = models.TextField(max_length=50, blank=True)
    primary_contact_number = models.TextField(max_length=20, blank=True)
    business_name = models.TextField(max_length=50, blank=True)
    business_contact_number = models.TextField(max_length=20, blank=True)
    address = models.TextField(max_length=80, blank=True)
    address_2 = models.TextField(max_length=80, blank=True, null=True)
    city = models.TextField(max_length=80, blank=True, null=True)
    state = models.TextField(max_length=80, blank=True, null=True)
    zip_code = models.TextField(max_length=80, blank=True, null=True)
    principal_city_of_business = models.TextField(max_length=50, blank=True)
    user_type = models.CharField(choices=USER_TYPES, max_length=20, default='super_admin')
    parent_user_id = models.IntegerField(blank=True, null=True, default=None)
    location_id = models.TextField(max_length=50, blank=True, null=True, default=None)
    deleted_at = models.DateTimeField(null=True, blank=True)

    REQUIRED_FIELDS = ['username', 'first_name', 'last_name']

    USERNAME_FIELD = 'email'

    db_table = 'user'

    def get_full_name(self):
        return '{0} {1}'.format(self.first_name, self.last_name)

    def is_active_user(self):
        return self.deleted_at == None

    class Meta(object):
        app_label = 'api_v1'

