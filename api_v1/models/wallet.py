from django.db import models
import datetime

from . user import User


class Wallet(models.Model):
    TYPE_CHOICES = [("AVAILABLE_BALANCE", "AVAILABLE_BALANCE"),
                    ("PENDING_DEPOSIT", "PENDING_DEPOSIT"),
                    ("PENDING_PAYMENT", "PENDING_PAYMENT")]

    wallet_id = models.TextField(max_length=14, blank=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    type = models.CharField(max_length=20, choices=TYPE_CHOICES)
    balance = models.FloatField(max_length=11, blank=False)
    created_by = models.IntegerField(blank=True)
    updated_by = models.IntegerField(blank=True)
    created_at = models.DateTimeField(default=datetime.datetime.now, blank=True)
    updated_at = models.DateTimeField(default=datetime.datetime.now, blank=True)

    def __str__(self):
        return self

    class Meta(object):
        app_label = 'api_v1'
