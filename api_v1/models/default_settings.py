from django.db import models
import datetime


class DefaultSettings(models.Model):
    tier_1_deposit_rate = models.DecimalField(default=None, max_digits=9, decimal_places=4)
    tier_2_deposit_rate = models.DecimalField(default=None, max_digits=9, decimal_places=4)
    tier_3_deposit_rate = models.DecimalField(default=None, max_digits=9, decimal_places=4)
    tier_4_deposit_rate = models.DecimalField(default=None, max_digits=9, decimal_places=4)
    tier_5_deposit_rate = models.DecimalField(default=None, max_digits=9, decimal_places=4)
    tier_1_deposit_threshold = models.DecimalField(default=None, max_digits=9, decimal_places=2)
    tier_2_deposit_threshold = models.DecimalField(default=None, max_digits=9, decimal_places=2)
    tier_3_deposit_threshold = models.DecimalField(default=None, blank=True, null=True, max_digits=9, decimal_places=2)
    tier_4_deposit_threshold = models.DecimalField(default=None, blank=True, null=True, max_digits=9, decimal_places=2)
    tier_5_deposit_threshold = models.DecimalField(default=None, blank=True, null=True, max_digits=9, decimal_places=2)
    tier_1_payment_rate = models.DecimalField(default=None, max_digits=9, decimal_places=4)
    tier_2_payment_rate = models.DecimalField(default=None, max_digits=9, decimal_places=4)
    tier_3_payment_rate = models.DecimalField(default=None, max_digits=9, decimal_places=4)
    tier_4_payment_rate = models.DecimalField(default=None, max_digits=9, decimal_places=4)
    tier_5_payment_rate = models.DecimalField(default=None, max_digits=9, decimal_places=4)
    tier_1_payment_threshold = models.DecimalField(default=None, max_digits=9, decimal_places=2)
    tier_2_payment_threshold = models.DecimalField(default=None, max_digits=9, decimal_places=2)
    tier_3_payment_threshold = models.DecimalField(default=None, blank=True, null=True, max_digits=9, decimal_places=2)
    tier_4_payment_threshold = models.DecimalField(default=None, blank=True, null=True, max_digits=9, decimal_places=2)
    tier_5_payment_threshold = models.DecimalField(default=None, blank=True, null=True, max_digits=9, decimal_places=2)
    tax_payment_rate = models.DecimalField(max_digits=9, decimal_places=4)
    payroll_payment_rate = models.DecimalField(max_digits=9, decimal_places=4)
    deposit_flat_rate = models.DecimalField(max_digits=9, decimal_places=4, default=0.0290)
    payment_flat_rate = models.DecimalField(max_digits=9, decimal_places=4, default=0.0290)
    created_by = models.IntegerField(blank=True)
    updated_by = models.IntegerField(blank=True)
    created_at = models.DateTimeField(default=datetime.datetime.now, blank=True)
    updated_at = models.DateTimeField(default=datetime.datetime.now, blank=True)

    def __str__(self):
        return self.id

    class Meta(object):
        app_label = 'api_v1'
