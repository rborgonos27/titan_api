from django.db import models
import datetime


class PayeeAgreement(models.Model):

    payee_id = models.IntegerField(blank=True)
    user_id = models.IntegerField(null=True, blank=True)
    remarks = models.TextField(max_length=80, blank=True)
    is_accepted = models.BooleanField(default=False)
    created_by = models.IntegerField(blank=True)
    updated_by = models.IntegerField(blank=True)
    deleted_at = models.DateTimeField(null=True, blank=True)
    created_at = models.DateTimeField(default=datetime.datetime.now, blank=True)
    updated_at = models.DateTimeField(default=datetime.datetime.now, blank=True)

    def __str__(self):
        return self

    class Meta(object):
        app_label = 'api_v1'
