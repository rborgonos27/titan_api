from django.db import models
import datetime

from . user import User


class FeeTier(models.Model):
    TYPE_CHOICES = [("v1", "v1"), ("v2", "v2")]

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    tier_no = models.IntegerField(default=None)
    deposit_rate = models.DecimalField(default=None, max_digits=9, decimal_places=4)
    deposit_threshold = models.DecimalField(default=None, blank=True, null=True, max_digits=9, decimal_places=2)
    payment_rate = models.DecimalField(default=None, max_digits=9, decimal_places=4)
    payment_threshold = models.DecimalField(default=None, max_digits=9, blank=True, null=True, decimal_places=2)
    type = models.CharField(max_length=10, choices=TYPE_CHOICES, default="v1")
    created_by = models.IntegerField(blank=True)
    updated_by = models.IntegerField(blank=True)
    created_at = models.DateTimeField(default=datetime.datetime.now, blank=True)
    updated_at = models.DateTimeField(default=datetime.datetime.now, blank=True)

    def __str__(self):
        return self.id

    class Meta(object):
        app_label = 'api_v1'
