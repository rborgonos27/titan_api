from django.db import models
import datetime

from . user import User


class FeeTransaction(models.Model):
    TRANSACTION_TYPE_CHOICES = [("payment", "payment"), ("deposit", "deposit")]
    STATUS_CHOICES = [("waived", "waived"), ("completed", "completed"), ("cancelled", "cancelled")]

    fee_transaction_id = models.TextField(max_length=14, blank=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    request_id = models.IntegerField(blank=True, default=None)
    type = models.CharField(max_length=10, choices=TRANSACTION_TYPE_CHOICES)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='completed')
    fee_percent = models.DecimalField(max_digits=9, decimal_places=4)
    transaction_amount = models.DecimalField(default=0, max_digits=9, decimal_places=2)
    total_transaction_amount = models.DecimalField(default=0, max_digits=9, decimal_places=2)
    fee_amount = models.DecimalField(max_digits=9, decimal_places=2)
    created_by = models.IntegerField(blank=True)
    updated_by = models.IntegerField(blank=True)
    process_at = models.DateTimeField(default=datetime.datetime.now, blank=True)
    created_at = models.DateTimeField(default=datetime.datetime.now, blank=True)
    updated_at = models.DateTimeField(default=datetime.datetime.now, blank=True)

    def __str__(self):
        return self

    class Meta(object):
        app_label = 'api_v1'
