from django.db import models
import datetime

from . user import User


class Deposit(models.Model):
    TYPE_CHOICES = [("immediate", "immediate"), ("scheduled", "scheduled")]
    STATUS_CHOICES = [("completed", "completed"), ("pending", "pending"), ("void", "void")]

    transaction_id = models.TextField(max_length=14, blank=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    schedule_at = models.DateTimeField(default=datetime.datetime.now, blank=True)
    type = models.CharField(max_length=10, choices=TYPE_CHOICES)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES)
    amount = models.DecimalField(max_digits=9, decimal_places=2)
    created_by = models.IntegerField(blank=True)
    updated_by = models.IntegerField(blank=True)
    created_at = models.DateTimeField(default=datetime.datetime.now, blank=True)
    updated_at = models.DateTimeField(default=datetime.datetime.now, blank=True)

    def __str__(self):
        return self.id

    class Meta(object):
        app_label = 'api_v1'

