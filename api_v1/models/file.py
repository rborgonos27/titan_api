from django.db import models
import datetime
from . user import User
from . member_statement import MemberStatement

def make_filepath(field_name, instance, filename):
    '''
        Produces a unique file path for the upload_to of a FileField.

        The produced path is of the form:
        "[model name]/[field name]/[random name].[filename extension]".
    '''
    print ("instance")
    print (instance.type)
    if instance.type == 'MEMBER_STATEMENT':
        member_statemet = MemberStatement.objects.get(id=instance.resource_id)
        new_filename = "{id}/estatement_{month}_{year}.{ext}".format(month=member_statemet.cutoff_at.strftime("%m"),
                                                          year=datetime.datetime.now().strftime("%y"),
                                                          id=instance.user_id,
                                                          ext=filename.split('.')[-1])
    else:
        new_filename = "%s.%s" % (User.objects.make_random_password(20),
                             filename.split('.')[-1])
    return '/'.join([instance.__class__.__name__.lower(),
                     field_name, new_filename])

from functools import partial


class File(models.Model):
    TYPE = [
        ("PAYMENT", "PAYMENT"),
        ("DEPOSIT", "DEPOSIT"),
        ("USER", "USER"),
        ("PAYEE", "PAYEE"),
    ]

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    filename = models.FileField(upload_to=partial(make_filepath, 'backup'))
    url = models.TextField(max_length=200, blank=False)
    type = models.TextField(max_length=200, choices=TYPE, default='PAYMENT')
    resource_id = models.IntegerField(default=None, blank=True, null=True)
    mime_type = models.TextField(max_length=200, blank=True)
    expire_at = models.DateTimeField(default=datetime.datetime.now, blank=True)
    created_by = models.IntegerField(blank=True)
    updated_by = models.IntegerField(blank=True)
    created_at = models.DateTimeField(default=datetime.datetime.now, blank=True)
    updated_at = models.DateTimeField(default=datetime.datetime.now, blank=True)
    remarks = models.TextField(max_length=100, blank=True, default=None, null=True)
    deleted_at = models.DateTimeField(default=None, blank=True, null=True)

    def __str__(self):
        return self

    class Meta(object):
        app_label = 'api_v1'
