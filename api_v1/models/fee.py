from django.db import models
import datetime

from . user import User

# flat_rate
# fee_tier_v1
# fee_tier_v2


class Fee(models.Model):
    fee_id = models.TextField(max_length=14, blank=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    tax_payment_rate = models.DecimalField(max_digits=9, decimal_places=4)
    payroll_payment_rate = models.DecimalField(max_digits=9, decimal_places=4)
    deposit_flat_rate = models.DecimalField(max_digits=9, decimal_places=4, default=0.0290)
    payment_flat_rate = models.DecimalField(max_digits=9, decimal_places=4, default=0.0290)
    fee_setting = models.TextField(max_length=14, default='flat_rate')
    setting_change_next_month = models.TextField(blank=True, max_length=14, default=None)
    created_by = models.IntegerField(blank=True)
    updated_by = models.IntegerField(blank=True)
    created_at = models.DateTimeField(default=datetime.datetime.now, blank=True)
    updated_at = models.DateTimeField(default=datetime.datetime.now, blank=True)

    def __str__(self):
        return self.id

    class Meta(object):
        app_label = 'api_v1'
