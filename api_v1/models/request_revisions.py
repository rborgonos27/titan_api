from django.db import models
from . user import User
from . payee import Payee
from . request import Request

import datetime


class RequestRevisions(models.Model):
    STATUS = [
        ("PENDING", "PENDING"),
        ("SCHEDULED", "SCHEDULED"),
        ("REQUESTED", "REQUESTED"),
        ("COMPLETED", "COMPLETED"),
        ("VOID", "VOID"),
    ]
    TYPE = [
        ("PAYMENT", "PAYMENT"),
        ("DEPOSIT", "DEPOSIT"),
    ]
    REQUEST_TYPE = [
        ("ELECTRONIC", "ELECTRONIC"),
        ("PAPER", "PAPER"),
    ]

    request_request_id = models.TextField(max_length=14, blank=False)
    request = models.ForeignKey(Request, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    payee = models.ForeignKey(Payee, on_delete=models.CASCADE, blank=True)
    type = models.CharField(max_length=20, choices=TYPE)
    status = models.CharField(max_length=20, choices=STATUS, default='REQUESTED')
    transaction_type = models.CharField(max_length=20, choices=REQUEST_TYPE, default='PAPER')
    to = models.TextField(max_length=50, blank=True)
    amount = models.DecimalField(max_digits=9, decimal_places=2)
    fee = models.DecimalField(max_digits=9, decimal_places=2, default=0)
    remarks = models.TextField(max_length=200, blank=True)
    due_at = models.DateTimeField(default=datetime.datetime.now, blank=False)
    pickup_at = models.DateTimeField(default=datetime.datetime.now, blank=True)
    created_by = models.IntegerField(blank=True)
    updated_by = models.IntegerField(blank=True)
    created_at = models.DateTimeField(default=datetime.datetime.now, blank=True)
    updated_at = models.DateTimeField(default=datetime.datetime.now, blank=True)

    def __str__(self):
        return self

    class Meta(object):
        app_label = 'api_v1'
