from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser, FormParser
from ..models import File
from ..serializers import PayeeFileSerializer
from rest_framework import status


class PayeeFileView(viewsets.ModelViewSet):
    serializer_class = PayeeFileSerializer
    file = File.objects.all()
    parser_classes = (MultiPartParser, FormParser,)
    queryset = file

    permission_classes = [
        AllowAny
    ]
