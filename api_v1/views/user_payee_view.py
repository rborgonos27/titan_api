from rest_framework.response import Response
from django.db import IntegrityError
from rest_framework import viewsets
from rest_framework import status
import datetime

from ..models import UserPayee
from ..serializers import UserPayeeSerializer


class UserPayeeView(viewsets.ModelViewSet):
    serializer_class = UserPayeeSerializer
    queryset = UserPayee.objects.all()

    def get(self):
        user = self.request.user
        user_payee = UserPayee.objects.filter(user=user)
        user_data = UserPayeeSerializer(user_payee)

        return Response(user_data, status.HTTP_200_OK)

    def create(self, request):
        try:
            user = self.request.user
            user_payee = UserPayee(
                user_id=request.POST['user_id'],
                payee_id=request.POST['payee_id'],
                created_by=user.id,
                updated_by=user.id,
                created_at=datetime.datetime.now(),
                updated_at=datetime.datetime.now(),
            )
            user_payee.save()
            user_data = UserPayeeSerializer(user_payee)
            return Response({'success': True, 'message': 'Created', 'data': user_data.data},
                            status=status.HTTP_200_OK)
        except IntegrityError:
            return Response({'success': False, 'message': 'Payee already exists in user.'},
                            status=status.HTTP_400_BAD_REQUEST)