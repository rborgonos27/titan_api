from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser, FormParser
from ..models import FeeTier, User
from ..serializers import FeeTierSerializer
from rest_framework import status


class FeeTierViewset(viewsets.ModelViewSet):
    serializer_class = FeeTierSerializer

    parser_classes = (MultiPartParser, FormParser,)

    def get_queryset(self):
        fee_data = FeeTier.objects.all()
        params = self.request.query_params
        if 'user_id' in params:
            user_data = User.objects.get(id=params.get('user_id'))
            user_id = user_data.parent_user_id if user_data.parent_user_id else params.get('user_id')
            fee_data = fee_data.filter(user_id=user_id)

        return fee_data

    # queryset = fee_data = Fee.objects.all()

