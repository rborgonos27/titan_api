from rest_framework.views import APIView
from rest_framework import status
from rest_framework.permissions import AllowAny
import os
from django.core.files.storage import FileSystemStorage
from reportlab.pdfgen import canvas
from PyPDF2 import PdfFileWriter, PdfFileReader

from rest_framework.response import Response
from django.template.loader import get_template
from django.core.mail import EmailMessage
from django.conf import settings
import requests
import socket
from datetime import datetime, timedelta
from ..serializers import PayeeFileSerializer
from ..models import File, PayeeAgreement, Payee
from collections import OrderedDict
from django.core.files import File as FileObject


class UploadSignature (APIView):
    permission_classes = [
        AllowAny
    ]

    def post(self, request):
        fail_response = {
            'version': 'v1',
            'success': False,
            'data': None,
            'message': 'Incomplete field(s).'
        }

        if 'payee_id' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'member_name' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'payee_agreement_id' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'file' not in request.FILES:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        img_sign = request.FILES['file']
        fs = FileSystemStorage()

        custom_file_name = "payee_sign_{payee_id}.jpg".format(payee_id=request.POST['payee_id'])
        filename = fs.save(custom_file_name, img_sign)
        uploaded_file_url = fs.url(filename)
        custom_file_name = "{root}/{file}".format(root='static/media', file=custom_file_name)

        payee = Payee.objects.get(id=request.POST['payee_id'])

        # TODO:: separate process for updating payee_agreement file
        agreement_file = 'static/media/payee_agreement_final.pdf'
        exists = os.path.isfile(agreement_file)
        if not exists:
            file_url = settings.PAYEE_AGREEMENT_FILE
            r = requests.get(file_url, stream=True)

            with open(agreement_file, "wb") as pdf:
                for chunk in r.iter_content(chunk_size=1024):
                    if chunk:
                        pdf.write(chunk)

        # Create the watermark from an image
        watermark_file = "static/media/watermark_sign_{payee_id}.pdf".format(payee_id=request.POST['payee_id'])
        c = canvas.Canvas(watermark_file)

        y_pos = 90
        # Draw the image at x, y. I positioned the x,y to be where i like here
        c.drawImage(custom_file_name, 120, y_pos, mask='auto', width=100, height=50)
        c.drawString(150, y_pos, payee.name)

        # Add some custom text for good measure
        c.drawString(150, y_pos - 40, datetime.now().strftime("%m/%d/%Y, %H:%M:%S"))
        c.drawString(370, y_pos, request.POST['member_name'])
        c.save()

        # Get the watermark file you just created
        watermark = PdfFileReader(open(watermark_file, "rb"))

        # Get our files ready
        output_file = PdfFileWriter()
        input_file = PdfFileReader(open(agreement_file, "rb"))

        # Number of pages in input document
        page_count = input_file.getNumPages()

        # Go through all the input file pages to add a watermark to them
        for page_number in range(page_count):
            print ("Watermarking page {} of {}".format(page_number, page_count))
            # merge the watermark with the page
            input_page = input_file.getPage(page_number)
            if page_number == 2:
                input_page.mergePage(watermark.getPage(0))
            # add page from input file to output document
            output_file.addPage(input_page)

        output_filename = "static/media/document-output_{payee_id}.pdf".format(payee_id=request.POST['payee_id'])
        # finally, write "output" to document-output.pdf
        with open(output_filename, "wb") as outputStream:
            output_file.write(outputStream)

        os.remove(watermark_file)
        os.remove(custom_file_name)

        output_file = open(output_filename, "rb")
        print(output_file)

        file = File(
            user_id=1,
            filename=FileObject(output_file),
            url='filename',
            mime_type='application/pdf',
            expire_at=datetime.now() + timedelta(hours=1),
            type='PAYEE_AGREEMENT',
            resource_id=request.POST['payee_agreement_id'],
            created_by=1,
            updated_by=1,
            created_at=datetime.now(),
            updated_at=datetime.now(),
        )

        file.save()

        payee_agreement = PayeeAgreement.objects.get(id=request.POST['payee_agreement_id'])
        payee_agreement.is_accepted = True
        payee_agreement.remarks = 'Signed'
        payee_agreement.save()

        response = { 'file_url': uploaded_file_url }

        return Response(response, status.HTTP_200_OK)
