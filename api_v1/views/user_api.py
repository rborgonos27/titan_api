from django.contrib.auth.hashers import make_password
from rest_framework.views import APIView
from django.db import IntegrityError
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import ListCreateAPIView

from ..models import User
from ..serializers import UserSerializer
from utils.response import ResponseTemplate


class userAPI(ListCreateAPIView):
    permission_classes = [
        AllowAny
    ]

    def get(self, request):
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        response = ResponseTemplate.success('v1', serializer.data, 200)

        return Response(response, status.HTTP_200_OK)

    def post(self, request):
        print (request.data)
        mutable = request.POST._mutable
        request.POST._mutable = True
        # request.POST['username'] = request.data.get('email')
        request.POST['password'] = make_password(request.data.get('password'))
        request.POST['parent_user_id'] = request.data.get('parent_user_id') if 'parent_user_id' in request.data.get\
            else None
        request.POST._mutable = mutable

        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()

            response = ResponseTemplate.success('v1', serializer.data, 201)
            return Response(response, status=status.HTTP_201_CREATED)

    def put(self, request, pk):
        try:
            request_data = User.objects.get(id=pk)
            request_data.first_name = request.POST['first_name']
            request_data.last_name = request.POST['last_name']
            request_data.email = request.POST['email']
            request_data.password = make_password(request.POST['password'])
            request_data.username = request.POST['username']
            request_data.user_type = request.POST['user_type']
            request_data.primary_contact_name = request.POST['primary_contact_name']
            request_data.primary_contact_number = request.POST['primary_contact_number']
            request_data.business_name = request.POST['business_name']
            request_data.principal_city_of_business = request.POST['principal_city_of_business']
            if 'is_first_time_member' in request.POST:
                if request.POST['is_first_time_member'] == 'true':
                    request_data.is_first_time_member = True
                if request.POST['is_first_time_member'] == 'false':
                    request_data.is_first_time_member = False
            if 'address' in request.POST:
                request_data.address = request.POST['address']
            if 'business_contact_number' in request.POST:
                request_data.business_contact_number = request.POST['business_contact_number']
            request_data.save()

            serializer = UserSerializer(request_data)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except IntegrityError:
            return Response({'success': False,
                             'message': 'The user name you choose is already in use.'
                                        ' Please select another unique user id login for your security purposes.'
                                        ' Thank you'},
                            status=status.HTTP_400_BAD_REQUEST)
