from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser, FormParser
from ..models import FeeTransaction
from ..serializers import FeeTransactionSerializer
from rest_framework import status


class FeeTransactionViewset(viewsets.ModelViewSet):
    serializer_class = FeeTransactionSerializer
    fee_transaction = FeeTransaction.objects.all()

    parser_classes = (MultiPartParser, FormParser,)
    queryset = fee_transaction
