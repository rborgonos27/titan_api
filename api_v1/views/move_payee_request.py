from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from utils.response import ResponseTemplate
from ..models import Payee, UserPayee
from ..serializers import PayeeSerializer
import datetime


class MovePayeeRequest(APIView):
    def post(self, request):
        try:
            fail_response = {
                'version': 'v1',
                'success': False,
                'data': None,
                'message': 'Incomplete field(s).'
            }
            if 'user_id' not in request.POST:
                return Response(fail_response, status.HTTP_400_BAD_REQUEST)
            if 'payee_id' not in request.POST:
                return Response(fail_response, status.HTTP_400_BAD_REQUEST)
            if 'payee_main_id' not in request.POST:
                return Response(fail_response, status.HTTP_400_BAD_REQUEST)

            UserPayee.objects.filter(payee_id=request.POST['payee_id'], user_id=request.POST['user_id']).delete()
            Payee.objects.filter(id=request.POST['payee_id']).delete()
            # UserPayee
            user_payee = UserPayee(
                created_by=request.POST['user_id'],
                updated_by=request.POST['user_id'],
                created_at=datetime.datetime.now(),
                updated_at=datetime.datetime.now(),
                payee_id=request.POST['payee_main_id'],
                user_id=request.POST['user_id'],
            )
            user_payee.save()
            return Response({'success': True, 'message': 'Successfully moved.'}, status=status.HTTP_200_Ok)
        except:
            return Response({'error': 'Cannot pull record'}, status=status.HTTP_400_BAD_REQUEST)
