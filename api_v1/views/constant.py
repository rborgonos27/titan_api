class Constant(object):
    STATUS_COMPLETED = 'COMPLETED'
    UPDATED_AT_FIELD = 'updated_at'
    TYPE_DEPOSIT = 'DEPOSIT'
    TYPE_PAYMENT = 'PAYMENT'
    DB_DATE_TIMESTAMP = "%Y-%m-%dT%H:%M:%S.%f"

    DATE_FORMAT = "%Y-%m-%d"
    TIME_FORMAT = "%H:%M:%S"
    DATE_TIME_FORMAT_AT = "%Y-%m-%dT%H:%M:%S"

    FIELD_USER_ID = 'user_id'
    FIELD_TYPE = 'type'
    FIELD_STATUS = 'status'
    FIELD_DELIVERY_METHOD = 'delivery_method'
    FIELD_DATE = 'date'

    USER_SUPER_ADMIN = 'super_admin'
    USER_MEMBER_USER = 'sub_member'
    USER_MEMBER = 'member'
