from rest_framework.views import APIView
from rest_framework import status
import base64

from rest_framework.response import Response
from django.template.loader import get_template
from django.core.mail import EmailMessage
from django.conf import settings
import requests
import os
import logging


class PayeeNotification (APIView):
    def post(self, request):
        logger = logging.getLogger(__name__)
        fail_response = {
            'version': 'v1',
            'success': False,
            'data': None,
            'message': 'Incomplete field(s).'
        }
        if 'email' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'user_id' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'member_name' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'payee_name' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'payee_id' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        subject = 'You have a pending payment from {name}'.format(name=request.POST['member_name'])
        to = [request.POST['email']]
        from_email = 'Titan Vault <no-reply@zeuss.tech>'
        reply_to = [settings.TITAN_REPLY_TO_EMAIL]

        to_encrypt = "{payee_id}:{user_id}".format(
            payee_id=request.POST['payee_id'],
            user_id=request.POST['user_id']
        )

        print(request.POST['member_name'])
        payee_encrypt = base64.b64encode(bytes(to_encrypt, 'utf-8'))
        ctx = {
            'member_name': request.POST['member_name'],
            'payee_name': request.POST['payee_name'],
            'link': "{server}/payee-terms-agreement/{id}".format(server=settings.APP_URL, id=payee_encrypt)
        }

        message = get_template('payee_notification.html').render(ctx)
        msg = EmailMessage(subject, message, to=to, from_email=from_email, reply_to=reply_to)
        msg.content_subtype = 'html'

        file_stored = 'storage/payee_agreement_v2.pdf'
        exists = os.path.isfile(file_stored)
        if not exists:
            file_url = settings.PAYEE_AGREEMENT_FILE
            r = requests.get(file_url, stream=True)

            with open(file_stored, "wb") as pdf:
                for chunk in r.iter_content(chunk_size=1024):
                    if chunk:
                        pdf.write(chunk)

        file = open(file_stored, "rb")
        msg.attach(filename="payee_agreement.pdf", mimetype="application/pdf", content=file.read())
        file.close()

        msg.send()

        logger.info("Payee Agreement sent to {email}".format(email=request.POST['email']))

        response = {
            'version': 'v1',
            'success': True,
            'data': None,
            'message': 'Payee agreement sent!'
        }

        return Response(response, status.HTTP_200_OK)
