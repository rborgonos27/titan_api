from rest_framework.response import Response
from django.db import IntegrityError
from rest_framework.views import APIView
from rest_framework import status
from ..models import User
from ..serializers import UserSerializer
import datetime
import csv
from django.http import HttpResponse


class UserCsv(APIView):

    def get_data(self, data):
        return "{data}".format(data=data)

    def get(self, request):
        print ('test')
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="user_data.csv"'

        user_data = User.objects.all()
        print (user_data)

        # request_data
        writer = csv.writer(response)
        report_header = [
            'user_type',
            'member_account_number',
            'member_name',
            'member_id',
            'email',
            'username',
            'is_verified',
            'primary_contact_name',
            'primary_contact_number',
            'principal_city_of_business',
            'business_contact_number',
            'address',
            'address_2',
            'city',
            'state',
            'zip_code',
        ]
        writer.writerow(report_header)
        for data in user_data:
            data = UserSerializer(data).data
            user_name = "{first_name} {last_name}".format(first_name=data['first_name'],
                                                            last_name=data['last_name'])

            user_type = 'Administrator' if data['is_superuser'] == 0 else 'Member'
            is_verified = 'Yes' if data['is_verified'] == 0 else 'No'

            writer.writerow([
                self.get_data(user_type),
                self.get_data(data['member_account_id']),
                self.get_data(data['business_name']),
                self.get_data(data['username']),
                self.get_data(data['email']),
                self.get_data(user_name),
                self.get_data(is_verified),
                self.get_data(data['primary_contact_name']),
                self.get_data(data['primary_contact_number']),
                self.get_data(data['principal_city_of_business']),
                self.get_data(data['business_contact_number']),
                self.get_data(data['address']),
                self.get_data(data['address_2']),
                self.get_data(data['city']),
                self.get_data(data['state']),
                self.get_data(data['zip_code']),
            ])

        return response
