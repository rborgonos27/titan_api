from rest_framework import generics
from rest_framework.settings import api_settings
from rest_framework.filters import SearchFilter, OrderingFilter
from ..serializers import PayeeSerializer, UserSerializer
from ..models import Payee, UserPayee


class PayeeDetailsListAPI(generics.ListAPIView):
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS
    serializer_class = PayeeSerializer

    def get_queryset(self):
        user = self.request.user
        user_data = UserSerializer(user)
        user_data = user_data.data
        if user_data['user_type'] == 'sub_member':
            return Payee.objects.filter(created_by=user_data['parent_user_id'])
        elif user_data['user_type'] == 'member':
            ids = UserPayee.objects.filter(user_id=user_data['id']).values_list('payee_id', flat=True)
            return Payee.objects.filter(id__in=ids)
        elif user_data['user_type'] == 'super_admin':
            return Payee.objects.all()


    filter_backends = (SearchFilter, OrderingFilter,)
    search_fields = ('name', 'bank_account', 'bank_account_name', 'bank_account', 'bank_routing_number')
    ordering_fields = ('name', 'bank_account', 'bank_account_name', 'bank_account', 'bank_routing_number')
