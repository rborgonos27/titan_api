from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser, FormParser
from ..models import File
from ..serializers import FileRequestSerializer
from rest_framework import status
import datetime

class FileRequestViewset(viewsets.ModelViewSet):
    serializer_class = FileRequestSerializer
    file = File.objects.filter(deleted_at=None)

    parser_classes = (MultiPartParser, FormParser,)
    queryset = file

    def destroy(self, request, pk=None):
        print ('pk')
        print (pk)
        print (request.GET)
        file = File.objects.get(id=pk)
        if file:
            file.remarks = request.GET['remarks']
            file.deleted_at = datetime.datetime.now()
            file.save()
            return Response({ 'success': True }, status.HTTP_200_OK)
