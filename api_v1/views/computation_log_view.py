from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
import logging


class ComputationLogView(APIView):

    def post(self, request):
        logger = logging.getLogger(__name__)
        if 'type' in request.POST:
            logger.info("Compute for {type} transaction".format(type=request.POST['type']))

        if 'input_amount' in request.POST:
            logger.info("Input Amount : {input_amount}".format(input_amount=request.POST['input_amount']))

        if 'first_tier_amount' in request.POST:
            logger.info("First tier amount: {amount}".format(amount=request.POST['first_tier_amount']))

        if 'second_tier_amount' in request.POST:
            logger.info("Second tier amount: {amount}".format(amount=request.POST['second_tier_amount']))

        if 'third_tier_amount' in request.POST:
            logger.info("Third tier amount: {amount}".format(amount=request.POST['third_tier_amount']))

        if 'first_tier_fee' in request.POST:
            logger.info("First Tier Amount ({tier_amount}) x first tier Rate ({tier_rate}) : {amount}"
                        .format(
                            amount=request.POST['first_tier_fee'],
                            tier_amount=request.POST['first_tier_amount'],
                            tier_rate=request.POST['first_tier_rate']
                            )
                        )

        if 'second_tier_fee' in request.POST:
            logger.info("Second Tier Amount ({tier_amount}) x second ier Rate ({tier_rate}) : {amount}"
                        .format(
                            amount=request.POST['second_tier_fee'],
                            tier_amount=request.POST['second_tier_amount'],
                            tier_rate=request.POST['second_tier_rate']
                            )
                        )

        if 'third_tier_fee' in request.POST:
            logger.info("Third Tier Amount ({tier_amount}) x third ier Rate ({tier_rate}) : {amount}"
                        .format(
                            amount=request.POST['third_tier_fee'],
                            tier_amount=request.POST['third_tier_amount'],
                            tier_rate=request.POST['third_tier_rate']
                            )
                        )

        if 'total_transaction_amount' in request.POST:
            logger.info("Current total amount of monthly {type} bucket: {amount}"
                        .format(
                            amount=request.POST['total_transaction_amount'],
                            type=request.POST['type']
                        ))

        if 'transaction_amount' in request.POST:
            logger.info("Transaction fee amount: {amount}".format(amount=request.POST['transaction_amount']))

        return Response({'success': True}, status=status.HTTP_200_OK)
