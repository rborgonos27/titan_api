from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.conf import settings
from utils.audit_log import audit_log
from ..models import Payee
from ..serializers import PayeeSerializer


class DepositPayeeAccount(APIView):
    def get(self, request):
        payee = Payee.objects.filter(id=settings.TITAN_DEPOSIT_PAYEE)[0]
        payee_deposit_account = PayeeSerializer(payee)
        audit = {
            'module': 'payee',
            'transaction': 'get current user',
            'description': 'get current deposit account',
            'user_id': request.user.id,
        }
        audit_log(audit)
        return Response(payee_deposit_account.data, status=status.HTTP_200_OK)
