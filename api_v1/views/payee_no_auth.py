from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
from rest_framework.response import Response
from ..models import Payee, PayeeAgreement
from ..serializers import PayeeSerializer, PayeeAgreementSerializer
from rest_framework import status


class PayeeNoAuth(APIView):
    permission_classes = [
        AllowAny
    ]

    def get(self, request):
        try:
            payee_agreement = PayeeAgreement.objects.get(
                user_id=request.GET['user_id'],
                payee_id=request.GET['payee_id'])

            serializer = PayeeAgreementSerializer(payee_agreement)
        except:
            response = {
                'version': 'v1',
                'success': False,
                'data': None,
                'message': 'No Data'
            }
            return Response(response, status.HTTP_200_OK)

        if serializer.data['is_accepted'] == False:
            is_accepted = {'is_accepted': False}
        else:
            is_accepted = {'is_accepted': serializer.data['is_accepted']}

        response = {
            'version': 'v1',
            'success': True,
            'data': is_accepted,
            'message': 'Payee!'
        }

        return Response(response, status.HTTP_200_OK)
