from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser, FormParser
from ..models import File
from ..serializers import UserFileSerializer
from rest_framework import status


class UserFileView(viewsets.ModelViewSet):
    serializer_class = UserFileSerializer
    file = File.objects.all()
    parser_classes = (MultiPartParser, FormParser,)
    queryset = file
