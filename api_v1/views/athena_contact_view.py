from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework import status

from rest_framework.response import Response
from django.template import Context
from django.template.loader import render_to_string, get_template
from django.core.mail import EmailMessage
from django.conf import settings


class AthenaContactView(APIView):
    permission_classes = [
        AllowAny
    ]

    def post(self, request):
        fail_response = {
            'version': 'v1',
            'success': False,
            'data': None,
            'message': 'Incomplete field(s).'
        }

        if 'name' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'email' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'contact_number' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        personal_message = ''
        if 'personal_message' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        subject = '[NEW CLIENT ALERT] Congratulations you have new client!'
        to = [settings.ATHENA_STAFF_EMAIL]
        from_email = 'Athena Pay <no-reply@zeuss.tech>'
        reply_to = [settings.ATHENA_REPLY_TO_EMAIL]

        ctx = {
            'name': request.POST['name'],
            'email': request.POST['email'],
            'contact_number': request.POST['contact_number'],
            'personal_message': personal_message,
        }

        message = get_template('new_athena_contacts_template.html').render(ctx)
        msg = EmailMessage(subject, message, to=to, from_email=from_email, reply_to=reply_to)
        msg.content_subtype = 'html'

        msg.send()

        notif_subject = 'Welcome to Athena Pay'
        notif_to = [request.POST['email']]
        notif_from_email = 'Athena Pay <no-reply@titan-vault.com>'
        notif_reply_to = [settings.ATHENA_NO_REPLY_EMAIL]

        message = get_template('athena_notification_template.html').render(ctx)
        notif = EmailMessage(notif_subject, message, to=notif_to, from_email=notif_from_email, reply_to=notif_reply_to)
        notif.content_subtype = 'html'

        notif.send()

        response = {
            'version': 'v1',
            'success': True,
            'data': None,
            'message': 'Thank you for interest. Let''s speak soon!'
        }

        return Response(response, status.HTTP_200_OK)
