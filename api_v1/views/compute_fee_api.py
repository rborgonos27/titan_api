from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from ..models import FeeTier, FeeTransaction
from ..serializers import FeeTierSerializer, FeeTransactionSerializer
from utils.computation import Computation
import datetime
import logging


class ComputationFeeView(APIView):

    def post(self, request):
        logger = logging.getLogger(__name__)
        fail_response = {
            'version': 'v1',
            'success': False,
            'data': None,
            'message': 'Incomplete field(s).'
        }

        if 'user_id' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'type' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'date' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'amount' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        logger.info("Compute for {type} transaction".format(type=request.POST['type']))
        logger.info("Input Amount : {input_amount}".format(input_amount=request.POST['amount']))

        # get fee transaction
        total_transaction_amount =\
            Computation.get_total_transaction_amount(
                request.POST['date'],
                request.POST['type'],
                request.POST['user_id'],
            )

        # get fee tier
        fee_tier_data = Computation.get_fee_tier(request.POST['user_id'])
        threshold_type = 'deposit_threshold'
        rate_type = 'deposit_rate'
        if request.POST['type'] == 'payment':
            threshold_type = 'payment_threshold'
            rate_type = 'payment_rate'

        # compute fee
        segragated_amount =\
            Computation.segregate_amount_by_tier(
                fee_tier_data.data,
                request.POST['amount'],
                total_transaction_amount,
                threshold_type,
            )

        total_fee_amount =\
            Computation.compute_fee(
                logger,
                segragated_amount,
                rate_type,
                fee_tier_data,
                total_transaction_amount,
                request.POST['type']
            )

        return Response({'fee': total_fee_amount}, status=status.HTTP_200_OK)
