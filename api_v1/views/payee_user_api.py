from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response

from ..models import UserPayee, User
from ..serializers import UserPayeeSerializer, UserSerializer
from utils.response import ResponseTemplate


class PayeeUserApi(APIView):

    def get(self, request):
        if 'payee_id' in request.GET:
            user_payee = UserPayee.objects.filter(payee_id=request.GET['payee_id'])
        elif 'user_id' in request.GET:
            user_payee = UserPayee.objects.filter(user_id=request.GET['user_id'], payee__status='APPROVED')
        else:
            user = self.request.user
            user_data = UserSerializer(user)
            user_data = user_data.data
            if user_data['parent_user_id']:
                sub_user = User.objects.get(id=user_data['parent_user_id'])
                parent_user = UserSerializer(sub_user)
                parent_user = parent_user.data
                user_payee = UserPayee.objects.filter(user_id=parent_user['id'], payee__status='APPROVED')
            else:
                user_payee = UserPayee.objects.filter(user_id=user.id, payee__status='APPROVED')

        serializer = UserPayeeSerializer(user_payee, many=True)
        response = ResponseTemplate.success('v1', serializer.data, 'UserPayee List.')

        return Response(response, status.HTTP_200_OK)

    def delete(self, request):
        if 'payee_id' in request.GET and 'user_id' in request.GET:
            user_payee = UserPayee.objects.filter(payee_id=request.GET['payee_id'], user_id=request.GET['user_id'])
        elif 'payee_id' in request.GET:
            user_payee = UserPayee.objects.filter(payee_id=request.GET['payee_id'])
        elif 'user_id' in request.GET:
            user_payee = UserPayee.objects.filter(payee_id=request.GET['user_id'])
        else:
            return Response({'success': False, 'message': 'user id or payee id not found.'}, status.HTTP_400_BAD_REQUEST)

        user_payee.delete()

        return Response({'success': True}, status.HTTP_200_OK)

