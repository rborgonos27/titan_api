from rest_framework.views import APIView
from rest_framework import status
from rest_framework.permissions import AllowAny

from rest_framework.response import Response
from django.template.loader import get_template
from ..models import User
from django.core.mail import EmailMessage
from django.conf import settings


class SendMaximumAttemptEmail (APIView):
    permission_classes = [
        AllowAny
    ]
    def post(self, request):
        fail_response = {
            'version': 'v1',
            'success': False,
            'data': None,
            'message': 'Incomplete field(s).'
        }

        print (request.POST)
        if 'username_or_email' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        try:
            user = User.objects.get(username=request.POST['username_or_email'])
        except User.DoesNotExist:
            user = None

        if not user:
            try:
                user = User.objects.get(email=request.POST['username_or_email'])
            except User.DoesNotExist:
                user = None

        if not user:
            fail_response = {
                'version': 'v1',
                'success': False,
                'data': None,
                'message': 'User not found.'
            }
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        subject = 'Maximum login attempts'
        to = [user.email]
        from_email = 'Titan Vault <no-reply@zeuss.tech>'
        reply_to = [settings.TITAN_REPLY_TO_EMAIL]

        ctx = {
            'first_name': user.first_name,
            'link': "{server}/reset_password/{id}".format(server=settings.APP_URL, id=user.id)
        }

        message = get_template('maximum_attempts_alert.html').render(ctx)
        msg = EmailMessage(subject, message, to=to, from_email=from_email, reply_to=reply_to)
        msg.content_subtype = 'html'

        msg.send()

        response = {
            'version': 'v1',
            'success': True,
            'data': None,
            'message': 'Email credentials sent to user!'
        }

        return Response(response, status.HTTP_200_OK)
