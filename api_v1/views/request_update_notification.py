from rest_framework.views import APIView
from rest_framework import status

from rest_framework.response import Response
from django.template.loader import get_template
from django.core.mail import EmailMessage
from django.conf import settings


class RequestUpdateNotification (APIView):
    def post(self, request):
        fail_response = {
            'version': 'v1',
            'success': False,
            'data': None,
            'message': 'Incomplete field(s).'
        }

        if 'id' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        emails = settings.TITAN_STAFF_EMAIL if isinstance(settings.TITAN_STAFF_EMAIL, list) else [
            settings.TITAN_STAFF_EMAIL]
        print (emails)
        subject = '[REQUEST CHANGES ALERT] Request has been modified.'
        to = emails
        from_email = 'Titan Vault <no-reply@zeuss.tech>'
        reply_to = [settings.TITAN_REPLY_TO_EMAIL]

        ctx = {
            'link': "{server}/request/{pk}/".format(server=settings.APP_URL, pk=request.POST['id'])
        }

        message = get_template('change_request_alert.html').render(ctx)
        msg = EmailMessage(subject, message, to=to, from_email=from_email, reply_to=reply_to)
        msg.content_subtype = 'html'

        msg.send()

        response = {
            'version': 'v1',
            'success': True,
            'data': None,
            'message': 'Email credentials sent to user!'
        }

        return Response(response, status.HTTP_200_OK)
