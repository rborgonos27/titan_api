from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.db.models import Q

from ..models import File, User
from ..serializers import FileSerializer
from utils.response import ResponseTemplate
import datetime


class FileRequestAPI(APIView):
    def get(self, request):
        request_data = None
        if 'show_deleted' in request.GET:
            request_data = File.objects.filter(resource_id=request.GET['request_id'])\
                .filter(~Q(deleted_at=None))\
                .filter(~Q(type='MEMBER_STATEMENT'))\
                .filter(~Q(type='PAYEE_AGREEMENT'))\
                .filter(~Q(type='USER'))
        else:
            request_data = File.objects.filter(resource_id=request.GET['request_id'], deleted_at=None)\
                .filter(~Q(type='MEMBER_STATEMENT'))\
                .filter(~Q(type='PAYEE_AGREEMENT')) \
                .filter(~Q(type='USER'))
        serializer = FileSerializer(request_data, many=True)
        response = ResponseTemplate.success('v1', serializer.data, 'Request List.')

        return Response(response, status.HTTP_200_OK)
