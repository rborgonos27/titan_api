from rest_framework import viewsets
from rest_framework.response import Response
from django.db.models import Q
from rest_framework import status
from ..serializers import UserMemberSerializer
from ..models import User

from .. import permission
import datetime


class UserMemberViewSet(viewsets.ModelViewSet):
    # permission_classes = [
    #     permission.UpdateOwnerProfile
    # ]

    serializer_class = UserMemberSerializer

    def get_queryset(self):
        user_data = User.objects
        user = self.request.user
        params = self.request.query_params
        if 'content' in params:
            if params.get('content') == 'member':
                user_data = user_data.filter(user_type='member')\
                    .filter(~Q(id=user.id))
            else:
                user_data = user_data.all()
        else:
            user_data = user_data.all()

        return user_data

    # queryset = User.objects.all()

    def destroy(self, request, pk):
        user_data = User.objects.get(id=pk)
        user_data.deleted_at = datetime.datetime.now()
        user_data.save();

        return Response({"success": True}, status.HTTP_200_OK)
