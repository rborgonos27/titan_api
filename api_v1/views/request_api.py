from rest_framework.response import Response
from django.core.files import File as FileObject
from rest_framework import status
from rest_framework import generics
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.settings import api_settings
from django.template.loader import render_to_string, get_template
from django.conf import settings
from django.core.mail import EmailMessage
from utils.audit_log import audit_log
from django.db.models import Q, Sum
from utils.computation import Computation
from utils.build_ticket import BuildDepositTicket

from ..models import Request, Transaction, Ledger, Wallet, User, File, FeeTransaction, Fee, User
from ..serializers import RequestSerializer, FeeSerializer, UserSerializer
from utils.response import ResponseTemplate
import datetime
import logging


class RequestAPI(generics.ListAPIView):
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS
    serializer_class = RequestSerializer

    filter_backends = (SearchFilter, OrderingFilter,)
    search_fields = ('payee__name', 'type', 'due_at', 'amount', 'fee', 'status')
    ordering_fields = ('payee__name', 'type', 'due_at', 'amount', 'fee', 'status')

    def get_queryset(self):
        user = self.request.user
        user_data = UserSerializer(user)
        user_data = user_data.data
        if user_data['parent_user_id']:
            sub_user = User.objects.get(id=user_data['parent_user_id'])
            parent_user = UserSerializer(sub_user)
            parent_user = parent_user.data
            request_data = Request.objects.filter(user_id=parent_user['id']).order_by('pickup_at', 'due_at')
        else:
            request_data = Request.objects.filter(user=user).order_by('pickup_at', 'due_at')

        params = self.request.query_params
        if 'type' in params:
            request_data = request_data.filter(type=params.get('type'))

        if 'status' in params:
            request_data = request_data.filter(status=params.get('status'))

        if 'delivery_method' in params:
            request_data = request_data.filter(type=params.get('status'))
        # custom
        # today
        # next_3_days
        # this_week
        # this_month
        # this_quarter
        # last_30_days
        # last_90_days
        # last_month
        # last_quarter
        # last_year
        # all_dates

        if 'date' in params:
            # custom
            if params.get('date') == 'payment_date':
                request_data = self.get_payment_date_filter(params, request_data)
            elif params.get('date') == 'pickup_date':
                request_data = self.get_pickup_date_filter(params, request_data)
            else:
                request_data = self.get_created_date_filter(params, request_data)
        else:
            request_data = self.get_created_date_filter(params, request_data)

        if 'payee_id' in params:
            request_data = request_data.filter(payee_id=params.get('payee_id'))

        if 'category' in params:
            request_data = request_data.filter(type=params.get('category'))

        return request_data.order_by('-created_by')
        # return Request.objects.filter(user=user)

    def post(self, request):
        # serializer = UserSerializer(data=request.data)
        user = request.user
        mutable = request.POST._mutable
        request.POST._mutable = True
        user_data = User.objects.get(id=user.id)
        request.POST['user_id'] = user_data.parent_user_id if user_data.parent_user_id else user.id
        request.POST['created_by'] = user.id
        request.POST['updated_by'] = user.id
        request.POST['created_at'] = datetime.datetime.now()
        request.POST['updated_at'] = datetime.datetime.now()
        request.POST._mutable = mutable

        serializer = RequestSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            print(serializer.data)
            self.update_file(serializer.data)
            self.create_ledger(serializer.data, request)
            self.update_wallet(serializer.data, request)
            response = ResponseTemplate.success('v1', serializer.data, 'Request created.')
            audit = {
                'module': 'request',
                'transaction': 'processing member {transaction}'.format(transaction=serializer.data['type']),
                'description': '{user} is creating new request'.format(user=serializer.data['user']['first_name']),
                'user_id': request.user.id,
            }
            audit_log(audit)
            return Response(response, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk):
        # turn request to processing
        user = request.user
        request_data = Request.objects.get(id=pk)
        request_status = request.POST['status']
        previous_status = request_data.status
        if 'update_status' in request.POST and request.POST['update_status'] == 'UPDATE':
            self.create_transaction('UPDATE', request_status, request_data, request)
            if 'waived_at' in request.POST:
                request_data.waived_at = request.POST['waived_at']
            else:
                request_data.waived_at = None

            # if request.POST['type'] == 'DEPOSIT':
            #     wallet = Wallet.objects.filter(user_id=user.id, type='PENDING_DEPOSIT')[0]
            #     actual_amount = wallet.balance + (float(request_data.amount) - float(request_data.fee))
            #     self.update_wallet_by_type('PENDING_DEPOSIT', user.id, actual_amount, request)
            # elif request.POST['type'] == 'PAYMENT':
            #     wallet = Wallet.objects.filter(user_id=user.id, type='PENDING_DEPOSIT')[0]
            #     actual_amount = wallet.balance + (float(request_data.amount) - float(request_data.fee))
            #     self.update_wallet_by_type('PENDING_DEPOSIT', user.id, actual_amount, request)
        elif request_status == 'SCHEDULED':
            # create scheduled transaction
            request_data.pickup_at = request.POST['pickup_at']
            request_data.due_at = request.POST['due_at']
            self.create_transaction('PICKUP', request_status, request_data, request)
            self.create_transaction('TRANSFER', request_status, request_data, request)
            if (request_data.type == 'DEPOSIT'):
                ticket = BuildDepositTicket.build(request_data)
                self.upload_ticket(ticket, request_data)
        elif request_status == 'COMPLETED':
            request_data.pickup_at = request.POST['pickup_at']
            request_data.due_at = request.POST['due_at']
            # update wallet
            data_for_wallet = {
                'type': request_data.type,
                'status': 'COMPLETED',
                'user': {'id': request_data.user_id},
                'amount': request_data.amount,
                'fee': request_data.fee,
                'request_id': request_data.request_id,
            }
            self.update_wallet(data_for_wallet, request)
            self.create_transaction('PICKUP', request_status, request_data, request)
            self.create_transaction('TRANSFER', request_status, request_data, request)
            # update fee of other transactions
            self.update_fee_transaction(self, request_data)
            self.update_fee_pending_transactions(self, request_data)
        elif request_status == 'PROCESSING':
            self.create_transaction('PROCESSING', request_status, request_data, request)
        elif request_status == 'CANCELLED':
            self.create_transaction('CANCELLED', request_status, request_data, request)
            data_for_wallet = {
                'type': request_data.type,
                'status': 'CANCELLED',
                'user': {'id': request_data.user_id},
                'amount': request_data.amount,
                'fee': request_data.fee,
            }
            self.update_wallet(data_for_wallet, request)

        request_data.due_at = request.POST['due_at']
        request_data.pickup_at = request.POST['pickup_at']
        request_data.amount = request.POST['amount']
        request_data.fee = request.POST['fee']
        request_data.type = request.POST['type']
        request_data.transaction_type = request.POST['transaction_type']
        request_data.updated_at = datetime.datetime.now()
        request_data.updated_by = user.id
        request_data.status = request.POST['status']
        request_data.save()

        # send data
        # self.send_email_notif(pk)

        serializer = RequestSerializer(request_data)
        audit = {
            'module': 'request',
            'transaction': 'processing member {transaction}'.format(transaction=serializer.data['type']),
            'description':
                'processing request_id {request_id} for user {user} from {previous_status} to {status}'
                    .format(previous_status=previous_status,
                            status=request_status,
                            request_id=request_data.request_id,
                            user=serializer.data['user']['first_name'],
                            ),
            'user_id': request.user.id,
        }
        audit_log(audit)
        self.update_wallet_pending(request_data.user_id)
        response = ResponseTemplate.success('v1', serializer.data, 'Request Updated.')

        return Response(response, status.HTTP_200_OK)

    @staticmethod
    def update_wallet_pending(user_id):
        print (user_id)
        pending_deposit = Request.objects.filter(user_id=user_id,
                                                type='DEPOSIT') \
            .filter(~Q(status='COMPLETED'))\
            .filter(~Q(status='CANCELLED'))\
            .aggregate(pending_deposit=Sum('amount') - Sum('fee'))

        pending_payment = Request.objects.filter(user_id=user_id,
                                                 type='PAYMENT') \
            .filter(~Q(status='COMPLETED')) \
            .filter(~Q(status='CANCELLED')) \
            .aggregate(pending_payment=Sum('amount') + Sum('fee'))

        print (pending_payment)
        print (pending_deposit)
        wallet = Wallet.objects.get(user_id=user_id, type='PENDING_DEPOSIT')
        wallet.balance = pending_deposit['pending_deposit'] if pending_deposit['pending_deposit'] != None else 0
        wallet.updated_by = user_id
        wallet.updated_at = datetime.datetime.now()
        wallet.save()

        wallet = Wallet.objects.get(user_id=user_id, type='PENDING_PAYMENT')
        wallet.balance = pending_payment['pending_payment'] if pending_payment['pending_payment'] != None else 0
        wallet.updated_by = user_id
        wallet.updated_at = datetime.datetime.now()
        wallet.save()


    @staticmethod
    def get_created_date_filter(params, request_data):
        if params.get('period') == 'custom':
            request_data = request_data \
                .filter(due_at__range=[params.get('from'), params.get('to')])
        elif params.get('period') == 'today':
            today = datetime.date.today() - datetime.timedelta(hours=8)
            print(today)
            request_data = request_data.filter(due_at__contains=today)
        elif params.get('period') == 'next_3_days':
            today = datetime.date.today()
            next_three_days = today + datetime.timedelta(days=3)
            request_data = request_data.filter(due_at__range=[today, next_three_days])
        elif params.get('period') == 'this_week':
            today = datetime.date.today()
            week = self.week_range(today)
            request_data = request_data.filter(due_at__range=week)
        elif params.get('period') == 'this_month':
            current_month = datetime.datetime.now().month
            request_data = request_data.filter(due_at__month=current_month)
        elif params.get('period') == 'this_quarter':
            quarter = self.quarter_range()
            request_data = request_data.filter(due_at__range=quarter)
        elif params.get('period') == 'last_30_days':
            today = datetime.date.today()
            last_30_days = datetime.date.today() - datetime.timedelta(days=30)
            request_data = request_data.filter(due_at__range=[last_30_days, today])
        elif params.get('period') == 'last_90_days':
            today = datetime.date.today()
            last_30_days = datetime.date.today() - datetime.timedelta(days=90)
            request_data = request_data.filter(due_at__range=[last_30_days, today])
        elif params.get('period') == 'last_month':
            current_month = datetime.datetime.now().month
            print (current_month)
            request_data = request_data.filter(due_at__month=current_month - 1)
        elif params.get('period') == 'last_quarter':
            quarter = self.last_quarter_range()
            request_data = request_data.filter(due_at__range=quarter)
        elif params.get('period') == 'last_year':
            year = datetime.date.now().year - 1
            request_data = request_data.filter(due_at__year=year)

        return request_data

    @staticmethod
    def get_payment_date_filter(params, request_data):
        if params.get('period') == 'custom':
            request_data = request_data \
                .filter(due_at__range=[params.get('from'), params.get('to')])
        elif params.get('period') == 'today':
            today = datetime.date.today() - datetime.timedelta(hours=8)
            request_data = request_data.filter(due_at__contains=today)
        elif params.get('period') == 'next_3_days':
            today = datetime.date.today()
            next_three_days = today + datetime.timedelta(days=3)
            request_data = request_data.filter(due_at__range=[today, next_three_days])
        elif params.get('period') == 'this_week':
            today = datetime.date.today()
            week = self.week_range(today)
            request_data = request_data.filter(due_at__range=week)
        elif params.get('period') == 'this_month':
            current_month = datetime.datetime.now().month
            request_data = request_data.filter(due_at__month=current_month)
        elif params.get('period') == 'this_quarter':
            quarter = self.quarter_range()
            request_data = request_data.filter(due_at__range=quarter)
        elif params.get('period') == 'last_30_days':
            today = datetime.date.today()
            last_30_days = datetime.date.today() - datetime.timedelta(days=30)
            request_data = request_data.filter(due_at__range=[last_30_days, today])
        elif params.get('period') == 'last_90_days':
            today = datetime.date.today()
            last_30_days = datetime.date.today() - datetime.timedelta(days=90)
            request_data = request_data.filter(due_at__range=[last_30_days, today])
        elif params.get('period') == 'last_month':
            current_month = datetime.datetime.now().month
            print (current_month)
            request_data = request_data.filter(due_at__month=current_month - 1)
        elif params.get('period') == 'last_quarter':
            quarter = self.last_quarter_range()
            request_data = request_data.filter(due_at__range=quarter)
        elif params.get('period') == 'last_year':
            year = datetime.date.now().year - 1
            request_data = request_data.filter(due_at__year=year)

        return request_data

    @staticmethod
    def get_pickup_date_filter(params, request_data):
        if params.get('period') == 'custom':
            request_data = request_data \
                .filter(pickup_at__range=[params.get('from'), params.get('to')])
        elif params.get('period') == 'today':
            today = datetime.date.today()
            request_data = request_data.filter(pickup_at__contains=today)
        elif params.get('period') == 'next_3_days':
            today = datetime.date.today()
            next_three_days = today + datetime.timedelta(days=3)
            request_data = request_data.filter(pickup_at__range=[today, next_three_days])
        elif params.get('period') == 'this_week':
            today = datetime.date.today()
            week = self.week_range(today)
            request_data = request_data.filter(pickup_at__range=week)
        elif params.get('period') == 'this_month':
            current_month = datetime.datetime.now().month
            request_data = request_data.filter(pickup_at__month=current_month)
        elif params.get('period') == 'this_quarter':
            quarter = self.quarter_range()
            request_data = request_data.filter(pickup_at__range=quarter)
        elif params.get('period') == 'last_30_days':
            today = datetime.date.today()
            last_30_days = datetime.date.today() - datetime.timedelta(days=30)
            request_data = request_data.filter(pickup_at__range=[last_30_days, today])
        elif params.get('period') == 'last_90_days':
            today = datetime.date.today()
            last_30_days = datetime.date.today() - datetime.timedelta(days=90)
            request_data = request_data.filter(pickup_at__range=[last_30_days, today])
        elif params.get('period') == 'last_month':
            current_month = datetime.datetime.now().month
            print (current_month)
            request_data = request_data.filter(pickup_at__month=current_month - 1)
        elif params.get('period') == 'last_quarter':
            quarter = self.last_quarter_range()
            request_data = request_data.filter(pickup_at__range=quarter)
        elif params.get('period') == 'last_year':
            year = datetime.date.now().year - 1
            request_data = request_data.filter(pickup_at__year=year)

        return request_data

    @staticmethod
    def send_email_notif(pk):
        subject = '[REQUEST CHANGES ALERT] Request has been modified.'
        to = [settings.TITAN_STAFF_EMAIL]
        from_email = 'Titan Vault <no-reply@zeuss.tech>'
        reply_to = [settings.TITAN_REPLY_TO_EMAIL]

        ctx = {
            'link': "{server}/request/{pk}/".format(server=settings.APP_URL, pk=pk)
        }

        message = get_template('change_request_alert.html').render(ctx)
        msg = EmailMessage(subject, message, to=to, from_email=from_email, reply_to=reply_to)
        msg.content_subtype = 'html'

        msg.send()

        return False

    @staticmethod
    def update_file(data):
        if data['user']['parent_user_id']:
            files = File.objects.filter(user_id=data['user']['parent_user_id'], resource_id=None).update(resource_id=data['id'])
        else:
            files = File.objects.filter(user_id=data['user']['id'], resource_id=None).update(resource_id=data['id'])
        return files

    @staticmethod
    def create_transaction(type, status, request_data, request):
        user = request.user
        transaction = Transaction(status=status,
                                  type=type,
                                  created_by=user.id,
                                  updated_by=user.id,
                                  request_id=request_data.id,
                                  user_id=request_data.user_id,
                                  to=request.POST['to'],
                                  process_at=datetime.datetime.now(),
                                  created_at=datetime.datetime.now(),
                                  updated_at=datetime.datetime.now(),
                                  amount=request_data.amount,
                                  remarks=request.data['remarks'])
        transaction.save()

        return transaction

    @staticmethod
    def update_wallet_by_type(wallet_type, owner_id, amount, request):
        print(wallet_type)
        print(amount)
        user = request.user

        wallet = Wallet.objects.get(user_id=owner_id, type=wallet_type)
        wallet.balance = amount
        wallet.updated_by = user.id
        wallet.updated_at = datetime.datetime.now()
        wallet.save()

        return wallet

    def update_wallet(self, data, request):
        user = request.user
        print(data)
        transaction_type = data['type']
        transaction_status = data['status']
        user_id = data['user']['id']
        amount = 0

        wallet = None
        if transaction_status == 'COMPLETED':
            # completed
            actual_wallet = wallet = Wallet.objects.filter(user_id=user_id, type='AVAILABLE_BALANCE')[0]
            actual_amount = 0
            if transaction_type == 'PAYMENT':
                wallet = Wallet.objects.filter(user_id=user_id, type='PENDING_PAYMENT')[0]
                print(wallet.balance)
                amount = wallet.balance - (float(data['amount']) + float(data['fee']))
                print (amount)
                actual_amount = actual_wallet.balance - (float(data['amount']) + float(data['fee']))
                print (actual_amount)
                self.update_wallet_by_type('PENDING_PAYMENT', user_id, amount, request)
            elif transaction_type == 'DEPOSIT':
                wallet = Wallet.objects.filter(user_id=user_id, type='PENDING_DEPOSIT')[0]
                amount = wallet.balance - (float(data['amount']) - float(data['fee']))
                actual_amount = actual_wallet.balance + (float(data['amount']) - float(data['fee']))
                self.update_wallet_by_type('PENDING_DEPOSIT', user_id, amount, request)

            self.update_wallet_by_type('AVAILABLE_BALANCE', user_id, actual_amount, request)
            self.update_ledger(data['request_id'], request, transaction_type, amount, actual_amount)
        elif transaction_status == 'CANCELLED':
            # cancelled
            if transaction_type == 'PAYMENT':
                wallet_payment = Wallet.objects.filter(user_id=user_id, type='PENDING_PAYMENT')[0]
                wallet_payment.balance -= float(data['amount']) + float(data['fee'])
                wallet_payment.updated_by = user.id
                wallet_payment.updated_at = datetime.datetime.now()
                wallet_payment.save()
            elif transaction_type == 'DEPOSIT':
                wallet_deposit = Wallet.objects.filter(user_id=user_id, type='PENDING_DEPOSIT')[0]
                wallet_deposit.balance -= float(data['amount']) - float(data['fee'])
                wallet_deposit.updated_by = user.id
                wallet_deposit.updated_at = datetime.datetime.now()
                wallet_deposit.save()
        else:
            # pending
            if transaction_type == 'PAYMENT':
                wallet_payment = Wallet.objects.filter(user_id=user_id, type='PENDING_PAYMENT')[0]
                wallet_payment.balance += float(data['amount']) + float(data['fee'])
                wallet_payment.updated_by = user.id
                wallet_payment.updated_at = datetime.datetime.now()
                wallet_payment.save()
            elif transaction_type == 'DEPOSIT':
                wallet_deposit = Wallet.objects.filter(user_id=user_id, type='PENDING_DEPOSIT')[0]
                wallet_deposit.balance += float(data['amount']) - float(data['fee'])
                wallet_deposit.updated_by = user.id
                wallet_deposit.updated_at = datetime.datetime.now()
                wallet_deposit.save()

        return wallet

    @staticmethod
    def create_ledger(data, request):
        user = request.user
        request_data = Request.objects.get(id=data['id'])
        print(request_data.request_id)

        resulting_available_balance_value = 0
        resulting_pending_payment_balance_value = 0
        resulting_pending_deposit_balance_value = 0

        if request_data.type == 'DEPOSIT':
            resulting_pending_deposit_balance_value = float(request_data.amount) - float(request_data.fee)

        if request_data.type == 'PAYMENT':
            resulting_pending_payment_balance_value = float(request_data.amount) + float(request_data.fee)

        ledger = Ledger(
            transaction_id=request_data.request_id,
            transaction_status='pending',
            amount=request_data.amount,
            resulting_available_balance=resulting_available_balance_value,
            resulting_pending_payment_balance=resulting_pending_payment_balance_value,
            resulting_pending_deposit_balance=resulting_pending_deposit_balance_value,
            notes='created transaction',
            user_id=request_data.user_id,
            created_by=user.id,
            updated_by=user.id,
            created_at=datetime.datetime.now(),
            updated_at=datetime.datetime.now(),
        )

        ledger.save()

        return ledger

    @staticmethod
    def update_ledger(request_id, request, transaction_type, amount, actual_amount):
        user = request.user
        request_data = Ledger.objects.filter(transaction_id=request_id)[0]

        resulting_pending_payment_balance = 0
        resulting_pending_deposit_balance = 0

        if transaction_type == 'PAYMENT':
            resulting_pending_payment_balance = amount

        if transaction_type == 'DEPOSIT':
            resulting_pending_deposit_balance = amount

        request_data.amount = request_data.amount
        request_data.resulting_available_balance = actual_amount
        request_data.resulting_pending_payment_balance = resulting_pending_payment_balance
        request_data.resulting_pending_deposit_balance = resulting_pending_deposit_balance
        request_data.transaction_status = 'completed'
        request_data.updated_by = user.id
        request_data.updated_at = datetime.datetime.now()

        request_data.save()

        return request_data

    @staticmethod
    def update_fee_pending_transactions(self, request_data):
        fee = Fee.objects.filter(user_id=request_data.user_id)
        fee_data = FeeSerializer(fee, many=True)
        if fee_data.data[0]['fee_setting'] == 'flat_rate':
            print ('flat rate')
            return None

        # get request not completed and cancelled
        requests = Request.objects\
            .filter(~Q(status='COMPLETED'))\
            .filter(
                user_id=request_data.user_id,
                type=request_data.type)

        # loop request
        for request in requests:
            print (request)
            # update_fee
            computed_fee = self.compute_fee(request)
            request_update = Request.objects.get(id=request.id)
            request_update.fee = computed_fee['total_fee_amount']
            request_update.updated_at = datetime.datetime.now()
            request_update.save()

        # update
        return None

    @staticmethod
    def update_fee_transaction(self, request):
        # formDataFeeTransaction.append('user_id', data.data.user.id);
        # formDataFeeTransaction.append('type', requestType.toLowerCase());
        # formDataFeeTransaction.append('fee_percent', feePercent);
        # formDataFeeTransaction.append('transaction_amount', data.data.amount);
        # formDataFeeTransaction.append(
        #     'total_transaction_amount',
        #     feeTotalTransactionAmount,
        # );
        #
        # formDataFeeTransaction.append('process_at', `${payAt}
        # 00:00:00
        # `);
        # formDataFeeTransaction.append('request_id', requestId);
        # formDataFeeTransaction.append('status', 'completed');
        # formDataFeeTransaction.append('fee_amount', feeTotal.toFixed(2));
        # user_id, type, fee_percent, transaction_amount, total_transaction_amount, fee_amount
        fee = Fee.objects.filter(user_id=request.user.id)
        fee_data = FeeSerializer(fee, many=True)
        if fee_data.data[0]['fee_setting'] == 'flat_rate':
            return None

        computed_fee = self.compute_fee(request)

        fee_transaction = FeeTransaction(
            user_id=request.user_id,
            type=request.type.lower(),
            fee_percent=computed_fee['fee_percent'],
            transaction_amount=request.amount,
            total_transaction_amount=computed_fee['total_transaction_amount'],
            process_at=datetime.datetime.now(),
            request_id=request.id,
            status='completed',
            fee_amount=computed_fee['total_fee_amount'],
            created_by=request.user_id,
            updated_by=request.user_id,
        )
        fee_transaction.save()
        return None

    @staticmethod
    def compute_fee(request):
        logger = logging.getLogger(__name__)
        total_transaction_amount = \
            Computation.get_total_transaction_amount(
                request.created_at.strftime('%Y-%m-%d'),
                request.type,
                request.user_id,
            )
        total_transaction_amount = float(total_transaction_amount) + float(request.amount)
        if request.transaction_type == 'ELECTRONIC':
            return {
                'total_fee_amount': 0,
                'fee_percent': 0,
                'total_transaction_amount': total_transaction_amount,
            }

        fee_tier_data = Computation.get_fee_tier(request.user_id)
        threshold_type = 'deposit_threshold'
        rate_type = 'deposit_rate'
        if request.type == 'PAYMENT':
            threshold_type = 'payment_threshold'
            rate_type = 'payment_rate'

        segragated_amount = \
            Computation.segregate_amount_by_tier(
                fee_tier_data.data,
                request.amount,
                total_transaction_amount,
                threshold_type,
            )

        fee_percent = Computation.get_fee_percent(segragated_amount, fee_tier_data, rate_type)

        total_fee_amount = \
            Computation.compute_fee(
                logger,
                segragated_amount,
                rate_type,
                fee_tier_data,
                total_transaction_amount,
                request.type,
            )

        return {
            'total_fee_amount': total_fee_amount,
            'fee_percent': fee_percent,
            'total_transaction_amount': total_transaction_amount,
        }

    @staticmethod
    def week_range(date):
        year, week, dow = date.isocalendar()
        if dow == 7:
            start_date = date
        else:
            start_date = date - datetime.timedelta(dow)

        end_date = start_date + datetime.timedelta(6)

        return start_date, end_date

    @staticmethod
    def quarter_range():
        current_date = datetime.datetime.now()
        current_quarter = round(current_date.month / 3)
        start_date = datetime.datetime(current_date.year, 3 * current_quarter - 2, 1)
        end_date = current_date
        return start_date, end_date

    @staticmethod
    def last_quarter_range():
        current_date = datetime.datetime.now() - datetime.timedelta(days=30)
        current_quarter = round(current_date.month / 3) - 1
        start_date = datetime.datetime(current_date.year, 3 * current_quarter - 2, 1)
        end_date = datetime.datetime(current_date.year, start_date.month + 3, 1)
        return start_date, end_date

    @staticmethod
    def upload_ticket(file_name, request_data):
        output_file = open(file_name, "rb")

        try:
            file = File.objects.get(resource_id=request_data.id, type='DEPOSIT_TICKET')
            file.filename = FileObject(output_file)
            file.save()
        except:
            file = File(
                user_id=request_data.user_id,
                filename=FileObject(output_file),
                url='filename',
                mime_type='application/pdf',
                expire_at=datetime.datetime.now() + datetime.timedelta(hours=1),
                type='DEPOSIT_TICKET',
                resource_id=request_data.id,
                created_by=request_data.user_id,
                updated_by=request_data.user_id,
                created_at=datetime.datetime.now(),
                updated_at=datetime.datetime.now(),
            )
            file.save()

        return file
