from rest_framework.views import APIView
from rest_framework.permissions import AllowAny

from django.http import HttpResponse
from django.template import Context
from django.template.loader import render_to_string, get_template
from django.core.mail import EmailMessage


class EmailView(APIView):
    permission_classes = [
        AllowAny
    ]

    def get(self, request):
        subject = "This is an HTML email"
        to = ['rodolfo.borgonos@flightdigitalmedia.com']
        from_email = 'Titan Vault Jun'
        reply_to = ['rodolfoborgonos@gmail.com']

        ctx = {
            'user': 'Jun Borgonos',
            'message': 'Welcome to Titan Vault'
        }

        messeage = get_template('user_template.html').render(ctx)
        msg = EmailMessage(subject, messeage, to=to, from_email=from_email, reply_to=reply_to)
        msg.content_subtype = 'html'

        msg.send()

        return HttpResponse('email sent')
