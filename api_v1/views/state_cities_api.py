from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
import datetime
import requests
import json


class StateCitiesAPI(APIView):
    def get(self, request):
        try:
            data = requests.get('https://raw.githubusercontent.com/cschoi3/US-states-and-cities-json/master/data.json')
            return Response({'data': json.loads(data.content)}, status=status.HTTP_200_OK)
        except:
            return Response({'error': 'Cannot pull record'}, status=status.HTTP_400_BAD_REQUEST)
