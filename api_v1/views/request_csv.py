from rest_framework.response import Response
from django.db import IntegrityError
from rest_framework.views import APIView
from rest_framework import status
from ..models import Request, User
from ..serializers import RequestSerializer
from .constant import Constant
import datetime
import csv
from django.http import HttpResponse


class RequestCsv(APIView):

    def get_data(self, data):
        return "{data}".format(data=data)

    def get(self, request):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="request_data.csv"'

        request_data = Request.objects.order_by('pickup_at', 'due_at')
        params = self.request.query_params
        if Constant.FIELD_USER_ID in params:
            user = User.objects.get(id=params.get('user_id'))
            if user.parent_user_id is None:
                user_id = user.id
            else:
                user_id = user.parent_user_id
            request_data = request_data.filter(user_id=user_id)

        if Constant.FIELD_TYPE in params:
            request_data = request_data.filter(type=params.get('type'))

        if Constant.FIELD_STATUS in params:
            request_data = request_data.filter(status=params.get('status'))

        if Constant.FIELD_DELIVERY_METHOD in params:
            request_data = request_data.filter(type=params.get('status'))

        if Constant.FIELD_DATE in params:
            # custom
            if params.get('date') == 'created_date':
                if params.get('period') == 'custom':
                    request_data = request_data \
                        .filter(created_at__range=[params.get('from'), params.get('to')])
                elif params.get('period') == 'today':
                    today = datetime.date.today() - datetime.timedelta(hours=8)
                    print(today)
                    request_data = request_data.filter(created_at__contains=today)
                elif params.get('period') == 'next_3_days':
                    next_three_days = datetime.date.today() + datetime.timedelta(days=3)
                    today = datetime.date.today()
                    request_data = request_data.filter(created_at__range=[today, next_three_days])
                elif params.get('period') == 'this_week':
                    today = datetime.date.today()
                    week = self.week_range(today)
                    request_data = request_data.filter(created_at__range=week)
                elif params.get('period') == 'this_month':
                    current_month = datetime.datetime.now().month
                    request_data = request_data.filter(created_at__month=current_month)
                elif params.get('period') == 'this_quarter':
                    quarter = self.quarter_range()
                    request_data = request_data.filter(created_at__range=quarter)
                elif params.get('period') == 'last_30_days':
                    today = datetime.date.today()
                    last_30_days = datetime.date.today() - datetime.timedelta(days=30)
                    request_data = request_data.filter(created_at__range=[last_30_days, today])
                elif params.get('period') == 'last_90_days':
                    today = datetime.date.today()
                    last_30_days = datetime.date.today() - datetime.timedelta(days=90)
                    request_data = request_data.filter(created_at__range=[last_30_days, today])
                elif params.get('period') == 'last_month':
                    current_month = datetime.datetime.now().month
                    print (current_month)
                    request_data = request_data.filter(created_at__month=current_month - 1)
                elif params.get('period') == 'last_quarter':
                    quarter = self.last_quarter_range()
                    request_data = request_data.filter(created_at__range=quarter)
                elif params.get('period') == 'last_year':
                    year = datetime.date.now().year - 1
                    request_data = request_data.filter(created_at__year=year)

            elif params.get('date') == 'payment_date':
                print ('payment date csv')
                if params.get('period') == 'custom':
                    request_data = request_data \
                        .filter(due_at__range=[params.get('from'), params.get('to')])
                elif params.get('period') == 'today':
                    today = datetime.date.today() - datetime.timedelta(hours=8)
                    print(today)
                    request_data = request_data.filter(due_at__contains=today)
                elif params.get('period') == 'next_3_days':
                    next_three_days = datetime.date.today() + datetime.timedelta(days=3)
                    today = datetime.date.today()
                    request_data = request_data.filter(due_at__range=[today, next_three_days])
                elif params.get('period') == 'this_week':
                    today = datetime.date.today()
                    week = self.week_range(today)
                    request_data = request_data.filter(due_at__range=week)
                elif params.get('period') == 'this_month':
                    current_month = datetime.datetime.now().month
                    request_data = request_data.filter(due_at__month=current_month)
                elif params.get('period') == 'this_quarter':
                    quarter = self.quarter_range()
                    request_data = request_data.filter(due_at__range=quarter)
                elif params.get('period') == 'last_30_days':
                    today = datetime.date.today()
                    last_30_days = datetime.date.today() - datetime.timedelta(days=30)
                    request_data = request_data.filter(due_at__range=[last_30_days, today])
                elif params.get('period') == 'last_90_days':
                    today = datetime.date.today()
                    last_30_days = datetime.date.today() - datetime.timedelta(days=90)
                    request_data = request_data.filter(due_at__range=[last_30_days, today])
                elif params.get('period') == 'last_month':
                    current_month = datetime.datetime.now().month
                    print (current_month)
                    request_data = request_data.filter(due_at__month=current_month - 1)
                elif params.get('period') == 'last_quarter':
                    quarter = self.last_quarter_range()
                    request_data = request_data.filter(due_at__range=quarter)
                elif params.get('period') == 'last_year':
                    year = datetime.date.now().year - 1
                    request_data = request_data.filter(due_at__year=year)

            elif params.get('date') == 'pickup_date':
                if params.get('period') == 'custom':
                    request_data = request_data \
                        .filter(pickup_at__range=[params.get('from'), params.get('to')])
                elif params.get('period') == 'today':
                    today = datetime.date.today()
                    request_data = request_data.filter(pickup_at__contains=today)
                elif params.get('period') == 'next_3_days':
                    next_three_days = datetime.date.today() + datetime.timedelta(days=3)
                    today = datetime.date.today()
                    request_data = request_data.filter(pickup_at__range=[today, next_three_days])
                elif params.get('period') == 'this_week':
                    today = datetime.date.today()
                    week = self.week_range(today)
                    request_data = request_data.filter(pickup_at__range=week)
                elif params.get('period') == 'this_month':
                    current_month = datetime.datetime.now().month
                    request_data = request_data.filter(pickup_at__month=current_month)
                elif params.get('period') == 'this_quarter':
                    quarter = self.quarter_range()
                    request_data = request_data.filter(pickup_at__range=quarter)
                elif params.get('period') == 'last_30_days':
                    today = datetime.date.today()
                    last_30_days = datetime.date.today() - datetime.timedelta(days=30)
                    request_data = request_data.filter(pickup_at__range=[last_30_days, today])
                elif params.get('period') == 'last_90_days':
                    today = datetime.date.today()
                    last_30_days = datetime.date.today() - datetime.timedelta(days=90)
                    request_data = request_data.filter(pickup_at__range=[last_30_days, today])
                elif params.get('period') == 'last_month':
                    current_month = datetime.datetime.now().month
                    print (current_month)
                    request_data = request_data.filter(pickup_at__month=current_month - 1)
                elif params.get('period') == 'last_quarter':
                    quarter = self.last_quarter_range()
                    request_data = request_data.filter(pickup_at__range=quarter)
                elif params.get('period') == 'last_year':
                    year = datetime.date.now().year - 1
                    request_data = request_data.filter(pickup_at__year=year)

        if 'payee_id' in params:
            request_data = request_data.filter(payee_id=params.get('payee_id'))

        if 'category' in params:
            request_data = request_data.filter(type=params.get('category'))

        # request_data
        writer = csv.writer(response)

        if params.get('user_report') == Constant.USER_SUPER_ADMIN:
            report_header = [
                'type',
                'member_name',
                'user_name',
                'member_id',
                'transaction_id',
                'transaction_date',
                'transaction_time',
                'cash_pickup_date',
                'cash_pickup_time',
                'send_to_memo',
                'amount',
                'fee',
                'net_total',
                'status',
                'payee_name',
                'payee_address',
                'payee_bank_name',
                'payee_bank_account_name',
                'payee_bank_account',
                'payee_bank_routing_number',
                'payee_email',
            ]
            writer.writerow(report_header)
            for data in request_data:
                data = RequestSerializer(data).data
                member_name = "{first_name} {last_name}".format(first_name=data['user']['first_name'],
                                                                last_name=data['user']['last_name'])
                net_total = 0
                payee_name = data['payee']['name']
                payee_address = data['payee']['address']
                payee_bank_name = data['payee']['bank_name']
                payee_bank_account_name = data['payee']['bank_account_name']
                payee_bank_account = data['payee']['bank_account']
                payee_bank_routing_number = data['payee']['bank_routing_number']
                payee_email = data['payee']['email']
                fee = "-{fee}".format(fee=data['fee'])
                amount = data['amount']
                date_format = Constant.DATE_FORMAT
                time_format = Constant.TIME_FORMAT
                date_time_format = Constant.DB_DATE_TIMESTAMP
                date_time_format_at = Constant.DATE_TIME_FORMAT_AT
                if data['status'] == Constant.STATUS_COMPLETED:
                    due_at = datetime.datetime.strptime(data['due_at'], date_time_format_at)
                else:
                    due_at = datetime.datetime.strptime(data['due_at'], date_time_format_at)
                transaction_date = due_at.strftime(date_format)
                transaction_time = due_at.strftime(time_format)
                pickup_at = datetime.datetime.strptime(data['pickup_at'], date_time_format_at) + datetime.timedelta(hours=8)
                pickup_date = pickup_at.strftime(date_format)
                pickup_time = pickup_at.strftime(time_format)
                if data['type'] == Constant.TYPE_PAYMENT:
                    net_total = float(data['amount']) + float(data['fee'])
                    amount = "-{amount}".format(amount=data['amount'])
                if data['type'] == Constant.TYPE_DEPOSIT:
                    net_total = float(data['amount']) - float(data['fee'])
                    payee_name = ''
                    payee_address = ''
                    payee_bank_name = ''
                    payee_bank_account_name = ''
                    payee_bank_account = ''
                    payee_bank_routing_number = ''
                    payee_email = ''

                writer.writerow([
                    self.get_data(data['type']),
                    self.get_data(data['user']['business_name']),
                    self.get_data(member_name),
                    self.get_data(data['user']['username']),
                    self.get_data(data['request_id']),
                    self.get_data(transaction_date),
                    self.get_data(transaction_time),
                    self.get_data(pickup_date),
                    self.get_data(pickup_time),
                    self.get_data(data['remarks']),
                    self.get_data(amount),
                    self.get_data(fee),
                    self.get_data(net_total),
                    self.get_data(data['status']),
                    self.get_data(payee_name),
                    self.get_data(payee_address),
                    self.get_data(payee_bank_name),
                    self.get_data(payee_bank_account_name),
                    self.get_data(payee_bank_account),
                    self.get_data(payee_bank_routing_number),
                    self.get_data(payee_email),
                ])
        elif params.get('user_report') == Constant.USER_MEMBER or params.get('user_report') == Constant.USER_MEMBER_USER:
            report_header = [
                'type',
                'member_name',
                'user_name',
                'member_id',
                'transaction_id',
                'transaction_date',
                'transaction_time',
                'send_to',
                'send_to_memo',
                'amount',
                'fee',
                'net_total',
                'status',
            ]
            # Type	Member Name	User Name	Member ID	Transaction ID	Send to	Memo	Amount	Fee	Net Total	Status

            writer.writerow(report_header)
            for data in request_data:
                data = RequestSerializer(data).data
                member_name = "{first_name} {last_name}".format(first_name=data['user']['first_name'],
                                                                last_name=data['user']['last_name'])
                payee_name = ''
                net_total = 0
                fee = "-{fee}".format(fee=data['fee'])
                amount = data['amount']
                date_format = Constant.DATE_FORMAT
                time_format = Constant.TIME_FORMAT
                date_time_format = Constant.DB_DATE_TIMESTAMP
                date_time_format_due_at = Constant.DATE_TIME_FORMAT_AT
                if data['status'] == Constant.STATUS_COMPLETED:
                    due_at = datetime.datetime.strptime(data['due_at'], date_time_format_due_at)
                else:
                    due_at = datetime.datetime.strptime(data['due_at'], date_time_format_due_at)
                transaction_date = due_at.strftime(date_format)
                transaction_time = due_at.strftime(time_format)
                if data['type'] == Constant.TYPE_PAYMENT:
                    payee_name = data['payee']['name']
                    amount = "-{amount}".format(amount=data['amount'])
                    net_total = float(data['amount']) + float(data['fee'])

                if data['type'] == Constant.TYPE_DEPOSIT:
                    payee_name = ''
                    net_total = float(data['amount']) - float(data['fee'])

                writer.writerow([
                    self.get_data(data['type']),
                    self.get_data(data['user']['business_name']),
                    self.get_data(member_name),
                    self.get_data(data['user']['username']),
                    self.get_data(data['request_id']),
                    self.get_data(transaction_date),
                    self.get_data(transaction_time),
                    self.get_data(payee_name),
                    self.get_data(data['remarks']),
                    self.get_data(amount),
                    self.get_data(fee),
                    self.get_data(net_total),
                    self.get_data(data['status']),
                ])

        return response
