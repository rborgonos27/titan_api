from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework import status

from rest_framework.response import Response
from django.template import Context
from django.template.loader import render_to_string, get_template
from django.core.mail import EmailMessage
from django.conf import settings


class ContactView(APIView):
    permission_classes = [
        AllowAny
    ]

    def post(self, request):
        fail_response = {
            'version': 'v1',
            'success': False,
            'data': None,
            'message': 'Incomplete field(s).'
        }

        if 'name' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'business_name' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'email' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'contact_number' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        website = ''
        if 'website' in request.POST:
            website = request.POST['website']

        subject = '[NEW CLIENT ALERT] Congratulations you have new client!'
        to = [settings.TITAN_STAFF_EMAIL]
        from_email = 'Titan Vault <no-reply@zeuss.tech>'
        reply_to = [settings.TITAN_REPLY_TO_EMAIL]

        ctx = {
            'name': request.POST['name'],
            'email': request.POST['email'],
            'business_name': request.POST['business_name'],
            'contact_number': request.POST['contact_number'],
            'website': website,
        }

        message = get_template('new_contacts_template.html').render(ctx)
        msg = EmailMessage(subject, message, to=to, from_email=from_email, reply_to=reply_to)
        msg.content_subtype = 'html'

        msg.send()

        notif_subject = 'Welcome to Titan Vault'
        notif_to = [request.POST['email']]
        notif_from_email = 'Titan Vault <no-reply@titan-vault.com>'
        notif_reply_to = [settings.TITAN_NO_REPLY_EMAIL]

        message = get_template('notification_template.html').render(ctx)
        notif = EmailMessage(notif_subject, message, to=notif_to, from_email=notif_from_email, reply_to=notif_reply_to)
        notif.content_subtype = 'html'

        notif.send()

        response = {
            'version': 'v1',
            'success': True,
            'data': None,
            'message': 'Thank you for interest. Let''s speak soon!'
        }

        return Response(response, status.HTTP_200_OK)
