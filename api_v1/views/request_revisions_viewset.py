from rest_framework.response import Response
from rest_framework import generics
from rest_framework import status
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.settings import api_settings

from ..models import RequestRevisions
from ..serializers import RequestRevisionsSerializer


class RequestRevisionAPI(generics.ListAPIView):
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS
    serializer_class = RequestRevisionsSerializer

    filter_backends = (SearchFilter, OrderingFilter,)

    def get_queryset(self):
        user = self.request.user
        request_id = self.request.query_params.get('request_id')
        if (request_id):
            return RequestRevisions.objects.filter(request_id=request_id)
        else:
            return RequestRevisions.objects.filter(user=user)
