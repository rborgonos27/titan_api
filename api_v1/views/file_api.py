from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser, FormParser
from ..models import File
from ..serializers import FileSerializer
from rest_framework import status


class FileAPI(viewsets.ModelViewSet):
    serializer_class = FileSerializer
    file = File.objects.all()

    parser_classes = (MultiPartParser, FormParser,)
    queryset = file
