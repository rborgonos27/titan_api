from rest_framework.views import APIView
from rest_framework import status
import base64

from rest_framework.response import Response
from django.template.loader import get_template
from django.core.mail import EmailMessage
from django.conf import settings


class UserMemberNotification (APIView):
    def post(self, request):
        fail_response = {
            'version': 'v1',
            'success': False,
            'data': None,
            'message': 'Incomplete field(s).'
        }

        if 'email' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'first_name' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        subject = 'Welcome! New Member of Titan Vault'
        to = [request.POST['email']]
        from_email = 'Titan Vault <no-reply@zeuss.tech>'
        reply_to = [settings.TITAN_REPLY_TO_EMAIL]

        encrypted_username = base64.b64encode(bytes(request.POST['email'], 'utf-8'))
        ctx = {
            'name': request.POST['first_name'],
            'link': "{server}/redirect/login/{username}".format(server=settings.APP_URL, username=encrypted_username)
        }

        message = get_template('new_member_notification.html').render(ctx)
        msg = EmailMessage(subject, message, to=to, from_email=from_email, reply_to=reply_to)
        msg.content_subtype = 'html'

        msg.send()

        response = {
            'version': 'v1',
            'success': True,
            'data': None,
            'message': 'Email credentials sent to user!'
        }

        return Response(response, status.HTTP_200_OK)
