from rest_framework import viewsets
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from ..serializers import RequestDetailSerializer
from ..models import Request

from utils.response import ResponseTemplate


class RequestDetailViewSet(viewsets.ModelViewSet):
    serializer_class = Request
    request_data = Request.objects.all()
    queryset = request_data

    def retrieve(self, request, pk=None):
        queryset = Request.objects.all()
        request_data = get_object_or_404(queryset, pk=pk)
        serializer = RequestDetailSerializer(request_data)
        response = ResponseTemplate.success('v1', serializer.data, 'Request Data')
        return Response(response)
