from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from ..models import File
from ..serializers import UserFileSerializer, FileSerializer
from rest_framework import status
from utils.response import ResponseTemplate


class UserProfilePicture(APIView):
    def get(self, request):
        print (request.GET['resource_id']);
        file = File.objects.filter(resource_id=request.GET['resource_id'])
        serializer = FileSerializer(file)
        response = ResponseTemplate.success('v1', serializer.data, 200)

        return Response(response, status.HTTP_200_OK)
