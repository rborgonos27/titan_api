from rest_framework.views import APIView
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.response import Response
from rest_framework import status
from utils.clear_record import ClearRecord
from django.shortcuts import render

class ClearUserRecord(APIView):

    def post(self, request):
        if 'email' not in request.POST:
            return Response({'success': False, 'message': 'email is required'},
                            status.HTTP_400_BAD_REQUEST)
        try:
            ClearRecord.clear(request.POST['email'])
        except ObjectDoesNotExist:
            return Response({'success': False, 'message': 'Email not found.'},
                            status.HTTP_400_BAD_REQUEST)


        return Response({'success': True, 'message': 'Record Cleared'}, status.HTTP_200_OK)

