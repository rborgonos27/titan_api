from rest_framework.views import APIView
from rest_framework import status

from rest_framework.response import Response
from django.template.loader import get_template
from django.core.mail import EmailMessage
from django.conf import settings


class NewPayeeRequestNotification (APIView):
    def post(self, request):
        fail_response = {
            'version': 'v1',
            'success': False,
            'data': None,
            'message': 'Incomplete field(s).'
        }

        if 'name' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'payee_name' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'payee_id' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        emails = settings.TITAN_STAFF_EMAIL if isinstance(settings.TITAN_STAFF_EMAIL, list) else [settings.TITAN_STAFF_EMAIL]

        subject = 'New Payee Request'
        to = emails
        from_email = 'Titan Vault <no-reply@zeuss.tech>'
        reply_to = [settings.TITAN_REPLY_TO_EMAIL]
        ctx = {
            'name': request.POST['name'],
            'payee_name': request.POST['payee_name'],
            'link': "{server}/payee/{payee_id}".format(server=settings.APP_URL, payee_id=request.POST['payee_id'])
        }

        email_template = 'new_payee_request_notification.html'
        message = get_template(email_template).render(ctx)
        msg = EmailMessage(subject, message, to=to, from_email=from_email, reply_to=reply_to)
        msg.content_subtype = 'html'

        msg.send()

        response = {
            'version': 'v1',
            'success': True,
            'data': None,
            'message': 'Payee request sent to admin!'
        }

        return Response(response, status.HTTP_200_OK)
