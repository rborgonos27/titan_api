from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.settings import api_settings

from ..models import User
from ..serializers import UserSerializer


class UserDetailAPI(generics.ListAPIView):
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS
    serializer_class = UserSerializer
    queryset = User.objects.filter(parent_user_id=None)

    filter_backends = (SearchFilter, OrderingFilter,)
    search_fields = ('user_type', 'last_name', 'business_name', 'email', 'username')
    ordering_fields = ('user_type', 'last_name', 'business_name', 'email', 'username')
