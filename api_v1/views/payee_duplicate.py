from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from utils.response import ResponseTemplate
from ..models import Payee
from ..serializers import PayeeSerializer


class PayeeDuplicate(APIView):
    def get(self, request):
        try:
            print (request.GET)
            if 'bank_account_name' in request.GET:
                payee = Payee.objects\
                    .filter(bank_account_name=request.GET['bank_account_name'], status='APPROVED')
                return Response(self.return_response(payee), status.HTTP_200_OK)
            if 'bank_account' in request.GET:
                payee = Payee.objects\
                    .filter(bank_account=request.GET['bank_account'], status='APPROVED')
                return Response(self.return_response(payee), status.HTTP_200_OK)
            if 'bank_routing_number' in request.GET:
                payee = Payee.objects\
                    .filter(bank_routing_number=request.GET['bank_routing_number'], status='APPROVED')
                return Response(self.return_response(payee), status.HTTP_200_OK)

            return Response({'error': 'Cannot pull record'}, status=status.HTTP_400_BAD_REQUEST)
        except:
            return Response({'error': 'Cannot pull record'}, status=status.HTTP_400_BAD_REQUEST)

    @staticmethod
    def return_response(payee):
        serializer = PayeeSerializer(payee, many=True)
        if len(serializer.data) == 0:
            raise Exception('no data')
        return ResponseTemplate.success('v1', serializer.data[0], 200)
