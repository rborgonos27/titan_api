from rest_framework import viewsets
from rest_framework.pagination import PageNumberPagination
from ..serializers import PayeeSerializer
from ..models import Payee
import datetime
from utils.response import ResponseTemplate
from rest_framework.response import Response
from rest_framework import status


class PayeeViewset(viewsets.ModelViewSet):
    serializer_class = PayeeSerializer
    payee = Payee.objects.all()
    queryset = payee

    def list(self, request):
        paginator = PageNumberPagination()
        paginator.page_size_query_param = 'page_size'
        payee = Payee.objects.all()
        if payee == None:
            raise ValueError('Represents a hidden bug, do not catch this')

        results = paginator.paginate_queryset(payee, request)
        serializer = PayeeSerializer(results, many=True)
        return paginator.get_paginated_response(serializer.data)

    def destroy(self, request, pk):
        payee = Payee.objects.filter(id=pk)[0]
        user = request.user
        payee.updated_by = user.id
        payee.deleted_at = datetime.datetime.now()
        payee.updated_at = datetime.datetime.now()
        payee.save()

        response = ResponseTemplate.success('v1', {'disabled': True}, 'Payee Disabled.')
        return Response(response, status.HTTP_200_OK)
