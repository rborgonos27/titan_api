from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.db.models import Sum
import datetime
from ..models import Request, Wallet
from .. serializers import UserSerializer, RequestSerializer, WalletSerializer


class AccountSummaryApi(APIView):

    def get(self, request):
        try:
            request_data = Request.objects \
                .filter(user=request.user,
                        status='COMPLETED') \
                .order_by('due_at')[:1].get()
            print(request_data)
            wallet = Wallet.objects.filter(user=request.user, type='AVAILABLE_BALANCE')[:1].get()

            print(request_data)
            request_amount_deposit = Request.objects.filter(type='DEPOSIT', status='COMPLETED').aggregate(deposit=Sum('amount') - Sum('fee'))
            print(request_amount_deposit)

            request_amount_payment = Request.objects.filter(type='PAYMENT', status='COMPLETED').aggregate(payment=Sum('amount') + Sum('fee'))
            print(request_amount_payment)

            beginning_balance = float(request_data.amount) - float(request_data.fee)
            return Response({
                'beginning_balance': beginning_balance,
                'available_balance': wallet.balance,
                'total_deposit': request_amount_deposit['deposit'],
                'total_payment': request_amount_payment['payment'],
            }, status=status.HTTP_200_OK)
        except:
            return Response({
                'beginning_balance': 0,
                'available_balance': 0,
                'total_deposit': 0,
                'total_payment': 0,
            }, status=status.HTTP_200_OK)

