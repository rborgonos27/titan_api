from django.contrib.auth.hashers import make_password
from rest_framework.views import APIView
from django.db import IntegrityError
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import ListCreateAPIView

from ..models import User
from ..serializers import UserSerializer
from utils.response import ResponseTemplate


class ResetPasswordAPI(ListCreateAPIView):
    permission_classes = [
        AllowAny
    ]

    def put(self, request):
        # request.POST
        if 'user_id' not in request.POST:
            fail_response = {
                'version': 'v1',
                'success': False,
                'data': None,
                'message': 'User id is required.'
            }
            return Response(fail_response, status=status.HTTP_400_BAD_REQUEST)
        if 'password' not in request.POST:
            fail_response = {
                'version': 'v1',
                'success': False,
                'data': None,
                'message': 'Password is required.'
            }
            return Response(fail_response, status=status.HTTP_400_BAD_REQUEST)

        user = User.objects.get(id=request.POST['user_id'])
        user.password = make_password(request.POST['password'])
        user.save()

        return Response(
            {
                'version': 'v1',
                'success': True,
                'data': None,
                'message': 'Password successfully changed.'
            },
            status=status.HTTP_200_OK)
