from rest_framework.views import APIView
from rest_framework import status
from ..serializers import UserSerializer, UserAccountSerializer
from django.db import IntegrityError
from ..models import User
from rest_framework.response import Response


class UserUpdateAccount(APIView):
    def get(self, request, pk):
        user = User.objects.get(id=pk)
        serializer = UserAccountSerializer(user)

        return Response(serializer.data, status.HTTP_200_OK)

    def put(self, request, pk):
        try:
            request_data = User.objects.get(id=pk)

            request_data.username = request.POST['username']
            request_data.business_contact_number = request.POST['business_contact_number']
            request_data.principal_city_of_business = request.POST['principal_city_of_business']
            request_data.save()

            serializer = UserSerializer(request_data)
            return Response(serializer.data, status.HTTP_200_OK)
        except IntegrityError:
            return Response({'success': False,
                             'message':
                                 'The user name you choose is already in use.'
                                 ' Please select another unique user id login for your security purposes. Thank you'},
                            status=status.HTTP_400_BAD_REQUEST)
