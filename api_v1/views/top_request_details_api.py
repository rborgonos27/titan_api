from rest_framework.response import Response
from django.db.models import Q
from rest_framework import generics
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework import status
from rest_framework.settings import api_settings

from ..models import Request, Transaction, Ledger, Wallet, User
from ..serializers import RequestSerializer, UserSerializer
from utils.response import ResponseTemplate
from utils.audit_log import audit_log
import datetime
import math
import logging


class TopRequestDetailsApi(generics.ListAPIView):
    serializer_class = RequestSerializer

    def get_queryset(self):
        params = self.request.query_params
        user = self.request.user
        user_data = UserSerializer(user)
        user_data = user_data.data
        if user_data['user_type'] == 'sub_member':
            request_data = Request.objects \
                               .filter(user_id=user_data['parent_user_id']) \
                               .filter(~Q(status='CANCELLED')) \
                               .order_by('-updated_at', 'pickup_at', 'due_at')[:5]
        else:
            request_data = Request.objects\
                .filter(user_id=user.id) \
                .filter(~Q(status='CANCELLED'))\
                .order_by('-updated_at', 'pickup_at', 'due_at')[:5]

        if 'limit' in params:
            request_data = request_data[:int(params.get('limit'))]

        return request_data

    filter_backends = (SearchFilter, OrderingFilter,)
    search_fields = ('request_id', 'payee__name', 'user__last_name', 'user__first_name', 'user__email', 'status')
    ordering_fields = ('user__username', 'user__first_name', 'payee__name', 'due_at', 'pickup_at', 'status')
