from rest_framework.views import APIView
from rest_framework import status

from rest_framework.response import Response
from django.template.loader import get_template
from django.core.mail import EmailMessage
from django.conf import settings


class NewRequestNotification (APIView):
    def post(self, request):
        fail_response = {
            'version': 'v1',
            'success': False,
            'data': None,
            'message': 'Incomplete field(s).'
        }

        if 'date' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'name' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'amount' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'type' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'request_id' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        emails = settings.TITAN_STAFF_EMAIL if isinstance(settings.TITAN_STAFF_EMAIL, list) else [settings.TITAN_STAFF_EMAIL]

        request_title = '[New Payment Request]' if request.POST['type'] == 'PAYMENT' else '[New Deposit Request]'
        subject = '{new_request} for {name}, {date}, {amount}'\
            .format(name=request.POST['name'],
                    date=request.POST['date'],
                    amount=request.POST['amount'],
                    new_request=request_title)
        to = emails
        from_email = 'Titan Vault <no-reply@zeuss.tech>'
        reply_to = [settings.TITAN_REPLY_TO_EMAIL]
        ctx = {
            'name': request.POST['name'],
            'link': "{server}/request/{request_id}".format(server=settings.APP_URL, request_id=request.POST['request_id'])
        }

        email_template = 'new_request_notification.html' if request.POST['type'] == 'PAYMENT' else 'new_deposit_notification.html'
        message = get_template(email_template).render(ctx)
        msg = EmailMessage(subject, message, to=to, from_email=from_email, reply_to=reply_to)
        msg.content_subtype = 'html'

        msg.send()

        response = {
            'version': 'v1',
            'success': True,
            'data': None,
            'message': 'Email credentials sent to user!'
        }

        return Response(response, status.HTTP_200_OK)
