from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from .. models import Deposit
from .. serializers import DepositSerializer


class DepositView(APIView):
    """
    A class based view for creating and fetching student records
    """
    def get(self, format=None):
        deposit = Deposit.objects.all()
        serializer = DepositSerializer(deposit, many=True)
        return Response(serializer.data)

    def post(self, request):

        serializer = DepositSerializer(data=request.data)

        if serializer.is_valid(raise_exception=ValueError):
            serializer.create(validated_data=request.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.error_messages,
                        status=status.HTTP_400_BAD_REQUEST)