from rest_framework.response import Response
from rest_framework import generics
from rest_framework import status
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.settings import api_settings

from ..models import Transaction
from ..serializers import TransactionSerializer


class TransactionAPI(generics.ListAPIView):
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS
    serializer_class = TransactionSerializer

    filter_backends = (SearchFilter, OrderingFilter,)

    def get_queryset(self):
        user = self.request.user
        request_id = self.request.query_params.get('request_id')
        if (request_id):
            return Transaction.objects.filter(request_id=request_id).order_by('-created_at')
        else:
            return Transaction.objects.filter(user=user)

    # def get(self, request):
    #     transaction_data = Transaction.objects.all()
    #
    #     serializer = RequestSerializer(transaction_data, many=True)
    #
    #     response = ResponseTemplate.success('v1', serializer.data, 'Transactions List.')
    #     return Response(response, status.HTTP_200_OK)
    #
    # def post(self, request):
    #     response = ResponseTemplate.success('v1', request.data, 'Transactions List.')
    #     return Response(response, status.HTTP_200_OK)
    #
    # def put(self, request, pk):
    #     return Response(request.data, status.HTTP_200_OK)

