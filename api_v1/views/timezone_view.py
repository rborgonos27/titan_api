from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
import datetime


class TimeZoneView(APIView):
    def get(self, request):
        time = datetime.datetime.now() - datetime.timedelta(hours=8)
        return Response({'current_time': time}, status=status.HTTP_200_OK)
