from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from ..models import Wallet, User
from ..serializers import WalletSerializer, UserSerializer
from utils.response import ResponseTemplate


class WalletAPI(APIView):
    def get(self, request):
        user = self.get_user(request.user)
        if user['user_type'] == 'sub_member':
            request_data = Wallet.objects.filter(user=user['parent_user_id'])
        else:
            request_data = Wallet.objects.filter(user=user['id'])

        serializer = WalletSerializer(request_data, many=True)
        response = ResponseTemplate.success('v1', serializer.data, 'User Wallet.')
        return Response(response, status.HTTP_200_OK)

    @staticmethod
    def get_user(user):
        user = UserSerializer(user)
        return user.data
