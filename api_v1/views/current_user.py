from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from utils.audit_log import audit_log
from ..models import User

from ..serializers import UserSerializer


class currentUser(APIView):
    def get(self, request):
        user = UserSerializer(request.user)
        user_data = user.data
        parent_user = None
        if user_data['parent_user_id']:
            sub_user = User.objects.get(id=user_data['parent_user_id'])
            parent_user = UserSerializer(sub_user)

        audit = {
            'module': 'user',
            'transaction': 'get current user',
            'description': 'access current user logged in',
            'user_id': request.user.id,
        }
        audit_log(audit)
        user_data['parent_user'] = parent_user.data if parent_user else None
        return Response(user_data, status=status.HTTP_200_OK)
