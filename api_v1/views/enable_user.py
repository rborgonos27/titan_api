from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response

from ..models import User
from utils.response import ResponseTemplate
import datetime


class EnableUser(APIView):

    def put(self, request):
        if 'user_id' not in request.POST:
            return Response({'success': False, 'message': 'user id not found.'},
                            status.HTTP_400_BAD_REQUEST)

        user = User.objects.get(id=request.POST['user_id'])
        user.deleted_at = None
        user.updated_at = datetime.datetime.now()
        user.save()

        response = ResponseTemplate.success('v1', None, 'User Enabled.')

        return Response(response, status.HTTP_200_OK)
