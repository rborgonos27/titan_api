from rest_framework import viewsets
from rest_framework.pagination import PageNumberPagination
from ..serializers import DepositSerializer
from ..models import Deposit


class DepositViewSet(viewsets.ModelViewSet):
    serializer_class = DepositSerializer
    deposit = Deposit.objects.all()
    queryset = deposit

    def list(self, request):
        user = request.user
        paginator = PageNumberPagination()
        deposit = Deposit.objects.filter(user=user.id)
        if deposit == None:
            raise ValueError('Represents a hidden bug, do not catch this')

        results = paginator.paginate_queryset(deposit, request)
        serializer = DepositSerializer(results, many=True)
        return paginator.get_paginated_response(serializer.data)
