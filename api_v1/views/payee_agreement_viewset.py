from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny

from rest_framework.parsers import MultiPartParser, FormParser
from ..models import PayeeAgreement
from ..serializers import PayeeAgreementSerializer
from rest_framework import status


class PayeeAgreementViewSet(viewsets.ModelViewSet):
    serializer_class = PayeeAgreementSerializer
    payee_agreement = PayeeAgreement.objects.all()

    parser_classes = (MultiPartParser, FormParser,)
    # queryset = payee_agreement

    permission_classes = [
        AllowAny
    ]

    def get_queryset(self):
        request_data = PayeeAgreement.objects
        params = self.request.query_params
        if 'payee_id' in params:
            request_data = request_data.filter(payee_id=params.get('payee_id'))
        else:
            request_data = request_data.all()

        return request_data
