from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response

from ..models import PayeeAgreement
from ..serializers import PayeeAgreementSerializer
from utils.response import ResponseTemplate


class PayeeUserAgreementApi(APIView):
    permission_classes = [
        AllowAny
    ]

    def get(self, request):
        try:
            payee_agreement = PayeeAgreement.objects.get(
                user_id=request.GET['user_id'],
                payee_id=request.GET['payee_id'])

            serializer = PayeeAgreementSerializer(payee_agreement)

            response = ResponseTemplate.success('v1', serializer.data, 'Payee Agreement List.')
            return Response(response, status.HTTP_200_OK)
        except:
            response = ResponseTemplate.failure('v1', 400)
            return Response(response, status.HTTP_400_BAD_REQUEST)


