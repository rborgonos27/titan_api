from rest_framework.views import APIView
from rolepermissions.roles import assign_role
from rest_framework import status

from rest_framework.response import Response
from ..models import User


class AssignRoleApi(APIView):

    ROLE_ADMIN = 'admin'
    ROLE_MAIN_ACCOUNT_USER = 'main_account_user'
    ROLE_MEMBER_USER = 'member_user'

    def post(self, request):

        if 'role' not in request.POST:
            return Response({'success': False, 'message': 'role is required'},
                            status.HTTP_400_BAD_REQUEST)
        if 'user_id' not in request.POST:
            return Response({'success': False, 'message': 'user id is required'},
                            status.HTTP_400_BAD_REQUEST)

        user = User.objects.get(id=request.POST['user_id'])
        role = None
        if request.POST['role'] == self.ROLE_ADMIN:
            role = self.ROLE_ADMIN
        elif request.POST['role'] == self.ROLE_MAIN_ACCOUNT_USER:
            role = self.ROLE_MAIN_ACCOUNT_USER
        elif request.POST['role'] == self.ROLE_MEMBER_USER:
            role = self.ROLE_MEMBER_USER

        assign_role(user, role)

        response = {
            'version': 'v1',
            'success': True,
            'data': None,
            'message': 'Role assigned!'
        }

        return Response(response, status.HTTP_200_OK)
