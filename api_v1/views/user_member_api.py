from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from ..models import User
from ..serializers import UserSerializer
from rest_framework import status
from utils.response import ResponseTemplate


class UserMemberAPI(APIView):
    def get(self, request):
        if 'user_id' not in request.GET:
            return Response({
                'version': 'v1',
                'success': False,
                'data': None,
                'message': 'User Id is required.'
            })

        users = User.objects.filter(parent_user_id=request.GET['user_id'])
        serializer = UserSerializer(users, many=True)
        response = ResponseTemplate.success('v1', serializer.data, 200)

        return Response(response, status.HTTP_200_OK)
