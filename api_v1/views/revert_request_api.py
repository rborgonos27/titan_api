from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.settings import api_settings
from django.template.loader import render_to_string, get_template
from django.conf import settings
from django.core.mail import EmailMessage
from utils.audit_log import audit_log
from django.db.models import Q, Sum
from utils.computation import Computation

from ..models import Request, Transaction, Ledger, Wallet, User, File, FeeTransaction, Fee
from ..serializers import RequestSerializer, FeeSerializer
from utils.response import ResponseTemplate
import datetime
import logging


class RevertRequestAPI(generics.ListAPIView):
    def put(self, request, pk):
        if 'status' not in request.POST:
            fail_response = {
                'version': 'v1',
                'success': False,
                'data': None,
                'message': 'Status is required.'
            }
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'due_at' not in request.POST:
            fail_response = {
                'version': 'v1',
                'success': False,
                'data': None,
                'message': 'Due at is required.'
            }
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'pickup_at' not in request.POST:
            fail_response = {
                'version': 'v1',
                'success': False,
                'data': None,
                'message': 'Pickup at is required.'
            }
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'remarks' not in request.POST:
            fail_response = {
                'version': 'v1',
                'success': False,
                'data': None,
                'message': 'Remarks at is required.'
            }
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        user = request.user
        # SCHEDULED
        # PROCESSING
        # REQUESTED
        request_status = request.POST['status']
        request_data = Request.objects.get(id=pk)
        previous_status = request_data.status
        transaction_type = request_data.type

        if request_status == 'SCHEDULED':
            self.create_transaction('SCHEDULED', request_status, request_data, request)
            if previous_status == 'COMPLETED':
                # update wallet
                if transaction_type == 'PAYMENT':
                    wallet_available = Wallet.objects.filter(user_id=request_data.user_id, type='AVAILABLE_BALANCE')[0]
                    wallet_available.balance += float(request_data.amount) + float(request_data.fee)
                    wallet_available.updated_by = user.id
                    wallet_available.updated_at = datetime.datetime.now()
                    wallet_available.save()
                elif transaction_type == 'DEPOSIT':
                    wallet_available = Wallet.objects.filter(user_id=request_data.user_id, type='AVAILABLE_BALANCE')[0]
                    wallet_available.balance -= float(request_data.amount) - float(request_data.fee)
                    wallet_available.updated_by = user.id
                    wallet_available.updated_at = datetime.datetime.now()
                    wallet_available.save()
        if request_status == 'REQUESTED':
            self.create_transaction('REQUESTED', request_status, request_data, request)
            if previous_status == 'CANCELLED':
                # update wallet
                if transaction_type == 'PAYMENT':
                    wallet_payment = Wallet.objects.filter(user_id=request_data.user_id, type='PENDING_PAYMENT')[0]
                    wallet_payment.balance += float(request_data.amount) + float(request_data.fee)
                    wallet_payment.updated_by = user.id
                    wallet_payment.updated_at = datetime.datetime.now()
                    wallet_payment.save()
                elif transaction_type == 'DEPOSIT':
                    wallet_deposit = Wallet.objects.filter(user_id=request_data.user_id, type='PENDING_DEPOSIT')[0]
                    wallet_deposit.balance += float(request_data.amount) - float(request_data.fee)
                    wallet_deposit.updated_by = user.id
                    wallet_deposit.updated_at = datetime.datetime.now()
                    wallet_deposit.save()

        if request_status == 'PROCESSING':
            self.create_transaction('PROCESSING', request_status, request_data, request)

        request_data.due_at = request.POST['due_at']
        request_data.pickup_at = request.POST['pickup_at']
        request_data.updated_at = datetime.datetime.now()
        request_data.updated_by = user.id
        request_data.status = request.POST['status']
        request_data.save()

        serializer = RequestSerializer(request_data)
        audit = {
            'module': 'request',
            'transaction': 'processing member {transaction}'.format(transaction=serializer.data['type']),
            'description':
                'processing request_id {request_id} for user {user} from {previous_status} to {status}'
                    .format(previous_status=previous_status,
                            status=request_status,
                            request_id=request_data.request_id,
                            user=serializer.data['user']['first_name'],
                            ),
            'user_id': request.user.id,
        }
        audit_log(audit)
        response = ResponseTemplate.success('v1', serializer.data, 'Request Updated.')
        self.update_wallet_pending(request_data.user_id)

        return Response(response, status.HTTP_200_OK)

    @staticmethod
    def update_wallet_pending(user_id):
        print (user_id)
        pending_deposit = Request.objects.filter(user_id=user_id,
                                                type='DEPOSIT') \
            .filter(~Q(status='COMPLETED'))\
            .filter(~Q(status='CANCELLED'))\
            .aggregate(pending_deposit=Sum('amount') - Sum('fee'))

        pending_payment = Request.objects.filter(user_id=user_id,
                                                 type='PAYMENT') \
            .filter(~Q(status='COMPLETED')) \
            .filter(~Q(status='CANCELLED')) \
            .aggregate(pending_payment=Sum('amount') + Sum('fee'))

        print (pending_payment)
        print (pending_deposit)
        wallet = Wallet.objects.get(user_id=user_id, type='PENDING_DEPOSIT')
        wallet.balance = pending_deposit['pending_deposit'] if pending_deposit['pending_deposit'] != None else 0
        wallet.updated_by = user_id
        wallet.updated_at = datetime.datetime.now()
        wallet.save()

        wallet = Wallet.objects.get(user_id=user_id, type='PENDING_PAYMENT')
        wallet.balance = pending_payment['pending_payment'] if pending_payment['pending_payment'] != None else 0
        wallet.updated_by = user_id
        wallet.updated_at = datetime.datetime.now()
        wallet.save()

    @staticmethod
    def create_transaction(type, status, request_data, request):
        user = request.user
        transaction = Transaction(status=status,
                                  type=type,
                                  created_by=user.id,
                                  updated_by=user.id,
                                  request_id=request_data.id,
                                  user_id=request_data.user_id,
                                  to='Anywhere', # dummy date from now
                                  process_at=datetime.datetime.now(),
                                  created_at=datetime.datetime.now(),
                                  updated_at=datetime.datetime.now(),
                                  amount=request_data.amount,
                                  remarks=request.data['remarks'])
        transaction.save()

        return transaction

        # request_data = Request.objects.get(id=pk)
        # serializer = RequestSerializer(request_data)
        # response = ResponseTemplate.success('v1', serializer.data, 'Request Updated.')
        # return Response(response, status.HTTP_200_OK)

        # turn request to processing
        # user = request.user
        # request_data = Request.objects.get(id=pk)
        # request_status = request.POST['status']
        # previous_status = request_data.status
        # if request_status == 'SCHEDULED':
        #     # create scheduled transaction
        #     request_data.pickup_at = request.POST['pickup_at']
        #     request_data.due_at = request.POST['due_at']
        #     self.create_transaction('PICKUP', request_status, request_data, request)
        #     self.create_transaction('TRANSFER', request_status, request_data, request)
        # elif request_status == 'COMPLETED':
        #     request_data.pickup_at = request.POST['pickup_at']
        #     request_data.due_at = request.POST['due_at']
        #     # update wallet
        #     data_for_wallet = {
        #         'type': request_data.type,
        #         'status': 'COMPLETED',
        #         'user': {'id': request_data.user_id},
        #         'amount': request_data.amount,
        #         'fee': request_data.fee,
        #     }
        #     self.update_wallet(data_for_wallet, request)
        #     self.create_transaction('PICKUP', request_status, request_data, request)
        #     self.create_transaction('TRANSFER', request_status, request_data, request)
        #     self.update_ledger(request_data.request_id, request)
        #     # update fee of other transactions
        #     self.update_fee_transaction(self, request_data)
        #     self.update_fee_pending_transactions(self, request_data)
        # elif request_status == 'PROCESSING':
        #     self.create_transaction('PROCESSING', request_status, request_data, request)
        # elif request_status == 'CANCELLED':
        #     self.create_transaction('CANCELLED', request_status, request_data, request)
        #
        # request_data.due_at = request.POST['due_at']
        # request_data.pickup_at = request.POST['pickup_at']
        # request_data.updated_at = datetime.datetime.now()
        # request_data.updated_by = user.id
        # request_data.status = request.POST['status']
        # request_data.save()
        #
        # # send data
        # # self.send_email_notif(pk)
        #
        # serializer = RequestSerializer(request_data)
        # audit = {
        #     'module': 'request',
        #     'transaction': 'processing member {transaction}'.format(transaction=serializer.data['type']),
        #     'description':
        #         'processing request_id {request_id} for user {user} from {previous_status} to {status}'
        #             .format(previous_status=previous_status,
        #                     status=request_status,
        #                     request_id=request_data.request_id,
        #                     user=serializer.data['user']['first_name'],
        #                     ),
        #     'user_id': request.user.id,
        # }
        # audit_log(audit)
        # response = ResponseTemplate.success('v1', serializer.data, 'Request Updated.')
        #
        # return Response(response, status.HTTP_200_OK)
        #
        # return Response({ 'test': 'test' });
