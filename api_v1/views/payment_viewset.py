from rest_framework import viewsets
from rest_framework.pagination import PageNumberPagination
from ..serializers import PaymentSerializer
from ..models import Payment


class PaymentViewSet(viewsets.ModelViewSet):
    serializer_class = PaymentSerializer
    payment = Payment.objects.all()
    queryset = payment

    def list(self, request):
        user = request.user
        paginator = PageNumberPagination()
        payment = Payment.objects.filter(user=user.id)
        if payment == None:
            raise ValueError('Represents a hidden bug, do not catch this')

        results = paginator.paginate_queryset(payment, request)
        serializer = PaymentSerializer(results, many=True)
        return paginator.get_paginated_response(serializer.data)
