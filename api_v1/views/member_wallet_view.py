from rest_framework import generics
from rest_framework.settings import api_settings
from rest_framework.filters import SearchFilter, OrderingFilter
from ..serializers import MemberWalletSerializer
from ..models import User


class MemberWalletView(generics.ListAPIView):
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS
    serializer_class = MemberWalletSerializer
    member = User.objects.filter(parent_user_id=None, user_type='member')
    queryset = member

    filter_backends = (SearchFilter, OrderingFilter,)
    search_fields = ('first_name', 'last_name', 'business_name', 'email', 'date_joined')
    ordering_fields = ('first_name', 'last_name', 'business_name', 'email', 'date_joined')

