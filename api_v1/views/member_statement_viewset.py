from rest_framework import viewsets

from rest_framework.parsers import MultiPartParser, FormParser
from ..models import MemberStatement, User
from ..serializers import MemberStatementSerializer, UserSerializer
import datetime

class MemberStatementViewSet(viewsets.ModelViewSet):
    serializer_class = MemberStatementSerializer
    # member_statement_agreement = MemberStatement.objects.all()

    parser_classes = (MultiPartParser, FormParser,)
    # queryset = member_statement_agreement

    def get_queryset(self):
        user = self.request.user
        user_data = UserSerializer(user)
        user_data = user_data.data
        if user_data['parent_user_id']:
            sub_user = User.objects.get(id=user_data['parent_user_id'])
            parent_user = UserSerializer(sub_user)
            parent_user = parent_user.data
            member_statement_agreement = MemberStatement.objects.filter(user_id=parent_user['id'])
        else:
            member_statement_agreement = MemberStatement.objects.filter(user_id=user.id)

        if 'cut_off' in self.request.query_params:
            cutoff = datetime.datetime.strptime(self.request.query_params.get('cut_off'), "%Y-%m-%d %H:%M:%S")
            # datetime.datetime(self.request.query_params.get('cut_off'))
            member_statement_agreement.filter(cutoff_at__contains=cutoff)



        return member_statement_agreement
