from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.db.models import Sum
from calendar import monthrange

from ..models import FeeTransaction, User
from ..serializers import FeeTransactionSerializer, UserAccountSerializer
import datetime


class FeeTransactionApi(APIView):
    @staticmethod
    def diff_month(first_date, second_date):
        return (first_date.year - second_date.year) * 12 + first_date.month - second_date.month

    def get(self, request):
        params = self.request.query_params
        transaction_data = None
        if 'user_id' not in params:
            return Response({'message': 'user not found'}, status.HTTP_400_BAD_REQUEST)

        # check if user exists
        # get the date joined
        # check the monthly transactions
        # average the monthly volume
        # if date joined is last month only average the total completed volume to days
        # else average the total completed volume in number of months

        # print(request.GET['user_id'])
        # user = User.objects.get(id=request.GET['user_id'])
        # print(user)
        # user_data = UserAccountSerializer(user)
        # date_joined = user_data['date_joined'].value
        # - first_date = datetime.datetime.strptime(date_joined.split(".")[0], "%Y-%m-%d %H:%M:%S")
        # first_date = datetime.datetime.strptime('2018-01-01', "%Y-%m-%d")
        # second_date = datetime.datetime.strptime(request.GET['date'], "%Y-%m-%d")
        # print(first_date)
        # print(second_date)
        # diff = self.diff_month(second_date, first_date)
        # print(diff)
        # print(date_joined)
        if 'type' in params:
            if 'date' in params:
                month = datetime.datetime.strptime(request.GET['date'], "%Y-%m-%d").month
                transaction_data = FeeTransaction.objects.filter(
                    user_id=request.GET['user_id'],
                    type=request.GET['type'],
                    created_at__month=month,
                ).order_by('-created_at')
                # month = datetime.datetime.strptime(request.GET['date'], "%Y-%m-%d").month
                # print(month)
                # year = datetime.datetime.strptime(request.GET['date'], "%Y-%m-%d").year
                # transaction_data = FeeTransaction.objects.filter(
                #     user_id=request.GET['user_id'],
                #     type=request.GET['type'],
                #     process_at__month=month - 1
                # ).aggregate(Sum('total_transaction_amount'))
                # print(transaction_data)
                # no_of_days = monthrange(year, month -1)
                # print(no_of_days[1])
                # average = float(transaction_data['total_transaction_amount__sum']) / float(no_of_days[1])
                # print(average)
                # return Response({'id': 1, 'total_transaction_amount': average}, status.HTTP_200_OK)
            else:
                transaction_data = FeeTransaction.objects.filter(
                    user_id=request.GET['user_id'],
                    type=request.GET['type']
                ).order_by('-created_at')
        else:
            transaction_data = FeeTransaction.objects.filter(user_id=request.GET['user_id']).order_by('-created_at')
        if not transaction_data:
            return Response({'message': 'No transaction fee'}, status.HTTP_200_OK)

        serializer = FeeTransactionSerializer(transaction_data, many=True)

        return Response(serializer.data[0], status.HTTP_200_OK)

