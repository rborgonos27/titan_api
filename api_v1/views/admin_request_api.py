from rest_framework.response import Response
from django.db.models import Q
from rest_framework import generics
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework import status
from rest_framework.settings import api_settings
from ..permission import IsAdminOrStaff

from ..models import Request, Transaction, Ledger, Wallet, User
from ..serializers import RequestDetailSerializer
from utils.response import ResponseTemplate
from utils.audit_log import audit_log
import datetime
import math
import logging


class AdminRequestAPI(generics.ListAPIView):
    permission_classes = (IsAdminOrStaff,)
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS
    serializer_class = RequestDetailSerializer
    # queryset = Request.objects.filter(~Q(status='COMPLETED')).filter(~Q(status='CANCELLED'))\
    #     .order_by('pickup_at', 'due_at')
    # queryset = Request.objects.order_by('pickup_at', 'due_at')

    def get_queryset(self):
        request_data = Request.objects.order_by('pickup_at', 'due_at')
        params = self.request.query_params
        if 'user_id' in params:
            request_data = request_data.filter(user_id=params.get('user_id'))

        if 'type' in params:
            request_data = request_data.filter(type=params.get('type'))

        if 'status' in params:
            request_data = request_data.filter(status=params.get('status'))

        if 'delivery_method' in params:
            request_data = request_data.filter(type=params.get('status'))
        # custom
        # today
        # next_3_days
        # this_week
        # this_month
        # this_quarter
        # last_30_days
        # last_90_days
        # last_month
        # last_quarter
        # last_year
        # all_dates

        if 'date' in params:
            # custom
            if params.get('date') == 'payment_date':
                request_data = self.get_payment_date_filter(params, request_data)
            elif params.get('date') == 'pickup_date':
                request_data = self.get_pickup_date_filter(params, request_data)
            else:
                request_data = self.get_created_date_filter(params, request_data)
        else:
            request_data = self.get_created_date_filter(params, request_data)

        if 'payee_id' in params:
            request_data = request_data.filter(payee_id=params.get('payee_id'))

        if 'category' in params:
            request_data = request_data.filter(type=params.get('category'))

        return request_data.order_by('-created_at')

    filter_backends = (SearchFilter, OrderingFilter,)
    search_fields = ('request_id', 'payee__name', 'user__last_name', 'user__first_name', 'user__username', 'user__business_name', 'user__email', 'status')
    ordering_fields = ('type', 'user__username', 'user__business_name', 'user__first_name', 'payee__name', 'due_at', 'pickup_at', 'status')

    def put(self, request, pk):
        logger = logging.getLogger(__name__)
        # turn request to processing
        user = request.user
        request_data = Request.objects.get(id=pk)
        request_status = request.POST['status']
        previous_status = request_data.status
        logger.info(request_status)
        if request_status == 'SCHEDULED':
            # create scheduled transaction
            request_data.pickup_at = request.POST['pickup_at']
            request_data.due_at = request.POST['due_at']
            self.create_transaction('PICKUP', request_status, request_data, request)
            self.create_transaction('TRANSFER', request_status, request_data, request)
        elif request_status == 'COMPLETED':
            request_data.pickup_at = request.POST['pickup_at']
            request_data.due_at = request.POST['due_at']
            # update wallet
            data_for_wallet = {
                'type': request_data.type,
                'status': 'COMPLETED',
                'user': {'id': request_data.user_id},
                'amount': request_data.amount
            }
            print(data_for_wallet)
            print('=============')
            logger.info('update wallet from put')
            self.update_wallet(data_for_wallet, request)
            self.create_transaction('PICKUP', request_status, request_data, request)
            self.create_transaction('TRANSFER', request_status, request_data, request)

        request_data.updated_at = datetime.datetime.now()
        request_data.updated_by = user.id
        request_data.status = request.POST['status']
        request_data.save()

        serializer = RequestSerializer(request_data)
        response = ResponseTemplate.success('v1', serializer.data, 'Request Updated.')
        audit = {
            'module': 'request',
            'transaction': 'processing member payment',
            'description': 'processing from {previous_status} to {status}'.format(previous_status=previous_status,
                                                                                  status=request_status),
            'user_id': request.user.id,
        }
        audit_log(audit)

        return Response(response, status.HTTP_200_OK)

    @staticmethod
    def get_created_date_filter(params, request_data):
        if params.get('period') == 'custom':
            request_data = request_data \
                .filter(created_at__range=[params.get('from'), params.get('to')])
        elif params.get('period') == 'today':
            today = datetime.date.today() - datetime.timedelta(hours=8)
            print(today)
            request_data = request_data.filter(created_at__contains=today)
        elif params.get('period') == 'next_3_days':
            today = datetime.date.today()
            next_three_days = today + datetime.timedelta(days=3)
            request_data = request_data.filter(created_at__range=[today, next_three_days])
        elif params.get('period') == 'this_week':
            today = datetime.date.today()
            week = self.week_range(today)
            request_data = request_data.filter(created_at__range=week)
        elif params.get('period') == 'this_month':
            current_month = datetime.datetime.now().month
            request_data = request_data.filter(created_at__month=current_month)
        elif params.get('period') == 'this_quarter':
            quarter = self.quarter_range()
            request_data = request_data.filter(created_at__range=quarter)
        elif params.get('period') == 'last_30_days':
            today = datetime.date.today()
            last_30_days = datetime.date.today() - datetime.timedelta(days=30)
            request_data = request_data.filter(created_at__range=[last_30_days, today])
        elif params.get('period') == 'last_90_days':
            today = datetime.date.today()
            last_30_days = datetime.date.today() - datetime.timedelta(days=90)
            request_data = request_data.filter(created_at__range=[last_30_days, today])
        elif params.get('period') == 'last_month':
            current_month = datetime.datetime.now().month
            print (current_month)
            request_data = request_data.filter(created_at__month=current_month - 1)
        elif params.get('period') == 'last_quarter':
            quarter = self.last_quarter_range()
            request_data = request_data.filter(created_at__range=quarter)
        elif params.get('period') == 'last_year':
            year = datetime.date.now().year - 1
            request_data = request_data.filter(created_at__year=year)

        return request_data

    @staticmethod
    def get_pickup_date_filter(params, request_data):
        if params.get('period') == 'custom':
            request_data = request_data \
                .filter(pickup_at__range=[params.get('from'), params.get('to')])
        elif params.get('period') == 'today':
            today = datetime.date.today()
            request_data = request_data.filter(pickup_at__contains=today)
        elif params.get('period') == 'next_3_days':
            today = datetime.date.today()
            next_three_days = today + datetime.timedelta(days=3)
            request_data = request_data.filter(pickup_at__range=[today, next_three_days])
        elif params.get('period') == 'this_week':
            today = datetime.date.today()
            week = self.week_range(today)
            request_data = request_data.filter(pickup_at__range=week)
        elif params.get('period') == 'this_month':
            current_month = datetime.datetime.now().month
            request_data = request_data.filter(pickup_at__month=current_month)
        elif params.get('period') == 'this_quarter':
            quarter = self.quarter_range()
            request_data = request_data.filter(pickup_at__range=quarter)
        elif params.get('period') == 'last_30_days':
            today = datetime.date.today()
            last_30_days = datetime.date.today() - datetime.timedelta(days=30)
            request_data = request_data.filter(pickup_at__range=[last_30_days, today])
        elif params.get('period') == 'last_90_days':
            today = datetime.date.today()
            last_30_days = datetime.date.today() - datetime.timedelta(days=90)
            request_data = request_data.filter(pickup_at__range=[last_30_days, today])
        elif params.get('period') == 'last_month':
            current_month = datetime.datetime.now().month
            print (current_month)
            request_data = request_data.filter(pickup_at__month=current_month - 1)
        elif params.get('period') == 'last_quarter':
            quarter = self.last_quarter_range()
            request_data = request_data.filter(pickup_at__range=quarter)
        elif params.get('period') == 'last_year':
            year = datetime.date.now().year - 1
            request_data = request_data.filter(pickup_at__year=year)

        return request_data

    @staticmethod
    def get_payment_date_filter(params, request_data):
        if params.get('period') == 'custom':
            request_data = request_data \
                .filter(due_at__range=[params.get('from'), params.get('to')])
        elif params.get('period') == 'today':
            today = datetime.date.today() - datetime.timedelta(hours=8)
            print(today)
            request_data = request_data.filter(due_at__contains=today)
        elif params.get('period') == 'next_3_days':
            today = datetime.date.today()
            next_three_days = today + datetime.timedelta(days=3)
            request_data = request_data.filter(due_at__range=[today, next_three_days])
        elif params.get('period') == 'this_week':
            today = datetime.date.today()
            week = self.week_range(today)
            request_data = request_data.filter(due_at__range=week)
        elif params.get('period') == 'this_month':
            current_month = datetime.datetime.now().month
            request_data = request_data.filter(due_at__month=current_month)
        elif params.get('period') == 'this_quarter':
            quarter = self.quarter_range()
            request_data = request_data.filter(due_at__range=quarter)
        elif params.get('period') == 'last_30_days':
            today = datetime.date.today()
            last_30_days = datetime.date.today() - datetime.timedelta(days=30)
            request_data = request_data.filter(due_at__range=[last_30_days, today])
        elif params.get('period') == 'last_90_days':
            today = datetime.date.today()
            last_30_days = datetime.date.today() - datetime.timedelta(days=90)
            request_data = request_data.filter(due_at__range=[last_30_days, today])
        elif params.get('period') == 'last_month':
            current_month = datetime.datetime.now().month
            print (current_month)
            request_data = request_data.filter(due_at__month=current_month - 1)
        elif params.get('period') == 'last_quarter':
            quarter = self.last_quarter_range()
            request_data = request_data.filter(due_at__range=quarter)
        elif params.get('period') == 'last_year':
            year = datetime.date.now().year - 1
            request_data = request_data.filter(due_at__year=year)

        return request_data

    @staticmethod
    def week_range(date):
        year, week, dow = date.isocalendar()
        if dow == 7:
            start_date = date
        else:
            start_date = date - datetime.timedelta(dow)

        end_date = start_date + datetime.timedelta(6)

        return start_date, end_date

    @staticmethod
    def quarter_range():
        current_date = datetime.datetime.now()
        current_quarter = round(current_date.month / 3)
        start_date = datetime.datetime(current_date.year, 3 * current_quarter - 2, 1)
        end_date = current_date
        return start_date, end_date

    @staticmethod
    def last_quarter_range():
        current_date = datetime.datetime.now() - datetime.timedelta(days=30)
        current_quarter = round(current_date.month / 3) - 1
        start_date = datetime.datetime(current_date.year, 3 * current_quarter - 2, 1)
        end_date = datetime.datetime(current_date.year, start_date.month + 3, 1)
        return start_date, end_date

    @staticmethod
    def create_transaction(type, status, request_data, request):
        user = request.user
        transaction = Transaction(status=status,
                                  type=type,
                                  created_by=user.id,
                                  updated_by=user.id,
                                  request_id=request_data.id,
                                  user_id=request_data.user_id,
                                  to=request.POST['to'],
                                  process_at=datetime.datetime.now(),
                                  created_at=datetime.datetime.now(),
                                  updated_at=datetime.datetime.now(),
                                  amount=request_data.amount,
                                  remarks='Request Scheduled.')
        transaction.save()

        return transaction

    @staticmethod
    def update_wallet_by_type(wallet_type, owner_id, amount, request):
        logger = logging.getLogger(__name__)
        user = request.user

        wallet = Wallet.objects.get(user_id=owner_id, type=wallet_type)
        logger.info('wallet user')
        logger.info(wallet)
        wallet.balance = amount
        wallet.updated_by = user.id
        wallet.updated_at = datetime.datetime.now()
        wallet.save()

        return wallet

    def update_wallet(self, data, request):
        logger = logging.getLogger(__name__)
        logger.info('update wallet')
        user = request.user
        logger.info(data)
        transaction_type = data['type']
        transaction_status = data['status']
        user_id = data['user']['id']

        wallet = None
        print(transaction_status)
        if transaction_status.strip() == 'COMPLETED':
            # completed
            actual_wallet = wallet = Wallet.objects.filter(user_id=user_id, type='AVAILABLE_BALANCE')[0]
            actual_amount = 0
            if transaction_type == 'PAYMENT':
                wallet = Wallet.objects.filter(user_id=user_id, type='PENDING_PAYMENT')[0]
                print(wallet.balance)
                amount = wallet.balance - float(data['amount'])
                actual_amount = actual_wallet.balance - float(data['amount'])
                self.update_wallet_by_type('PENDING_PAYMENT', user_id, amount, request)
            elif transaction_type == 'DEPOSIT':
                logger.info('Deposit should {type} {amount}'.format(amount=actual_amount, type=transaction_type))
                wallet = Wallet.objects.filter(user_id=user_id, type='PENDING_DEPOSIT')[0]
                amount = wallet.balance - float(data['amount'])
                actual_amount = actual_wallet.balance + float(data['amount'])
                self.update_wallet_by_type('PENDING_DEPOSIT', user_id, amount, request)

            logger.info('test actual amount {amount}'.format(amount=actual_amount))
            self.update_wallet_by_type('AVAILABLE_BALANCE', user_id, actual_amount, request)
            self.update_ledger(request, request, transaction_type, amount, actual_amount)
        else:
            # pending
            if transaction_type == 'PAYMENT':
                wallet_payment = Wallet.objects.filter(user_id=user_id, type='PENDING_PAYMENT')[0]
                wallet_payment.balance += float(data['amount'])
                wallet_payment.updated_by = user.id
                wallet_payment.updated_at = datetime.datetime.now()
                wallet_payment.save()
            elif transaction_type == 'DEPOSIT':
                wallet_deposit = Wallet.objects.filter(user_id=user_id, type='PENDING_DEPOSIT')[0]
                wallet_deposit.balance += float(data['amount'])
                wallet_deposit.updated_by = user.id
                wallet_deposit.updated_at = datetime.datetime.now()
                wallet_deposit.save()

        return wallet

    @staticmethod
    def create_ledger(data, request):
        user = request.user
        request_data = Request.objects.get(id=data['id'])
        print(request_data.request_id)

        resulting_available_balance_value = 0
        resulting_pending_payment_balance_value = 0
        resulting_pending_deposit_balance_value = 0

        if request_data.type == 'DEPOSIT':
            resulting_pending_deposit_balance_value = float(request_data.amount) - float(request_data.fee)

        if request_data.type == 'PAYMENT':
            resulting_pending_payment_balance_value = float(request_data.amount) + float(request_data.fee)

        ledger = Ledger(
            transaction_id=request_data.request_id,
            transaction_status='pending',
            amount=request_data.amount,
            resulting_available_balance=resulting_available_balance_value,
            resulting_pending_payment_balance=resulting_pending_payment_balance_value,
            resulting_pending_deposit_balance=resulting_pending_deposit_balance_value,
            notes='created transaction',
            user_id=request_data.user_id,
            created_by=user.id,
            updated_by=user.id,
            created_at=datetime.datetime.now(),
            updated_at=datetime.datetime.now(),
        )

        ledger.save()

        return ledger

    @staticmethod
    def update_ledger(request_id, request, transaction_type, amount, actual_amount):
        user = request.user
        request_data = Ledger.objects.filter(transaction_id=request_id)[0]

        resulting_pending_payment_balance = 0
        resulting_pending_deposit_balance = 0

        if transaction_type == 'PAYMENT':
            resulting_pending_payment_balance = amount

        if transaction_type == 'DEPOSIT':
            resulting_pending_deposit_balance = amount

        request_data.amount = request_data.amount
        request_data.resulting_available_balance = actual_amount
        request_data.resulting_pending_payment_balance = resulting_pending_payment_balance
        request_data.resulting_pending_deposit_balance = resulting_pending_deposit_balance
        request_data.transaction_status = 'completed'
        request_data.updated_by = user.id
        request_data.updated_at = datetime.datetime.now()

        request_data.save()

        return request_data
