from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response

from ..models import Payee
from utils.response import ResponseTemplate
import datetime


class EnablePayee(APIView):

    def put(self, request):
        if 'payee_id' not in request.POST:
            return Response({'success': False, 'message': 'payee id not found.'},
                            status.HTTP_400_BAD_REQUEST)

        payee = Payee.objects.get(id=request.POST['payee_id'])
        payee.deleted_at = None
        payee.updated_at = datetime.datetime.now()
        payee.save()

        response = ResponseTemplate.success('v1', None, 'Payee Enabled.')

        return Response(response, status.HTTP_200_OK)
