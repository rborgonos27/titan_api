from rest_framework.views import APIView
from rest_framework import status
import base64

from rest_framework.response import Response
from django.template.loader import get_template
from django.core.mail import EmailMessage
from django.conf import settings
import requests
import os
import logging


class SendMessageMember (APIView):
    def post(self, request):
        logger = logging.getLogger(__name__)
        fail_response = {
            'version': 'v1',
            'success': False,
            'data': None,
            'message': 'Incomplete field(s).'
        }
        if 'email' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'user_id' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'member_name' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'message' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        subject = 'You have a message from {name}'.format(name=request.POST['member_name'])
        to = [settings.TITAN_ALERT_EMAIL]
        from_email = 'Titan Vault <no-reply@zeuss.tech>'
        reply_to = [request.POST['email']]

        ctx = {
            'member_name': request.POST['member_name'],
            'message': request.POST['message'],
        }

        message = get_template('send_message_notification.html').render(ctx)
        msg = EmailMessage(subject, message, to=to, from_email=from_email, reply_to=reply_to)
        msg.content_subtype = 'html'

        msg.send()

        logger.info("Payee Agreement sent to {email}".format(email=request.POST['email']))

        response = {
            'version': 'v1',
            'success': True,
            'data': None,
            'message': 'Message sent!'
        }

        return Response(response, status.HTTP_200_OK)
