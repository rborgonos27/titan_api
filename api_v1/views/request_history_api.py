from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.settings import api_settings
from django.template.loader import render_to_string, get_template
from django.conf import settings
from django.core.mail import EmailMessage
from utils.audit_log import audit_log
from django.db.models import Q
from utils.computation import Computation

from ..models import Request, Transaction, Ledger, Wallet, User, File, FeeTransaction, Fee
from ..serializers import RequestSerializer, FeeSerializer
from utils.response import ResponseTemplate
import datetime


class RequestHistoryAPI(generics.ListAPIView):
    serializer_class = RequestSerializer
    pagination_class = None

    def get_queryset(self):
        user = self.request.user
        today = datetime.date.today()
        request_data = Request.objects\
            .filter(user=user,
                    status='COMPLETED',
                    due_at__month=today.month)\
            .order_by('due_at')

        return request_data

