from rest_framework.views import APIView
from rest_framework import status
import os
from .constant import Constant

from ..serializers.request_detail_serializer import RequestDetailSerializer
from django.db.models import Sum
from django.core.files import File as FileObject
from utils.build_report import BuildReport

from reportlab.pdfgen import canvas
from PyPDF2 import PdfFileWriter, PdfFileReader


from rest_framework.response import Response
from django.template.loader import get_template
from django.core.mail import EmailMessage
from django.conf import settings

from datetime import timedelta
from .. models import File, User, MemberStatement, Request, Wallet, Ledger
from collections import OrderedDict
from django.core.files import File as FileObject


import requests
import socket
import datetime


class GenerateStatementAPI (APIView):

    def post(self, request):
        fail_response = {
            'version': 'v1',
            'success': False,
            'data': None,
            'message': 'Incomplete field(s).'
        }

        if 'cutoff_at' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        if 'user_id' not in request.POST:
            return Response(fail_response, status.HTTP_400_BAD_REQUEST)

        user_id = request.POST['user_id']
        member = User.objects.get(id=user_id)
        if member.parent_user_id:
            user_id = member.parent_user_id
            member = User.objects.get(id=user_id)

        member_start = member.date_joined
        cut_offs = self.getDateRange(member_start, datetime.datetime.now())

        count = len(cut_offs)
        last_two_cut_offs = [cut_offs[count - 2], cut_offs[count - 1]]
        print (cut_offs)
        print (last_two_cut_offs)

        for cut_off in last_two_cut_offs:
            MemberStatement.objects.filter(cutoff_at=cut_off).delete()
            cut_off_format = cut_off.strftime("%Y-%m-%d")
            member_start_format = member_start.strftime("%Y-%m-%d")
            if cut_off_format != member_start_format:
                member_statement = MemberStatement(
                    cutoff_at=cut_off,
                    user_id=user_id,
                    created_by=user_id,
                    updated_by=user_id,
                    created_at=datetime.datetime.now(),
                    updated_at=datetime.datetime.now(),
                )

                member_statement.save()
                file_name = self.build_report(cut_off, member_statement)
                self.upload_statement(file_name, member_statement)
                os.remove(file_name)

        response = {
            'version': 'v1',
            'success': True,
            'data': None,
            'message': 'Created!'
        }

        return Response(response, status.HTTP_200_OK)

    @staticmethod
    def getDateRange(start_date, end_date):
        start_day = int(start_date.strftime("%d"))
        start_month = int(start_date.strftime("%m"))
        end_day = int(end_date.strftime("%d"))
        end_month = int(end_date.strftime("%m")) - 1 if end_day <= start_day else int(end_date.strftime("%m"))
        # end day = 4 start day = 17 4 <= 17 -> current month - 1
        # end day = 18 start day = 17 18 <= 17 -> current month
        # end day = 17 start day = 17 17 <= 17 -> current month - 1
        date_today = datetime.datetime.now()
        cut_offs = []
        # start month = 5; end month = 6 // since day of cut off is not reached yet
        for x in range(start_month, end_month):
            cutoff_date = "{year}-{month}-{day} 00:00:00".format(year=date_today.strftime("%Y"),
                                                                 month=x,
                                                                 day=start_date.strftime("%d"))
            cutoff_date = datetime.datetime.strptime(cutoff_date, "%Y-%m-%d %H:%M:%S")
            cut_offs.append(cutoff_date)

        cutoff_date = "{year}-{month}-{day} 00:00:00".format(year=date_today.strftime("%Y"),
                                                             month=end_month,
                                                             day=start_date.strftime("%d"))
        cutoff_date = datetime.datetime.strptime(cutoff_date, "%Y-%m-%d %H:%M:%S")
        cut_offs.append(cutoff_date)

        return cut_offs

    @staticmethod
    def upload_statement(file_name, member_statement):
        output_file = open(file_name, "rb")

        try:
            file = File.object.get(resource_id=member_statement.id)
            file.filename = FileObject(output_file)
            file.save()
        except:
            file = File(
                user_id=member_statement.user_id,
                filename=FileObject(output_file),
                url='filename',
                mime_type='application/pdf',
                expire_at=datetime.datetime.now() + datetime.timedelta(hours=1),
                type='MEMBER_STATEMENT',
                resource_id=member_statement.id,
                created_by=member_statement.user_id,
                updated_by=member_statement.user_id,
                created_at=datetime.datetime.now(),
                updated_at=datetime.datetime.now(),
            )
            file.save()

        return file

    @staticmethod
    def build_report(cut_off, member_statement):
        watermark_file = "storage/statement_file_{user_id}.pdf".format(user_id=member_statement.user_id)
        custom_file = 'storage/images/titan-vault.png'
        titan_logo = 'storage/images/titan-logo.png'
        # request_data = Request.objects.
        member = User.objects.get(id=member_statement.user_id)

        # member_id
        member_id = member.member_account_id
        member_name = "{member_name}" \
            .format(member_name=member.business_name)
        member_address = "{address} {address_2}" \
            .format(address=member.address,
                    address_2='' if not member.address_2 else member.address_2
                    )
        city = '' if not member.city else member.city.title()
        state = '' if not member.state else member.state.title()
        zip_code = '' if not member.zip_code else member.zip_code
        member_address_2 = "{city} {state} {zip_code}" \
            .format(city=city,
                    state=state.capitalize(),
                    zip_code=zip_code
                    )

        date_format = "%b %d"
        date_data_format = "%b - %d"
        member_start = member.date_joined.strftime("%b %Y")
        date_today = datetime.datetime.now()

        # identify cut off
        month_cutoff = cut_off.strftime("%m")

        cut_off_date_string = "{year}-{month}-{day} 00:00:00".format(year=date_today.strftime("%Y"),
                                                                     month=month_cutoff,
                                                                     day=int(member.date_joined.strftime("%d")) -1)

        # support previous year
        month = 12 if int(month_cutoff) - 1 == 0 else int(month_cutoff) - 1
        year = int(date_today.strftime("%Y")) - 1 if int(month_cutoff) - 1 == 0 else date_today.strftime("%Y")
        cut_off_start_date_string = "{year}-{month}-{day} 00:00:00".format(year=year,
                                                                           month=month,
                                                                           day=member.date_joined.strftime("%d"))
        cut_off_date = datetime.datetime.strptime(cut_off_date_string, "%Y-%m-%d %H:%M:%S")
        cut_off_start_date = datetime.datetime.strptime(cut_off_start_date_string, "%Y-%m-%d %H:%M:%S")

        date_from = cut_off_start_date.strftime(date_format)
        date_to = cut_off_date.strftime(date_format)
        member_date = "For {date_from}, to {date_to} {year}" \
            .format(date_from=date_from,
                    date_to=date_to,
                    year=cut_off_date.strftime("%Y"))

        # deposits
        request_amount_deposit = Request.objects.filter(user_id=member_statement.user_id,
                                                        type=Constant.TYPE_DEPOSIT,
                                                        status=Constant.STATUS_COMPLETED,
                                                        updated_at__range=[cut_off_start_date, cut_off_date]) \
            .aggregate(deposit=Sum('amount') - Sum('fee'))

        request_amount_payment = Request.objects.filter(user_id=member_statement.user_id,
                                                        type=Constant.TYPE_PAYMENT,
                                                        status=Constant.STATUS_COMPLETED,
                                                        updated_at__range=[cut_off_start_date, cut_off_date]) \
            .aggregate(payment=Sum('amount') + Sum('fee'))

        if not request_amount_deposit['deposit']:
            request_amount_deposit = {'deposit': 0}

        # payments
        if not request_amount_payment['payment']:
            request_amount_payment = {'payment': 0}

        # print ('cut_off_start_date')
        # print (cut_off_start_date)
        # beginning balance
        try:
            request_data = Request.objects \
                               .filter(user_id=member_statement.user_id,
                                       status=Constant.STATUS_COMPLETED,
                                       updated_at__lt=cut_off_start_date
                                       ) \
                               .order_by(Constant.UPDATED_AT_FIELD)[:1]
        except:
            request_data = None

        if request_data is None:
            beginning_balance = 0
        elif not request_data:
            beginning_balance = 0
        else:
            beginning_balance_serializer = RequestDetailSerializer(request_data, many=True)
            beginning_balance = float(beginning_balance_serializer.data[0]['ledger']['resulting_available_balance'])
            print(beginning_balance_serializer)
        # for next statement, get ending balance of previous statement
        # for first statement = 0
        # actual balance
        wallet = Wallet.objects.filter(user_id=member_statement.user_id, type='AVAILABLE_BALANCE')[:1].get()

        # transactions
        requests = Request.objects.filter(user_id=member_statement.user_id,
                                          status=Constant.STATUS_COMPLETED,
                                          updated_at__range=[cut_off_start_date, cut_off_date]
                                          ).order_by(Constant.UPDATED_AT_FIELD)
        request_data = RequestDetailSerializer(requests, many=True)
        print('==================================================================')
        print('cut_off_start_date')
        print(cut_off_start_date)
        print('cut_off_date')
        print(cut_off_date)

        deposits = BuildReport.format_transaction_number(request_amount_deposit['deposit'])
        payments = BuildReport.format_transaction_number(request_amount_payment['payment'])
        beginning_balance = BuildReport.format_transaction_number(beginning_balance)

        ending_balance_query = Request.objects\
            .filter(user_id=member_statement.user_id,
                    status=Constant.STATUS_COMPLETED,
                    updated_at__range=[cut_off_start_date, cut_off_date]) \
            .aggregate(ending_balance=Sum('amount') - Sum('fee'))

        if not ending_balance_query:
            ending_balance_data = 0
        elif not ending_balance_query['ending_balance']:
            ending_balance_data = 0
        else:
            ending_balance_data = ending_balance_query['ending_balance']

        ending_balance = BuildReport.format_transaction_number(ending_balance_data)
        actual_balance = BuildReport.format_transaction_number(wallet.balance)

        c = canvas.Canvas(watermark_file)
        c.setTitle(member_date)
        font = 'Helvetica'

        x = 30
        y = 735
        next_line_start = 320
        # LOGO
        c.drawImage(custom_file, x, y, width=200, height=22)

        # header
        c.setFont(font, 26)
        line_x = next_line_start
        line_y = 729
        line_end = 580

        c.drawString(line_x, y + 5, 'STATEMENT')
        BuildReport.build_line(c, line_x, line_y, line_end, True)

        y = 695

        # next row
        account_statement = 'Account Summary'
        BuildReport.build_sub_header(c, x, y, account_statement, True)
        line_x = x
        line_y = y - 5
        line_end = 280

        BuildReport.build_line(c, line_x, line_y, line_end, False)

        # 2nd column
        line_x = next_line_start
        line_y = y - 5
        line_end = 580

        BuildReport.build_sub_header(c, line_x, y, member_date, False)
        BuildReport.build_line(c, line_x, line_y, line_end, False)

        BuildReport.build_sub_header_small(c, line_x + 5, y - 25, 'MEMBER NUMBER', True)
        BuildReport.build_ordinary_text(c, line_x + 5, y - 40, member_id, False)

        BuildReport.build_ordinary_text(c, line_x + 5, y - 65, member_name, False)
        BuildReport.build_ordinary_text(c, line_x + 5, y - 80, member_address, False)
        BuildReport.build_ordinary_text(c, line_x + 5, y - 90, member_address_2, False)

        BuildReport.build_sub_header_small(c, line_x + 5, y - 110, 'Contact Information', True)
        BuildReport.build_line(c, line_x, line_y - 115, line_end, False)

        BuildReport.build_ordinary_text(c, line_x + 5, y - 140, 'memberservices@titan-vault.com', False)
        BuildReport.build_ordinary_text(c, line_x + 5, y - 155, 'Urgent Request or Inquiries:', False)
        BuildReport.build_ordinary_text(c, line_x + 5, y - 170, '1-650-680-8328', False)

        # 1st column
        add_tab = 170

        y = y - 20
        BuildReport.build_ordinary_text(c, x + 10, y, 'Beginning Balance', False)
        BuildReport.build_ordinary_text(c, x + add_tab, y, beginning_balance, True)

        y = y - 15
        BuildReport.build_ordinary_text(c, x + 10, y, 'Payments', False)
        BuildReport.build_ordinary_text(c, x + add_tab-6, y, "- {payments}".format(payments=payments), True)

        y = y - 15
        BuildReport.build_ordinary_text(c, x + 10, y, 'Deposits', False)
        BuildReport.build_ordinary_text(c, x + add_tab, y, deposits, True)

        line_x = x
        line_y = y - 15
        line_end = 280
        BuildReport.build_line(c, line_x, line_y, line_end, False)
        y = y - 30
        ending_balance_as_of = "Ending balance as of {date}".format(date=date_today.strftime("%b %Y"))
        BuildReport.build_ordinary_text(c, x, y, ending_balance_as_of, True)
        BuildReport.build_ordinary_text(c, x + add_tab, y, ending_balance, True)
        line_y = y - 10
        BuildReport.build_line(c, line_x, line_y, line_end, False)

        y = y - 30
        member_since_text = "Member Since {member_since}".format(member_since=member_start)
        BuildReport.build_sub_header_small(c, x, y, member_since_text, True)

        y = y - 30
        long_text = "Contact us by email for questions, on this statement, "
        long_text_2 = "change of business information, and general inquiries,"
        long_text_3 = "24 hours a day, 7 days a week."
        BuildReport.build_ordinary_text(c, x, y, long_text, False)
        y = y - 15
        BuildReport.build_ordinary_text(c, x, y, long_text_2, False)
        y = y - 15
        BuildReport.build_ordinary_text(c, x, y, long_text_3, False)

        y = y - 25
        BuildReport.build_sub_header_small(c, x, y, 'Your Transaction Details', True)

        line_y = y - 15
        line_end = 580
        BuildReport.build_line(c, line_x, line_y, line_end, True)

        # header table
        y = y - 30
        description_position = x + 80
        debit_position = x + 250
        deposit_position = x + 320
        fee_position = x + 410
        balance_position = x + 480

        BuildReport.build_ordinary_text(c, x, y, 'Date', True)
        BuildReport.build_ordinary_text(c, description_position, y, 'Description', True)
        BuildReport.build_ordinary_text(c, debit_position, y, 'Debits', True)
        BuildReport.build_ordinary_text(c, deposit_position, y, 'Deposits', True)
        BuildReport.build_ordinary_text(c, fee_position, y, 'Fee', True)
        BuildReport.build_ordinary_text(c, balance_position, y, 'Balance', True)

        line_y = y - 15
        line_end = 580
        BuildReport.build_line(c, line_x, line_y, line_end, False)

        # put details here
        y = y - 5
        print('request data')
        print (request_data.data)
        for request in request_data.data:
            if y < 50:
                c.showPage()
                y = 735
                c.drawImage(custom_file, x, y, width=200, height=22)

            y = y - 25
            due_at = datetime.datetime.strptime(request[Constant.UPDATED_AT_FIELD], Constant.DB_DATE_TIMESTAMP)
            BuildReport.build_ordinary_text(c, x, y, due_at.strftime(date_data_format), False)
            # description_text = 'Cash Deposit'
            if request['type'] == 'DEPOSIT':
                if request['transaction_type'] == 'ELECTRONIC':
                    description_text = 'Electronic Deposit'
                else:
                    description_text = 'Cash Deposit'
            else:
                description_text = "Pay to {payee}".format(payee=request['payee']['name'])

            if len(description_text) > 30:
                first_text = description_text[0:30]
                BuildReport.build_ordinary_text(c, description_position, y, first_text, False)
                y = y - 25
                second_text = description_text[30:len(description_text)]
                BuildReport.build_ordinary_text(c, description_position, y, second_text, False)
            else:
                BuildReport.build_ordinary_text(c, description_position, y, description_text, False)

            amount_request = float(request['amount'])
            if request['type'] == Constant.TYPE_DEPOSIT:
                BuildReport.build_ordinary_text(c, deposit_position, y,
                                                BuildReport.format_report_number(amount_request), False)
            else:
                BuildReport.build_ordinary_text(c, debit_position, y,
                                                "- {amount}"
                                                .format(amount=BuildReport.format_report_number(amount_request)),
                                                False)

            fee = float(request['fee'])
            fee_display = "({fee})".format(fee=BuildReport.format_report_number(fee))\
                if request['type'] == Constant.TYPE_DEPOSIT \
                else "{fee}".format(fee=BuildReport.format_report_number(fee))
            BuildReport.build_ordinary_text(c, fee_position, y, fee_display, False)

            request_balance = float(request['ledger']['resulting_available_balance']) # TODO:: double check the source
            BuildReport.build_ordinary_text(c, balance_position, y, BuildReport.format_report_number(request_balance),
                                            False)

        y = y - 30
        BuildReport.build_ordinary_text(c, description_position, y, 'Ending Balance', True)
        BuildReport.build_ordinary_text(c, balance_position, y, ending_balance, True)

        line_y = y - 30
        line_end = 580
        BuildReport.build_line(c, line_x, line_y, line_end, True)

        y = y - 100
        BuildReport.build_sub_header(c, line_x, y, 'Electronic', True)
        BuildReport.build_sub_header(c, line_x + 200, y, 'Get your vendors to ', True)
        c.drawImage(titan_logo, line_x + 400, y - 30, width=50, height=50, mask='auto')
        y = y - 20
        BuildReport.build_sub_header(c, line_x, y, 'payments', True)
        BuildReport.build_sub_header(c, line_x + 200, y, 'sign up and save on', True)

        y = y - 20
        BuildReport.build_sub_header(c, line_x, y, 'made', True)
        BuildReport.build_sub_header(c, line_x + 200, y, 'bill pay fees.', True)
        y = y - 20
        BuildReport.build_sub_header(c, line_x, y, 'easy!', True)
        y = y - 20

        c.setLineWidth(.3)
        c.setFont(font, 12)

        # page 2
        # c.showPage()
        # c.drawString(120, 653, "JOHN DOE")
        #
        c.save()

        return watermark_file
