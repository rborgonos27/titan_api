from rest_framework import permissions


class UpdateOwnerProfile(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):

        if request.method in permissions.SAFE_METHODS:
            return True

        return obj.id == request.user.id


class IsAdminOrStaff(permissions.BasePermission):
    message = 'You do not have access to user this resource. Contact your administrator.'

    def has_permission(self, request, view):
        # if not hasattr(request.user, 'user_type'):
        #     return None

        return request.user.user_type == 'super_admin' or request.user.user_type == 'staff'
