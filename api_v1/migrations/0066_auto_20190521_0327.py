# Generated by Django 2.0.7 on 2019-05-21 03:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api_v1', '0065_auto_20190521_0202'),
    ]

    operations = [
        migrations.AlterField(
            model_name='file',
            name='mime_type',
            field=models.TextField(blank=True, max_length=200),
        ),
        migrations.AlterField(
            model_name='file',
            name='type',
            field=models.TextField(choices=[('PAYMENT', 'PAYMENT'), ('DEPOSIT', 'DEPOSIT'), ('USER', 'USER'), ('PAYEE', 'PAYEE')], default='PAYMENT', max_length=200),
        ),
        migrations.AlterField(
            model_name='file',
            name='url',
            field=models.TextField(max_length=200),
        ),
    ]
