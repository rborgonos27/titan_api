# Generated by Django 2.0.7 on 2018-11-15 06:37

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api_v1', '0023_user_business_contact_number'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserPayee',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_by', models.IntegerField(blank=True)),
                ('updated_by', models.IntegerField(blank=True)),
                ('created_at', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('updated_at', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('payee', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api_v1.Payee')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
