# Generated by Django 2.0.7 on 2019-09-30 03:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api_v1', '0078_payee_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='file',
            name='remarks',
            field=models.TextField(blank=True, default=None, max_length=100, null=True),
        ),
    ]
