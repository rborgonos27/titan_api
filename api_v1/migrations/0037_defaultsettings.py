# Generated by Django 2.0.7 on 2019-01-08 09:16

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api_v1', '0036_auto_20190108_0902'),
    ]

    operations = [
        migrations.CreateModel(
            name='DefaultSettings',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tier_1_deposit_rate', models.DecimalField(decimal_places=4, default=None, max_digits=9)),
                ('tier_2_deposit_rate', models.DecimalField(decimal_places=4, default=None, max_digits=9)),
                ('tier_3_deposit_rate', models.DecimalField(decimal_places=4, default=None, max_digits=9)),
                ('tier_1_deposit_threshold', models.DecimalField(decimal_places=2, default=None, max_digits=9)),
                ('tier_2_deposit_threshold', models.DecimalField(decimal_places=2, default=None, max_digits=9)),
                ('tier_3_deposit_threshold', models.DecimalField(decimal_places=2, default=None, max_digits=9)),
                ('tier_1_payment_rate', models.DecimalField(decimal_places=4, default=None, max_digits=9)),
                ('tier_2_payment_rate', models.DecimalField(decimal_places=4, default=None, max_digits=9)),
                ('tier_3_payment_rate', models.DecimalField(decimal_places=4, default=None, max_digits=9)),
                ('tier_1_payment_threshold', models.DecimalField(decimal_places=2, default=None, max_digits=9)),
                ('tier_2_payment_threshold', models.DecimalField(decimal_places=2, default=None, max_digits=9)),
                ('tier_3_payment_threshold', models.DecimalField(decimal_places=2, default=None, max_digits=9)),
                ('tax_payment_rate', models.DecimalField(decimal_places=2, max_digits=9)),
                ('payroll_payment_rate', models.DecimalField(decimal_places=2, max_digits=9)),
                ('created_by', models.IntegerField(blank=True)),
                ('updated_by', models.IntegerField(blank=True)),
                ('created_at', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('updated_at', models.DateTimeField(blank=True, default=datetime.datetime.now)),
            ],
        ),
    ]
