# Generated by Django 2.0.7 on 2019-01-02 05:29

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api_v1', '0030_fee'),
    ]

    operations = [
        migrations.RenameField(
            model_name='fee',
            old_name='deposit_thresh_hold',
            new_name='deposit_threshold',
        ),
        migrations.RenameField(
            model_name='fee',
            old_name='payment_thresh_hold',
            new_name='payment_threshold',
        ),
    ]
