# Generated by Django 2.0.7 on 2018-08-29 05:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api_v1', '0012_request_remarks'),
    ]

    operations = [
        migrations.AlterField(
            model_name='file',
            name='filename',
            field=models.FileField(upload_to=''),
        ),
    ]
