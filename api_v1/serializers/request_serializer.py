from rest_framework import serializers

from .user_serializer import UserSerializer
from .payee_serializers import PayeeSerializer
from .transaction_serializer import TransactionSerializer
from .ledger_serializer import LedgerSerializer
from .file_serializer import FileSerializer
from .. models import Request, User, Ledger, File


class RequestSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, read_only=True)
    user = UserSerializer(read_only=True)
    payee = PayeeSerializer(read_only=True)
    payee_id = serializers.IntegerField(write_only=True, required=False)
    # transactions = TransactionSerializer(read_only=True)
    request_id = serializers.CharField(required=False, read_only=True)
    user_id = serializers.IntegerField(write_only=True)
    created_by = serializers.IntegerField(write_only=True)
    updated_by = serializers.IntegerField(write_only=True)
    # created_at = serializers.DateTimeField(write_only=True)
    updated_at = serializers.DateTimeField(read_only=True)
    waived_at = serializers.DateTimeField(read_only=True)
    last_updated_by = serializers.SerializerMethodField()
    created_by_user = serializers.SerializerMethodField()
    ledger = serializers.SerializerMethodField()
    ticket = serializers.SerializerMethodField()


    def get_last_updated_by (self, obj):
        user = User.objects.get(id=obj.updated_by)
        if user:
            return UserSerializer(user).data  # assuming it has this field
        else:
            return None

    def get_created_by_user (self, obj):
        user = User.objects.get(id=obj.created_by)
        if user:
            return UserSerializer(user).data  # assuming it has this field
        else:
            return None

    def get_ledger (self, obj):
        try:
            ledger = Ledger.objects \
                .get(transaction_id=obj.request_id)

            return LedgerSerializer(ledger).data  # assuming it has this field
        except:
            return None

    def get_ticket (self, obj):
        file = File.objects.filter(resource_id=obj.id).filter(type='DEPOSIT_TICKET')
        if file:
            file_data = FileSerializer(file, many=True).data
            if len(file_data) > 0:
                return file_data[0]['filename']
            else:
                return None
        else:
            return None

    class Meta:
        model = Request
        fields = (
            'id',
            'request_id',
            'user',
            'payee',
            'payee_id',
            'user_id',
            'type',
            'status',
            'to',
            'amount',
            'fee',
            'remarks',
            'due_at',
            'pickup_at',
            'transaction_type',
            'pickup_type',
            'ledger',
            'created_by',
            'updated_by',
            'created_at',
            'waived_at',
            'updated_at',
            'last_updated_by',
            'created_by_user',
            'ticket'
        )

    def __str__(self):
        return self.data

    def create(self, validated_data):
        return Request.objects.create(**validated_data)
