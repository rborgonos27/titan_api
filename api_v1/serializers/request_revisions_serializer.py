from rest_framework import serializers

from .user_serializer import UserSerializer
from .payee_serializers import PayeeSerializer
from .transaction_serializer import TransactionSerializer
from .. models import RequestRevisions, User


class RequestRevisionsSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, read_only=True)
    user = UserSerializer(read_only=True)
    payee = PayeeSerializer(read_only=True)
    payee_id = serializers.IntegerField(write_only=True)
    request_id = serializers.CharField(required=False, read_only=True)
    user_id = serializers.IntegerField(write_only=True)
    created_by = serializers.IntegerField(write_only=True)
    updated_by = serializers.IntegerField(write_only=True)
    updated_at = serializers.DateTimeField(read_only=True)
    last_updated_by = serializers.SerializerMethodField()
    created_by_user = serializers.SerializerMethodField()


    def get_last_updated_by (self, obj):
        user = User.objects.get(id=obj.updated_by)
        if user:
            return UserSerializer(user).data  # assuming it has this field
        else:
            return None

    def get_created_by_user (self, obj):
        user = User.objects.get(id=obj.created_by)
        if user:
            return UserSerializer(user).data  # assuming it has this field
        else:
            return None

    class Meta:
        model = RequestRevisions
        fields = (
            'id',
            'request_request_id',
            'request_id',
            'user',
            'payee',
            'payee_id',
            'user_id',
            'type',
            'status',
            'to',
            'amount',
            'fee',
            'remarks',
            'due_at',
            'pickup_at',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
            'updated_by',
            'created_by_user',
            'last_updated_by',
        )

    def __str__(self):
        return self.data

