from rest_framework import serializers
from django.forms.fields import FileField
from django.conf import settings
from ..models import File, PayeeAgreement
import datetime


class PayeeFileSerializer(serializers.ModelSerializer):
    filename = serializers.FileField(max_length=None, allow_empty_file=False)
    filename_url = serializers.SerializerMethodField()
    payee_agreement_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = File
        fields = (
            'id',
            'filename',
            'url',
            'filename_url',
            'mime_type',
            'expire_at',
            'type',
            'resource_id',
            'payee_agreement_id'
        )

    def get_filename_url(self, obj):
        return obj.filename.url

    def create(self, validated_data):
        file = File(
            user_id=1,
            filename=validated_data['filename'],
            url=validated_data['url'],
            mime_type=validated_data['mime_type'],
            expire_at=validated_data['expire_at'],
            type=validated_data['type'],
            resource_id=validated_data['resource_id'],
            created_by=1,
            updated_by=1,
            created_at=datetime.datetime.now(),
            updated_at=datetime.datetime.now(),
        )

        file.save()
        payee_agreement = PayeeAgreement.objects.get(id=validated_data['payee_agreement_id'])
        payee_agreement.is_accepted = True
        payee_agreement.remarks = 'Signed'
        payee_agreement.save()

        return file
