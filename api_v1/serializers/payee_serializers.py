from rest_framework import serializers
from ..models import Payee, PayeeAgreement, File, User
from .payee_agreement_serializer import PayeeAgreementSerializer
from .file_serializer import FileSerializer
import datetime


class PayeeSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, read_only=True)
    payee_id = serializers.CharField(required=False, read_only=True)
    deleted_at = serializers.DateTimeField(required=False, read_only=True)
    payee_agreement = serializers.SerializerMethodField()
    agreement_file = serializers.SerializerMethodField()

    def get_payee_agreement(self, obj):
        # try:
        #     payee_agreement = PayeeAgreement.objects.get(payee_id=obj.id)
        #     if payee_agreement:
        #         return PayeeAgreementSerializer(payee_agreement).data  # assuming it has this field
        #     else:
        #         return None
        # except PayeeAgreement.DoesNotExist:
            return None


    def get_agreement_file(self, obj):
        file = File.objects.filter(resource_id=obj.id).filter(type='PAYEE')
        if file:
            file_data = FileSerializer(file, many=True).data
            if len(file_data) > 0:
                return file_data[0]['filename']
            else:
                return None
        else:
            return None

    class Meta:
        model = Payee
        fields = (
            'id',
            'payee_id',
            'name',
            'bank_name',
            'address',
            'email',
            'bank_account_name',
            'bank_account',
            'bank_routing_number',
            'payee_agreement',
            'status',
            'agreement_file',
            'address_2',
            'city',
            'state',
            'zip_code',
            'deleted_at',
        )

    def __str__(self):
        return self.data

    def create(self, validated_data):
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user

        print ('user')
        member = User.objects.get(id=user.id)
        print(member.parent_user_id)
        print(member.first_name)
        created_by = member.parent_user_id if member.parent_user_id is not None else member.id
        payee = Payee(
            name=validated_data['name'],
            bank_name=validated_data['bank_name'],
            bank_account_name=validated_data['bank_account_name'],
            bank_account=validated_data['bank_account'],
            bank_routing_number=validated_data['bank_routing_number'],
            address=validated_data['address'],
            email=validated_data['email'],
            address_2=validated_data['address_2'],
            city=validated_data['city'],
            state=validated_data['state'],
            zip_code=validated_data['zip_code'],
            status=validated_data['status'],
            created_by=created_by,
            updated_by=user.id,
            created_at=datetime.datetime.now(),
            updated_at=datetime.datetime.now(),
        )

        payee.save()
        return payee
