from rest_framework import serializers
from .. models import User, Request
from django.db.models import Sum
from . wallet_info_serializer import WalletInfoSerializer


class MemberWalletSerializer(serializers.ModelSerializer):
    username = serializers.CharField(read_only=True)
    date_joined = serializers.CharField(read_only=True)
    member_account_id = serializers.CharField(read_only=True)
    id = serializers.IntegerField(read_only=True)
    primary_contact_name = serializers.CharField(allow_blank=True)
    primary_contact_number = serializers.CharField(allow_blank=True)
    business_name = serializers.CharField(allow_blank=True)
    principal_city_of_business = serializers.CharField(allow_blank=True)
    address = serializers.CharField(read_only=True)
    business_contact_number = serializers.CharField(read_only=True)
    wallet_info = WalletInfoSerializer(many=True, read_only=True, source='wallet_set')
    member_fee = serializers.SerializerMethodField()

    def get_member_fee(self, obj):
        try:
            fee = Request.objects.filter(user_id=obj.id)\
                .filter(status='COMPLETED')\
                .aggregate(total=Sum('fee'))

            return fee['total'] if fee['total'] != None else 0
        except:
            return None

    class Meta:
        model = User
        fields = [
            'id',
            'member_account_id',
            'email',
            'first_name',
            'last_name',
            'username',
            'middle_name',
            'primary_contact_name',
            'primary_contact_number',
            'business_name',
            'principal_city_of_business',
            'address',
            'user_type',
            'business_contact_number',
            'date_joined',
            'wallet_info',
            'member_fee'
        ]
