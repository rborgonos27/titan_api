from rest_framework import serializers
from .. models import Wallet


class WalletInfoSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, read_only=True)
    created_by = serializers.IntegerField(write_only=True)
    updated_by = serializers.IntegerField(write_only=True)
    created_at = serializers.DateTimeField(write_only=True)
    updated_at = serializers.DateTimeField(write_only=True)

    class Meta:
        model = Wallet
        fields = (
            'id',
            'type',
            'balance',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        )

        def __str__(self):
            return self
