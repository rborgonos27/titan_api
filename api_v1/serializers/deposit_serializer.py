from rest_framework import serializers
from .. models import Deposit
from .user_serializer import UserSerializer
import datetime


class DepositSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    status = serializers.CharField(required=False, read_only=True)
    transaction_id = serializers.CharField(required=False, read_only=True)

    class Meta:
        model = Deposit
        fields = (
            'transaction_id',
            'schedule_at',
            'type',
            'status',
            'user',
            'amount',
        )

    def __str__(self):
        return self.data

    def create(self, validated_data):
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user

        deposit = Deposit(
            user=user,
            schedule_at=validated_data['schedule_at'],
            type=validated_data['type'],
            status='pending',
            amount=validated_data['amount'],
            created_by=user.id,
            updated_by=user.id,
            created_at=datetime.datetime.now(),
            updated_at=datetime.datetime.now(),
        )


        deposit.save()

        return deposit