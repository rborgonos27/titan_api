from rest_framework import serializers
from .. models import User


class UserAccountSerializer(serializers.ModelSerializer):
    username = serializers.CharField(read_only=True)
    date_joined = serializers.CharField(read_only=True)
    member_account_id = serializers.CharField(read_only=True)
    id = serializers.IntegerField(read_only=True)
    primary_contact_name = serializers.CharField(allow_blank=True)
    primary_contact_number = serializers.CharField(allow_blank=True)
    business_name = serializers.CharField(allow_blank=True)
    principal_city_of_business = serializers.CharField(allow_blank=True)
    address = serializers.CharField(read_only=True)
    business_contact_number = serializers.CharField(read_only=True)

    class Meta:
        model = User
        fields = [
            'id',
            'member_account_id',
            'email',
            'password',
            'first_name',
            'last_name',
            'username',
            'middle_name',
            'password',
            'primary_contact_name',
            'primary_contact_number',
            'business_name',
            'principal_city_of_business',
            'address',
            'user_type',
            'business_contact_number',
            'date_joined',
            'is_first_time_member',
        ]