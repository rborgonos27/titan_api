from rest_framework import serializers

# from .request_serializer import RequestSerializer
from .. models import Transaction


class TransactionSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, read_only=True)
    # request = RequestSerializer(read_only=True)
    request_id = serializers.IntegerField(required=False, read_only=True)
    user_id = serializers.IntegerField(write_only=True)
    created_by = serializers.IntegerField(write_only=True)
    updated_by = serializers.IntegerField(write_only=True)
    created_at = serializers.DateTimeField(write_only=True)
    updated_at = serializers.DateTimeField(write_only=True)

    class Meta:
        model = Transaction
        fields = (
            'id',
            'transaction_id',
            'user_id',
            'request_id',
            'type',
            'status',
            'to',
            'amount',
            'remarks',
            'process_at',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',

        )

        def __str__(self):
            return self

        def create(self, validated_data):
            return Transaction.objects.create(**validated_data)
