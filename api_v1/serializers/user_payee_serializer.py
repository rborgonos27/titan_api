from rest_framework import serializers
from ..models import UserPayee, User, Payee
from .user_serializer import UserSerializer
from .payee_serializers import PayeeSerializer

import datetime


class UserPayeeSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, read_only=True)
    user = UserSerializer(read_only=True)
    payee = PayeeSerializer(read_only=True)
    user_id = serializers.IntegerField(write_only=True)
    payee_id = serializers.IntegerField(write_only=True)
    created_at = serializers.DateTimeField(read_only=True)
    updated_at = serializers.DateTimeField(read_only=True)

    class Meta:
        model = UserPayee
        fields = (
            'id',
            'user',
            'payee',
            'user_id',
            'payee_id',
            'created_at',
            'updated_at',
        )

    def __str__(self):
        return self.data
