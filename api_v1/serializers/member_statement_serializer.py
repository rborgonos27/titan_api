from rest_framework import serializers
from . user_serializer import UserSerializer
from . file_serializer import FileSerializer
from . request_detail_serializer import RequestDetailSerializer
from .. models import File, User, MemberStatement, Request, Wallet, Ledger
from django.core.files.storage import FileSystemStorage
from django.db.models import Sum
from django.core.files import File as FileObject
from utils.build_report import BuildReport

from reportlab.pdfgen import canvas
from PyPDF2 import PdfFileWriter, PdfFileReader

import time
from reportlab.lib.enums import TA_JUSTIFY
from reportlab.lib.pagesizes import letter
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image
from reportlab.platypus.tables import Table, TableStyle
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import inch
from reportlab.lib import colors
from reportlab.pdfbase.pdfmetrics import stringWidth

import datetime


class MemberStatementSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, read_only=True)
    user = UserSerializer(read_only=True)
    user_id = serializers.IntegerField(write_only=True)
    statement_file = serializers.SerializerMethodField()

    class Meta:
        model = MemberStatement
        fields = (
            'id',
            'user',
            'user_id',
            'cutoff_at',
            'statement_file',
        )

    def __str__(self):
        return self

    def get_statement_file(self, obj):
        file = File.objects.filter(resource_id=obj.id).filter(type='MEMBER_STATEMENT')
        if file:
            file_data = FileSerializer(file, many=True).data
            if len(file_data) > 0:
                return file_data[0]['filename']
            else:
                return None
        else:
            return None


    def create(self, validated_data):
        print(validated_data)
        # cut off
        member_statement = MemberStatement(
            cutoff_at=validated_data['cutoff_at'],
            user_id=validated_data['user_id'],
            created_by=1,
            updated_by=1,
            created_at=datetime.datetime.now(),
            updated_at=datetime.datetime.now(),
        )

        member_statement.save()
        # self.update_ledger(member_statement)
        print ('cutoff')
        print (validated_data['cutoff_at'])

        file_name = self.build_report(member_statement)
        self.upload_statement(file_name, member_statement)
        return member_statement

    def update(self, instance, validated_data):
        print(validated_data)
        instance.cutoff_at = validated_data.get('cutoff_at', instance.cutoff_at),
        instance.save()

        file_name = self.build_report(instance)
        self.upload_statement(file_name, instance)

        return instance

    @staticmethod
    def upload_statement(file_name, member_statement):
        output_file = open(file_name, "rb")

        try :
            file = File.object.get(resource_id=member_statement.id)
            file.filename = FileObject(output_file)
            file.save()
        except:
            file = File(
                user_id=member_statement.user_id,
                filename=FileObject(output_file),
                url='filename',
                mime_type='application/pdf',
                expire_at=datetime.datetime.now() + datetime.timedelta(hours=1),
                type='MEMBER_STATEMENT',
                resource_id=member_statement.id,
                created_by=member_statement.user_id,
                updated_by=member_statement.user_id,
                created_at=datetime.datetime.now(),
                updated_at=datetime.datetime.now(),
            )
            file.save()

        return file

    @staticmethod
    def update_ledger(member_statement):
        requests = Request.objects.filter(user_id=member_statement.user_id, status='COMPLETED')
        i = 0
        resulting_available_balance = 0
        for request in requests:
            if request.type == 'DEPOSIT':
                request_balance = float(request.amount) - float(request.fee)
                if i == 0:
                    resulting_available_balance = request_balance
                else:
                    resulting_available_balance = resulting_available_balance + request_balance
            else:
                request_balance = float(request.amount) + float(request.fee)
                resulting_available_balance = resulting_available_balance - request_balance
            i = i + 1
            print("resulting balances")
            print(resulting_available_balance)

            ledger = Ledger.objects.get(transaction_id=request.request_id)
            ledger.resulting_available_balance = resulting_available_balance
            ledger.amount = request.amount
            ledger.save()


    @staticmethod
    def build_report(member_statement):
        watermark_file = "storage/statement_file_{user_id}.pdf".format(user_id=member_statement.user_id)
        custom_file = 'storage/images/titan-vault.png'
        titan_logo = 'storage/images/titan-logo.png'
        # request_data = Request.objects.
        member = User.objects.get(id=member_statement.user_id)

        # member_id
        member_id = member.member_account_id
        member_name = "{member_name}"\
            .format(member_name=member.business_name)
        member_address = "{address} {address_2}"\
            .format(address=member.address,
                    address_2='' if not member.address_2 else member.address_2
                    )

        member_address_2 = "{city} {state} {zip_code}" \
            .format(city='' if not member.city else member.city,
                    state='' if not member.state else member.state,
                    zip_code='' if not member.zip_code else member.zip_code
                    )

        date_format = "%b %d"
        date_data_format = "%b - %d"
        member_start = member.date_joined.strftime("%b %Y")
        date_today = datetime.datetime.now()

        # identify cut off
        # datetime.datetime.strptime(request['due_at'], "%Y-%m-%dT%H:%M:%S")
        month_cutoff = date_today.strftime("%m")
        if int(date_today.strftime("%m")) == int(member.date_joined.strftime("%m")) and \
            int(date_today.strftime("%d")) > int(member.date_joined.strftime("%d")):
            month_cutoff = int(date_today.strftime("%m")) + 1

        cut_off_date_string = "{year}-{month}-{day} 00:00:00".format(year=date_today.strftime("%Y"),
                                                                     month=month_cutoff,
                                                                     day=member.date_joined.strftime("%d"))

        cut_off_start_date_string = "{year}-{month}-{day} 00:00:00".format(year=date_today.strftime("%Y"),
                                                                           month=int(month_cutoff) - 1,
                                                                           day=member.date_joined.strftime("%d"))

        cut_off_date = datetime.datetime.strptime(cut_off_date_string, "%Y-%m-%d %H:%M:%S")
        cut_off_start_date = datetime.datetime.strptime(cut_off_start_date_string, "%Y-%m-%d %H:%M:%S")

        print ("cut_off_date")
        print (cut_off_date)

        # compute by cut off
        month_start = date_today - datetime.timedelta(days=30)

        date_from = cut_off_start_date.strftime(date_format)
        date_to = cut_off_date.strftime(date_format)
        member_date = "For {date_from}, to {date_to} {year}"\
            .format(date_from=date_from,
                    date_to=date_to,
                    year=cut_off_date.strftime("%Y"))

        print("start date")
        print(cut_off_start_date)
        print("start date")
        print(cut_off_date)
        # deposits
        request_amount_deposit = Request.objects.filter(user_id=member_statement.user_id,
                                                        type='DEPOSIT',
                                                        status='COMPLETED',
                                                        updated_at__range=[cut_off_start_date, cut_off_date])\
            .aggregate(deposit=Sum('amount') - Sum('fee'))

        request_amount_payment = Request.objects.filter(user_id=member_statement.user_id,
                                                        type='PAYMENT',
                                                        status='COMPLETED',
                                                        updated_at__range=[cut_off_start_date, cut_off_date]) \
            .aggregate(payment=Sum('amount') + Sum('fee'))

        print("request amount deposit")
        print(request_amount_deposit)
        if not request_amount_deposit['deposit']:
            request_amount_deposit = { 'deposit': 0 }

        # payments
        if not request_amount_payment['payment']:
            request_amount_payment = { 'payment': 0 }

        print (request_amount_payment)
        # beginning balance
        request_data = Request.objects \
                           .filter(user_id=member_statement.user_id,
                                   status='COMPLETED') \
                           .order_by('due_at')[:1].get()
        beginning_balance = float(request_data.amount) - float(request_data.fee)

        # actual balance
        wallet = Wallet.objects.filter(user_id=member_statement.user_id, type='AVAILABLE_BALANCE')[:1].get()

        # transactions
        requests = Request.objects.filter(user_id=member_statement.user_id,
                                          status='COMPLETED',
                                          updated_at__range=[cut_off_start_date, cut_off_date]
                                          )
        request_data = RequestDetailSerializer(requests, many=True)

        deposits = BuildReport.format_transaction_number(request_amount_deposit['deposit'])
        payments = BuildReport.format_transaction_number(request_amount_payment['payment'])
        beginning_balance = BuildReport.format_transaction_number(beginning_balance)
        actual_balance = BuildReport.format_transaction_number(wallet.balance)

        c = canvas.Canvas(watermark_file)
        c.setTitle(member_date)
        font = 'Helvetica'

        x = 30
        y = 735
        next_line_start = 320
        # LOGO
        c.drawImage(custom_file, x, y, width=200, height=22)

        # header
        c.setFont(font, 26)
        line_x = next_line_start
        line_y = 729
        line_end = 580

        c.drawString(line_x, y + 5, 'STATEMENT')
        BuildReport.build_line(c, line_x, line_y, line_end, True)

        y = 695

        # next row
        account_statement = 'Account Summary'
        BuildReport.build_sub_header(c, x, y, account_statement, True)
        line_x = x
        line_y = y - 5
        line_end = 280

        BuildReport.build_line(c, line_x, line_y, line_end, False)

        # 2nd column
        line_x = next_line_start
        line_y = y - 5
        line_end = 580

        BuildReport.build_sub_header(c, line_x, y, member_date, False)
        BuildReport.build_line(c, line_x, line_y, line_end, False)

        BuildReport.build_sub_header_small(c, line_x + 5, y - 25, 'MEMBER NUMBER', True)
        BuildReport.build_ordinary_text(c, line_x + 5, y - 40, member_id, False)

        BuildReport.build_ordinary_text(c, line_x + 5, y - 65, member_name, False)
        BuildReport.build_ordinary_text(c, line_x + 5, y - 80, member_address, False)
        BuildReport.build_ordinary_text(c, line_x + 5, y - 90, member_address_2, False)

        BuildReport.build_sub_header_small(c, line_x + 5, y - 110, 'Contact Information', True)
        BuildReport.build_line(c, line_x, line_y - 115, line_end, False)

        BuildReport.build_ordinary_text(c, line_x + 5, y - 140, 'memberservices@titan-vault.com', False)
        BuildReport.build_ordinary_text(c, line_x + 5, y - 155, 'Urgent Request or Inquiries:', False)
        BuildReport.build_ordinary_text(c, line_x + 5, y - 170, '1-650-680-8328', False)

        # 1st column
        add_tab = 170

        y = y - 20
        BuildReport.build_ordinary_text(c, x + 10, y, 'Beginning Balance', False)
        BuildReport.build_ordinary_text(c, x + add_tab, y, beginning_balance, True)

        y = y - 15
        BuildReport.build_ordinary_text(c, x + 10, y, 'Payments', False)
        BuildReport.build_ordinary_text(c, x + add_tab, y, payments, True)

        y = y - 15
        BuildReport.build_ordinary_text(c, x + 10, y, 'Deposits', False)
        BuildReport.build_ordinary_text(c, x + add_tab, y, deposits, True)

        line_x = x
        line_y = y - 15
        line_end = 280
        BuildReport.build_line(c, line_x, line_y, line_end, False)
        y = y - 30
        ending_balance_as_of = "Ending balance as of {date}".format(date=date_today.strftime("%b %Y"))
        BuildReport.build_ordinary_text(c, x, y, ending_balance_as_of, True)
        BuildReport.build_ordinary_text(c, x + add_tab, y, actual_balance, True)
        line_y = y - 10
        BuildReport.build_line(c, line_x, line_y, line_end, False)

        y = y - 30
        member_since_text = "Member Since {member_since}".format(member_since=member_start)
        BuildReport.build_sub_header_small(c, x, y, member_since_text, True)

        y = y - 30
        long_text = "Contact us by email for questions, on this statement, "
        long_text_2 = "change of business information, and general inquiries,"
        long_text_3 = "24 hours a day, 7 days a week."
        BuildReport.build_ordinary_text(c, x, y, long_text, False)
        y = y - 15
        BuildReport.build_ordinary_text(c, x, y, long_text_2, False)
        y = y - 15
        BuildReport.build_ordinary_text(c, x, y, long_text_3, False)

        y = y - 25
        BuildReport.build_sub_header_small(c, x, y, 'Your Transaction Details', True)

        line_y = y - 15
        line_end = 580
        BuildReport.build_line(c, line_x, line_y, line_end, True)

        # header table
        y = y - 30
        description_position = x + 80
        debit_position = x + 250
        deposit_position = x + 320
        fee_position = x + 410
        balance_position = x + 480

        BuildReport.build_ordinary_text(c, x, y, 'Date', True)
        BuildReport.build_ordinary_text(c, description_position, y, 'Description', True)
        BuildReport.build_ordinary_text(c, debit_position, y, 'Debits', True)
        BuildReport.build_ordinary_text(c, deposit_position, y, 'Deposits', True)
        BuildReport.build_ordinary_text(c, fee_position, y, 'Fee', True)
        BuildReport.build_ordinary_text(c, balance_position, y, 'Balance', True)

        line_y = y - 15
        line_end = 580
        BuildReport.build_line(c, line_x, line_y, line_end, False)

        # put details here
        y = y - 5
        for request in request_data.data:
            print("statement y")
            print(y)
            if y < 50:
                c.showPage()
                y = 735
                c.drawImage(custom_file, x, y, width=200, height=22)

            y = y - 25
            due_at = datetime.datetime.strptime(request['due_at'], "%Y-%m-%dT%H:%M:%S")
            BuildReport.build_ordinary_text(c, x, y, due_at.strftime(date_data_format), False)
            # description_text = 'Cash Deposit'
            if request['type'] == 'DEPOSIT':
                description_text = 'Cash Deposit'
            else:
                description_text = "Pay to {payee}".format(payee=request['payee']['name'])

            if len(description_text) > 30:
                first_text = description_text[0:30]
                BuildReport.build_ordinary_text(c, description_position, y, first_text, False)
                y = y - 25
                second_text = description_text[30:len(description_text)]
                BuildReport.build_ordinary_text(c, description_position, y, second_text, False)
            else:
                BuildReport.build_ordinary_text(c, description_position, y, description_text, False)

            amount_request = float(request['amount'])
            if request['type'] == 'DEPOSIT':
                BuildReport.build_ordinary_text(c, deposit_position, y, BuildReport.format_report_number(amount_request), False)
            else:
                BuildReport.build_ordinary_text(c, debit_position, y, BuildReport.format_report_number(amount_request), False)

            fee = float(request['fee'])
            fee_display = "({fee})".format(fee=BuildReport.format_report_number(fee)) if request['type'] == 'DEPOSIT'\
                else "{fee}".format(fee=BuildReport.format_report_number(fee))
            BuildReport.build_ordinary_text(c, fee_position, y, fee_display, False)

            request_balance = float(request['ledger']['resulting_available_balance'])
            BuildReport.build_ordinary_text(c, balance_position, y, BuildReport.format_report_number(request_balance), False)

        y = y - 30
        BuildReport.build_ordinary_text(c, description_position, y, 'Ending Balance', True)
        BuildReport.build_ordinary_text(c, balance_position, y, BuildReport.format_report_number(wallet.balance), True)

        line_y = y - 30
        line_end = 580
        BuildReport.build_line(c, line_x, line_y, line_end, True)

        y = y - 100
        BuildReport.build_sub_header(c, line_x, y, 'Electronic', True)
        BuildReport.build_sub_header(c, line_x + 200, y, 'Get your vendors to ', True)
        c.drawImage(titan_logo, line_x + 400, y - 30, width=50, height=50, mask='auto')
        y = y - 20
        BuildReport.build_sub_header(c, line_x, y, 'payments', True)
        BuildReport.build_sub_header(c, line_x + 200, y, 'sign up and save on', True)

        y = y - 20
        BuildReport.build_sub_header(c, line_x, y, 'made', True)
        BuildReport.build_sub_header(c, line_x + 200, y, 'bill pay fees.', True)
        y = y - 20
        BuildReport.build_sub_header(c, line_x, y, 'easy!', True)
        y = y - 20
        print ('y value')
        print (y)

        c.setLineWidth(.3)
        c.setFont(font, 12)

        # page 2
        # c.showPage()
        # c.drawString(120, 653, "JOHN DOE")
        #
        c.save()

        return watermark_file
