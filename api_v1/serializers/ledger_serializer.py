from rest_framework import serializers
from . user_serializer import UserSerializer
from .. models import Ledger


class LedgerSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, read_only=True)
    user = UserSerializer(read_only=True)
    created_by = serializers.IntegerField(write_only=True)
    updated_by = serializers.IntegerField(write_only=True)
    created_at = serializers.DateTimeField(write_only=True)
    updated_at = serializers.DateTimeField(write_only=True)

    # resulting_available_balance = serializers.DecimalField(max_digits=9, decimal_places=2, write_only=True)
    # resulting_pending_payment_balance = serializers.DecimalField(max_digits=9, decimal_places=2, write_only=True)
    # resulting_pending_deposit_balance = serializers.DecimalField(max_digits=9, decimal_places=2, write_only=True)

    class Meta:
        model = Ledger
        fields = (
            'id',
            'user',
            'transaction_id',
            'transaction_status',
            'resulting_available_balance',
            'resulting_pending_payment_balance',
            'resulting_pending_deposit_balance',
            'amount',
            'notes',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        )

        def __str__(self):
            return self

        def create(self, validated_data):
            return Ledger.objects.create(**validated_data)
