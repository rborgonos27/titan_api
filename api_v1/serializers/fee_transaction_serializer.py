from rest_framework import serializers
from django.conf import settings
from ..models import FeeTransaction
from ..serializers import UserSerializer
import datetime


class FeeTransactionSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    fee_transaction_id = serializers.CharField(read_only=True)
    user_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = FeeTransaction
        fields = (
            'id',
            'user',
            'user_id',
            'fee_transaction_id',
            'type',
            'request_id',
            'status',
            'fee_percent',
            'transaction_amount',
            'total_transaction_amount',
            'fee_amount',
            'process_at',
        )

    def create(self, validated_data):
        fee_transaction = FeeTransaction(
            user_id=validated_data['user_id'],
            type=validated_data['type'],
            fee_percent=validated_data['fee_percent'],
            transaction_amount=validated_data['transaction_amount'],
            total_transaction_amount=validated_data['total_transaction_amount'],
            fee_amount=validated_data['fee_amount'],
            created_by=validated_data['user_id'],
            updated_by=validated_data['user_id'],
            process_at=validated_data['process_at'],
            request_id=validated_data['request_id'],
            status=validated_data['status'],
            created_at=datetime.datetime.now(),
            updated_at=datetime.datetime.now(),
        )

        fee_transaction.save()
        return fee_transaction
