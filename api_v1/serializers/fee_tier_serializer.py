from rest_framework import serializers
from django.conf import settings
from ..models import FeeTier
from ..serializers import UserSerializer
import datetime


class FeeTierSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    user_id = serializers.IntegerField(write_only=True)
    deposit_threshold = serializers.IntegerField(required=False, allow_null=True)
    payment_threshold = serializers.IntegerField(required=False, allow_null=True)

    class Meta:
        model = FeeTier
        fields = (
            'id',
            'user',
            'user_id',
            'type',
            'tier_no',
            'deposit_rate',
            'deposit_threshold',
            'payment_rate',
            'payment_threshold',
        )

    def create(self, validated_data):
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user

        fee_tier = FeeTier(
            user_id=validated_data['user_id'],
            tier_no=validated_data['tier_no'],
            deposit_rate=validated_data['deposit_rate'],
            payment_rate=validated_data['payment_rate'],
            payment_threshold=validated_data['payment_threshold'],
            created_by=user.id,
            updated_by=user.id,
            created_at=datetime.datetime.now(),
            updated_at=datetime.datetime.now(),
        )

        fee_tier.save()
        return fee_tier
