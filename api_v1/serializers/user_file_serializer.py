from rest_framework import serializers
from django.forms.fields import FileField
from django.conf import settings
from ..models import File
import datetime


class UserFileSerializer(serializers.ModelSerializer):
    filename = serializers.FileField(max_length=None, allow_empty_file=False)
    filename_url = serializers.SerializerMethodField()

    class Meta:
        model = File
        fields = (
            'id',
            'filename',
            'url',
            'filename_url',
            'mime_type',
            'expire_at',
            'type',
            'resource_id'
        )

    def get_filename_url(self, obj):
        return obj.filename.url

    def create(self, validated_data):
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user

        file = File(
            user=user,
            filename=validated_data['filename'],
            url=validated_data['url'],
            mime_type=validated_data['mime_type'],
            expire_at=validated_data['expire_at'],
            type=validated_data['type'],
            resource_id=validated_data['resource_id'],
            created_by=user.id,
            updated_by=user.id,
            created_at=datetime.datetime.now(),
            updated_at=datetime.datetime.now(),
        )

        file.save()
        return file
