from rest_framework import serializers
from django.conf import settings
from ..models import Fee
from ..serializers import UserSerializer
import datetime



class FeeSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    user_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Fee
        fields = (
            'id',
            'user',
            'user_id',
            'tax_payment_rate',
            'payroll_payment_rate',
            'deposit_flat_rate',
            'payment_flat_rate',
            'fee_setting',
            'setting_change_next_month',
        )

    def create(self, validated_data):
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user

        fee = Fee(
            user_id=validated_data['user_id'],
            tax_payment_rate=validated_data['tax_payment_rate'],
            payroll_payment_rate=validated_data['payroll_payment_rate'],
            deposit_flat_rate=validated_data['deposit_flat_rate'],
            payment_flat_rate=validated_data['payment_flat_rate'],
            fee_setting=validated_data['fee_setting'],
            setting_change_next_month=validated_data['setting_change_next_month'],
            created_by=user.id,
            updated_by=user.id,
            created_at=datetime.datetime.now(),
            updated_at=datetime.datetime.now(),
        )

        fee.save()
        return fee
