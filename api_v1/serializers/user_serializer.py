from rest_framework import serializers
from django.conf import settings
import random
import string
from .. models import User, File
from .user_file_serializer import UserFileSerializer


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)
    username = serializers.CharField(read_only=True)
    date_joined = serializers.CharField(read_only=True)
    member_account_id = serializers.CharField(read_only=True)
    is_superuser = serializers.IntegerField(read_only=True)
    is_verified = serializers.IntegerField(read_only=True)
    id = serializers.IntegerField(read_only=True)
    location_id = serializers.CharField(allow_blank=True, required=False)
    deleted_at = serializers.DateTimeField(read_only=True)
    primary_contact_name = serializers.CharField(allow_blank=True)
    primary_contact_number = serializers.CharField(allow_blank=True)
    business_name = serializers.CharField(allow_blank=True)
    principal_city_of_business = serializers.CharField(allow_blank=True)
    address = serializers.CharField(allow_blank=True)
    business_contact_number = serializers.CharField(allow_blank=True)
    profile_picture = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = [
            'id',
            'member_account_id',
            'email',
            'first_name',
            'last_name',
            'username',
            'middle_name',
            'password',
            'primary_contact_name',
            'primary_contact_number',
            'business_name',
            'principal_city_of_business',
            'address',
            'address_2',
            'city',
            'state',
            'zip_code',
            'user_type',
            'business_contact_number',
            'date_joined',
            'is_first_time_member',
            'profile_picture',
            'deleted_at',
            'is_superuser',
            'is_verified',
            'parent_user_id',
            'location_id',
        ]

    def create(self, validated_data):
        username = "{from_email}{id}"\
            .format(from_email=validated_data['email'].split("@")[0],
                    id=self.id_generator())
        user = User(
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            username=username,
            primary_contact_name=validated_data['primary_contact_name'],
            primary_contact_number=validated_data['primary_contact_number'],
            business_name=validated_data['business_name'],
            principal_city_of_business=validated_data['principal_city_of_business'],
            address=validated_data['address'],
            business_contact_number=validated_data['business_contact_number'],
            user_type=validated_data['user_type'],
            is_first_time_member=validated_data['is_first_time_member'],
            address_2=validated_data['address_2'],
            city=validated_data['city'],
            state=validated_data['state'],
            zip_code=validated_data['zip_code'],
        )

        user.set_password(validated_data['password'])
        user.save()

        return user

    def get_profile_picture(self, obj):
        file = File.objects.filter(resource_id=obj.id).filter(type='USER').order_by('-created_at')
        file_data = UserFileSerializer(file, many=True).data
        print (file_data)
        
        
        
        if len(file_data) > 0:
            return file_data[0]['filename']
        else:
            return None

    @staticmethod
    def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
        return ''.join(random.choice(chars) for _ in range(size))

