from rest_framework import serializers
from . user_serializer import UserSerializer
from .. models import Wallet


class WalletSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, read_only=True)
    user = UserSerializer(read_only=True)
    created_by = serializers.IntegerField(write_only=True)
    updated_by = serializers.IntegerField(write_only=True)
    created_at = serializers.DateTimeField(write_only=True)
    updated_at = serializers.DateTimeField(write_only=True)

    class Meta:
        model = Wallet
        fields = (
            'id',
            'user',
            'type',
            'balance',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
        )

        def __str__(self):
            return self

        def create(self, validated_data):
            return Wallet.objects.create(**validated_data)
