from rest_framework import serializers
from django.forms.fields import FileField
from django.conf import settings
from ..models import File, User
from ..serializers import UserSerializer
import datetime


class FileSerializer(serializers.ModelSerializer):
    filename = serializers.FileField(max_length=None, allow_empty_file=False)
    user = UserSerializer(read_only=True)
    filename_url = serializers.SerializerMethodField()

    class Meta:
        model = File
        fields = (
            'id',
            'user',
            'filename',
            'url',
            'type',
            'filename_url',
            'mime_type',
            'remarks',
            'expire_at',
        )

    def get_filename_url(self, obj):
        return obj.filename.url

    def create(self, validated_data):
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
            user_data = user
            member = User.objects.get(id=user.id)
            if member.parent_user_id:
                user = User.objects.get(id=member.parent_user_id)
                user_data = user

        file = File(
            user=user_data,
            filename=validated_data['filename'],
            url=validated_data['url'],
            mime_type=validated_data['mime_type'],
            type=validated_data['type'],
            expire_at=validated_data['expire_at'],
            remarks=validated_data['remarks'],
            created_by=user.id,
            updated_by=user.id,
            created_at=datetime.datetime.now(),
            updated_at=datetime.datetime.now(),
        )

        file.save()

        return file
