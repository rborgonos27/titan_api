from rest_framework import serializers

from .user_serializer import UserSerializer
from .. models import Payment, User
import datetime


class PaymentSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    pay_to = UserSerializer(read_only=True)
    pay_to_id = serializers.IntegerField(write_only=True)

    status = serializers.CharField(required=False, read_only=True)
    transaction_id = serializers.CharField(required=False, read_only=True)

    class Meta:
        model = Payment
        fields = (
            'transaction_id',
            'pay_at',
            'status',
            'pay_at',
            'notes',
            'amount',
            'user',
            'pay_to',
            'pay_to_id',
        )

    def __str__(self):
        return self.data

    def create(self, validated_data):
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user

        pay_to_id = validated_data.pop('pay_to_id')

        try:
            pay_to = User.objects.get(id=pay_to_id)
        except User.DoesNotExist:
            pass
        except User.MultipleObjectsReturned:
            pass

        payment = Payment(
            user=user,
            pay_to=pay_to,
            pay_at=validated_data['pay_at'],
            status='pending',
            amount=validated_data['amount'],
            notes=validated_data['notes'],
            created_by=user.id,
            updated_by=user.id,
            created_at=datetime.datetime.now(),
            updated_at=datetime.datetime.now(),
        )

        payment.save()

        return payment
