from rest_framework import serializers
from . user_serializer import UserSerializer
from . file_serializer import FileSerializer
from .. models import PayeeAgreement, File, User
import datetime


class PayeeAgreementSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, read_only=True)
    agreement_file = serializers.SerializerMethodField()
    member_name = serializers.SerializerMethodField()

    class Meta:
        model = PayeeAgreement
        fields = (
            'id',
            'user_id',
            'payee_id',
            'remarks',
            'member_name',
            'is_accepted',
            'agreement_file',
        )

    def __str__(self):
        return self

    def get_agreement_file(self, obj):
        file = File.objects.filter(resource_id=obj.id).filter(type='PAYEE_AGREEMENT')
        if file:
            file_data = FileSerializer(file, many=True).data
            if len(file_data) > 0:
                return file_data[0]['filename']
            else:
                return None
        else:
            return None

    def get_member_name(self, obj):
        user = User.objects.filter(id=obj.user_id)
        if user:
            user_data = UserSerializer(user, many=True).data
            if len(user_data) > 0:
                return "{business_name}".format(business_name=user_data[0]['business_name'])
            else:
                return None
        else:
            return None

    def create(self, validated_data):
        payee_agreement = PayeeAgreement(
            payee_id=validated_data['payee_id'],
            user_id=validated_data['user_id'],
            remarks=validated_data['remarks'],
            is_accepted=validated_data['is_accepted'],
            created_by=1,
            updated_by=1,
            created_at=datetime.datetime.now(),
            updated_at=datetime.datetime.now(),
        )

        payee_agreement.save()

        return payee_agreement
