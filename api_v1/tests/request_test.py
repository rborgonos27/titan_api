from rest_framework.test import APIClient
from rest_framework.test import APITestCase
from rest_framework import status
from ..models import Request


class RequestTest(APITestCase):
    def setUp(self):
        client = APIClient()
        data = {
            'email': 'titan.payee@gmail.com',
            'username': 'titan.payee@gmail.com',
            'first_name': 'Titan',
            'last_name': 'Payee',
            'password': 'password123',
            'primary_contact_name': 'test',
            'primary_contact_number': 'test',
            'business_name': 'test',
            'principal_city_of_business': 'test',
            'user_type': 'super_admin',
        }
        response = client.post('/api/v1/user/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        client = APIClient()
        client.login(email='titan.payee@gmail.com', password='password123')

        data = {
            'name': 'Test Payee 2',
            'bank_name': 'TPA 2',
            'bank_account_name': 'TEST PAYEE 2',
            'bank_account': '1111-1111-2222',
            'bank_routing_number': 'test123',
            'address': 'test dfdf',
        }
        response = client.post('/api/v1/payee/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        client.logout()


    def test_create_payment(self):
        print('test_create_payment...')
        client = APIClient()
        client.login(email='titan.payee@gmail.com', password='password123')
        data = {
            'type': 'PAYMENT',
            'payee_id': 2,
            'amount': 1000,
            'due_at': '2018-08-05 11:00',
            'pickup_at': '2018-08-04 11:00',
            'remarks': 'test',
        }
        response = client.post('/api/v1/request/', data, format='json')
        # self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # print (response)
        # print (response.data)
        #response_expected = {'id': 1, 'payee_id': '', 'name': 'Test Payee', 'bank_name': 'TPA', 'address': 'test dfdf', 'bank_account_name': 'TEST PAYEE', 'bank_account': '1111-1111-2222', 'bank_routing_number': 'test123'}
        #self.assertEqual(response.data, response_expected)

        client.logout()
        print('test_create_payment passed...')