from rest_framework.test import APIClient
from rest_framework.test import APITestCase
from rest_framework import status
from ..models import Payee


class PayeeTest(APITestCase):
    def setUp(self):
        client = APIClient()
        data = {
            'email': 'titan.payee@gmail.com',
            'username': 'titan.payee@gmail.com',
            'first_name': 'Titan',
            'last_name': 'Payee',
            'password': 'password123',
            'primary_contact_name': 'test',
            'primary_contact_number': 'test',
            'business_name': 'test',
            'principal_city_of_business': 'test',
            'user_type': 'super_admin',
        }
        response = client.post('/api/v1/user/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_payee(self):
        print('test_create_payee...')
        client = APIClient()
        client.login(email='titan.payee@gmail.com', password='password123')
        data = {
            'name': 'Test Payee',
            'bank_name': 'TPA',
            'bank_account_name': 'TEST PAYEE',
            'bank_account': '1111-1111-2222',
            'bank_routing_number': 'test123',
            'address': 'test dfdf',
        }
        response = client.post('/api/v1/payee/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response_expected = {'id': 1, 'payee_id': '', 'name': 'Test Payee', 'bank_name': 'TPA', 'address': 'test dfdf', 'bank_account_name': 'TEST PAYEE', 'bank_account': '1111-1111-2222', 'bank_routing_number': 'test123'}
        self.assertEqual(response.data, response_expected)
        client.logout()
        print('test_create_payee passed...')

    def test_edit_payee(self):
        print('test_edit_payee...')
        client = APIClient()
        client.login(email='titan.payee@gmail.com', password='password123')

        client.login(email='titan.payee@gmail.com', password='password123')
        data = {
            'name': 'Test Payee',
            'bank_name': 'TPA',
            'bank_account_name': 'TEST PAYEE',
            'bank_account': '1111-1111-2222',
            'bank_routing_number': 'test123',
            'address': 'test dfdf',
        }
        response = client.post('/api/v1/payee/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        data = {
            'name': 'Test Payee 1',
            'bank_name': 'TPA',
            'bank_account_name': 'TEST PAYEE',
            'bank_account': '1111-1111-2222',
            'bank_routing_number': 'test123',
            'address': 'test dfdf',
        }
        response = client.put('/api/v1/payee/2/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response_expected = {'id': 2, 'payee_id': '', 'name': 'Test Payee 1', 'bank_name': 'TPA', 'address': 'test dfdf', 'bank_account_name': 'TEST PAYEE', 'bank_account': '1111-1111-2222', 'bank_routing_number': 'test123'}
        self.assertEqual(response.data, response_expected)
        client.logout()
        print('test_edit_payee passed...')

    class Meta(object):
        app_label = 'api_v1'