from rest_framework.test import APIClient
from rest_framework.test import APITestCase
from rest_framework import status
from ..models import User


class UserTest(APITestCase):
    def setUp(self):
        client = APIClient()
        data = {
            'email': 'titan.tester@gmail.com',
            'username': 'titan.tester@gmail.com',
            'first_name': 'Titan',
            'last_name': 'Tester',
            'password': 'password123',
            'primary_contact_name': 'test',
            'primary_contact_number': 'test',
            'business_name': 'test',
            'principal_city_of_business': 'test',
            'user_type': 'super_admin',
        }
        response = client.post('/api/v1/user/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_user(self):
        print('starting test_create_user...')
        client = APIClient()
        data = {
          'email': 'titan2.tester@gmail.com',
          'username': 'titan2.tester@gmail.com',
          'first_name': 'Titan 2',
          'last_name': 'Tester 2',
          'password': 'password123',
          'primary_contact_name': 'test',
          'primary_contact_number': 'test',
          'business_name': 'test',
          'principal_city_of_business': 'test',
          'user_type': 'super_admin',
        }
        response = client.post('/api/v1/user/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 2)
        response_expected = {'id': 5, 'email': 'titan2.tester@gmail.com', 'first_name': 'Titan 2', 'last_name': 'Tester 2', 'middle_name': '', 'primary_contact_name': 'test', 'primary_contact_number': 'test', 'business_name': 'test', 'principal_city_of_business': 'test', 'username': 'titan2.tester@gmail.com', 'user_type': 'super_admin'}
        self.assertEqual(response.data, response_expected)
        print('test_create_user passed...')

    def test_user_login(self):
        print('starting test_create_login...')
        client = APIClient()
        data = {
            'email': 'titan.tester@gmail.com',
            'password': 'password123'
        }
        response = client.post('/api/v1/login', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        print('test_create_login passed...')

    class Meta(object):
        app_label = 'api_v1'
